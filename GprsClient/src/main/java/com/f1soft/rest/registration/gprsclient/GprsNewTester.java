package com.f1soft.rest.registration.gprsclient;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.LoggingFilter;
import javax.ws.rs.core.MediaType;
import org.json.simple.JSONObject;

/**
 *
 * @author user
 */
public class GprsNewTester {

    static final String REST_URI = "http://localhost:8080/Registration-web/";

    public static void main(String[] args) {

        selfRegistration();
    }

    public static void selfRegistration() {
        try {
            ClientConfig config = new DefaultClientConfig();
            Client client = Client.create(config);
            client.addFilter(new LoggingFilter());
            WebResource service = client.resource(REST_URI);

            JSONObject jSONObject = new JSONObject();
            jSONObject.put("mobileNumber", "9812345678");
            jSONObject.put("accountNumber", "BSGANP713657");
            jSONObject.put("referenceNum", "12345");

            ClientResponse response = service.path("selfRegistration/register")
                    .type(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .post(ClientResponse.class, jSONObject.toJSONString());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
