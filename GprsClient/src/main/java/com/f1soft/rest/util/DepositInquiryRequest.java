package com.f1soft.rest.util;

/**
 *
 * @author santosh
 */
public class DepositInquiryRequest {

    private String accountNumber;
    private String query;
    private String requestType;

    public DepositInquiryRequest() {
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }
}
