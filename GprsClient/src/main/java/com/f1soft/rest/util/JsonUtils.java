package com.f1soft.rest.util;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class JsonUtils {

    public static JSONObject mapToJson(Map<?, ?> data) {
        JSONObject object = new JSONObject();

        for (Map.Entry<?, ?> entry : data.entrySet()) {
            /*
             * Deviate from the original by checking that keys are non-null and
             * of the proper type. (We still defer validating the values).
             */
            String key = (String) entry.getKey();
            if (key == null) {
                throw new NullPointerException("key == null");
            }
            try {
                object.put(key, wrap(entry.getValue()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return object;
    }

    public static JSONArray collectionToJson(Collection data) {
        JSONArray jsonArray = new JSONArray();
        if (data != null) {
            for (Object aData : data) {
                jsonArray.put(wrap(aData));

            }
        }
        return jsonArray;
    }

    public static JSONArray arrayToJson(Object data) throws Exception {
        if (!data.getClass().isArray()) {
            throw new Exception("Not a primitive data: " + data.getClass());
        }
        final int length = Array.getLength(data);
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < length; ++i) {
            jsonArray.put(wrap(Array.get(data, i)));
        }

        return jsonArray;
    }

    private static Object wrap(Object o) {
        if (o == null) {
            return null;
        }
        if (o instanceof JSONArray || o instanceof JSONObject) {
            return o;
        }
        try {
            if (o instanceof Collection) {
                return collectionToJson((Collection) o);
            } else if (o.getClass().isArray()) {
                return arrayToJson(o);
            }
            if (o instanceof Map) {
                return mapToJson((Map) o);
            }
            if (o instanceof Boolean
                    || o instanceof Byte
                    || o instanceof Character
                    || o instanceof Double
                    || o instanceof Float
                    || o instanceof Integer
                    || o instanceof Long
                    || o instanceof Short
                    || o instanceof String) {
                return o;
            }
            if (o.getClass().getPackage().getName().startsWith("java.")) {
                return o.toString();
            }
        } catch (Exception ignored) {
        }
        return null;
    }

    public static void main(String[] args) throws JSONException {
        List<Map<String, String>> miniStatementMapList = new ArrayList<Map<String, String>>();

        Map<String, String> miniStMap = new HashMap<String, String>();
        miniStMap.put("amount", "100");
        miniStMap.put("particular", "Cash Withdraw");
        miniStMap.put("date", "151111");
        miniStMap.put("txnType", "D");
        
        
        
        miniStatementMapList.add(miniStMap);

        Map<String, String> miniStMap2 = new HashMap<String, String>();
        miniStMap2.put("amount", "200");
        miniStMap2.put("particular", "Cash Deposit");
        miniStMap2.put("date", "151111");
        miniStMap2.put("txnType", "C");
        miniStatementMapList.add(miniStMap2);

        JSONArray arr = collectionToJson(miniStatementMapList);
        System.out.println(arr.toString());

        for (int i = 0; i < arr.length(); i++) {
            JSONObject obj = arr.getJSONObject(i);

            String particular = obj.getString("particular");
            Double amount = obj.getDouble("amount");
            System.out.println("amount__" + amount);
            System.out.println("particular__" + particular);
//        String B = obj.getString("B");
//        String C = obj.getString("C");
//        System.out.println(A + " " + B + " " + C);
        }



//        for (Map<String, String> map : miniStatementMapList) {
//            JSONObject jSONObject = mapToJson(map);
//            System.out.println("json__" + jSONObject.toString());
//        }
    }
}
