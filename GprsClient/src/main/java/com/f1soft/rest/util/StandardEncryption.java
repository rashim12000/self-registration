package com.f1soft.rest.util;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author sandhya.pokhrel
 */
public class StandardEncryption {

    private static String sharedKeyForEncryption = "123000000000000000000xyz";
    private static byte[] sharedkey = {
        0x01, 0x02, 0x03, 0x05, 0x07, 0x0B, 0x0D, 0x11,
        0x12, 0x11, 0x0D, 0x0B, 0x07, 0x02, 0x04, 0x08,
        0x01, 0x02, 0x03, 0x05, 0x07, 0x0B, 0x0D, 0x11
    };
    private static byte[] sharedvector = {
        10, 10, 10, 20, 20, 20, 55, 8
    };

    public static String encrypt(String plaintext) {
        try {
            Cipher c = Cipher.getInstance("DESede/ECB/PKCS5Padding");
            c.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(sharedKeyForEncryption.getBytes(), "DESede"));
            byte[] encrypted = c.doFinal(plaintext.getBytes("UTF-8"));

            return Base64.encode(encrypted);
        } catch (Exception ex) {
            return null;
        }
    }

    public static String decrypt(String ciphertext) {
        try {
            Cipher c = Cipher.getInstance("DESede/ECB/PKCS5Padding");
            c.init(Cipher.DECRYPT_MODE, new SecretKeySpec(sharedKeyForEncryption.getBytes(), "DESede"));
            byte[] decrypted = c.doFinal(Base64.decode(ciphertext));
            return new String(decrypted, "UTF-8");
        } catch (Exception ex) {
            return null;
        }
    }

    public static void main(String[] args) {

//        String username = "Nab6v87CL&:zK<^m&jsSm@rt";                  
//        String password = "Nab6v87CL&:zK<^m&jsSm@rt";
//        System.out.println("Username enrypted ::" + encrypt(username));
//        System.out.println("Password encrypted ::" + encrypt(password));


//        String loginUsername = "9841002254";
//        String loginPassword = "nar1234";
//        String loginDeviceId = "ffffffff-ac04-7e51-ffff-fffff19295f9";

        String loginUsername = "9860113030";
        String loginPassword = "nar4321";
        String loginDeviceId = "ffffffff-b295-513e-0000-0000567d62be";

        System.out.println("Encr loginUsername :" + StandardEncryption.encrypt(loginUsername));
        System.out.println("Encr loginPassword :" + StandardEncryption.encrypt(loginPassword));
        System.out.println("Encr loginDeviceId :" + StandardEncryption.encrypt(loginDeviceId));
        System.out.println("Encr Txn Password :" + StandardEncryption.encrypt("adm12"));


    }
}
