package com.f1soft.banksmart.auto.registration.connector;

import com.f1soft.cbs.common.constant.QueryParameterConstant;
import com.f1soft.cbs.common.dto.CustomerAccount;
import com.f1soft.cbs.common.dto.CustomerInformation;
import com.f1soft.soap.connector.factory.SoapManagerFactory;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author prashant Apr 19, 2018 12:25:02 PM
 */
public class SoapConnector {

    private static final Logger logger = LoggerFactory.getLogger(SoapConnector.class);
    private static SoapConnector instance = null;

    private SoapConnector() {
    }

    public static SoapConnector getInstance() {
        if (instance == null) {
            synchronized (SoapConnector.class) {
                if (instance == null) {
                    instance = new SoapConnector();
                }
            }
        }
        return instance;
    }

    public synchronized CustomerInformation getCustomerInformationFromAccountNumber(String accountNumber) {
        try {
            Map<String, Object> queryParameterMap = new HashMap<>();
            queryParameterMap.put(QueryParameterConstant.MAIN_CODE, accountNumber);
            String customerCBSID = SoapManagerFactory.getSoapManager().findCbsIdFromAccountNumber(queryParameterMap);
            logger.info("customer id in Customer information from account number " + customerCBSID);

            queryParameterMap.put(QueryParameterConstant.CLIENT_CODE, customerCBSID);
            CustomerInformation customerInformation = SoapManagerFactory.getSoapManager().findCustomerInformation(queryParameterMap);

            return customerInformation;
        } catch (Exception e) {
            logger.error("Exception", e);
            return null;
        }
    }

    public synchronized List<CustomerAccount> getCustomerAccountsFromCBS(String customerCBSID) {
        try {
            Map<String, Object> queryParameterMap = new HashMap<>();
            logger.info("customer id in Customer information from account number " + customerCBSID);

            queryParameterMap.put(QueryParameterConstant.CLIENT_CODE, customerCBSID);
            List<CustomerAccount> customerAccounts = SoapManagerFactory.getSoapManager().getCustomerAccountList(queryParameterMap);

            return customerAccounts;
        } catch (Exception e) {
            logger.error("Exception", e);
            return null;
        }
    }

}
