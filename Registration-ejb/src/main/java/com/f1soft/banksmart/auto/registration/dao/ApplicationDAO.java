package com.f1soft.banksmart.auto.registration.dao;

import com.f1soft.banksmart.auto.registration.entities.AccessibleChannel;
import com.f1soft.banksmart.auto.registration.entities.AccountType;
import com.f1soft.banksmart.auto.registration.entities.ApplicationPattern;
import com.f1soft.banksmart.auto.registration.entities.ApplicationSetup;
import com.f1soft.banksmart.auto.registration.entities.ApplicationUser;
import com.f1soft.banksmart.auto.registration.entities.Branch;
import com.f1soft.banksmart.auto.registration.entities.CBSQuery;
import com.f1soft.banksmart.auto.registration.entities.ChargeType;
import com.f1soft.banksmart.auto.registration.entities.CustomerLogin;
import com.f1soft.banksmart.auto.registration.entities.MessageFormat;
import com.f1soft.banksmart.auto.registration.entities.Profile;
import com.f1soft.banksmart.auto.registration.entities.ProfileCharge;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Narayan Joshi
 * <narayan.joshi@f1soft.com>
 */
public interface ApplicationDAO {

    Map<String, Branch> getBranchesMap();

    Map<String, AccountType> getAccountTypeMap();

    Map<String, ChargeType> chargeTypeMap();

    Map<String, MessageFormat> messageFormatMap();

    List<ApplicationPattern> getAllApplicationPatterns();

    Profile getProfileById(Long id);

    ApplicationUser getApplicationUser(Long id);

    CustomerLogin getCustomerLogin(String mobileNumber);

    List<AccessibleChannel> accessibleChannels();

    List<ProfileCharge> getProfileChargeByProfileId(Long profileId);

    List<CBSQuery> getAllCBSQuery();

    List<ApplicationSetup> getAllApplicationSetups();

}
