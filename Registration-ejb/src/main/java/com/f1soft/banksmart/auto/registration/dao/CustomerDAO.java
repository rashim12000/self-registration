package com.f1soft.banksmart.auto.registration.dao;

import com.f1soft.banksmart.auto.registration.entities.BankAccount;
import com.f1soft.banksmart.auto.registration.entities.Customer;
import com.f1soft.banksmart.auto.registration.entities.CustomerLogin;
import com.f1soft.banksmart.auto.registration.entities.LoginAccessChannel;
import java.util.List;

/**
 *
 * @author Narayan Joshi
 * <narayan.joshi@f1soft.com>
 */
public interface CustomerDAO {

    boolean registerCustomer(Customer customer, List<CustomerLogin> customerLogins, List<LoginAccessChannel> loginAccessChannels, List<BankAccount> bankAccounts);

}
