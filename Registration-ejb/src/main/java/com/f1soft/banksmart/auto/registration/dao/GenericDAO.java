package com.f1soft.banksmart.auto.registration.dao;

import java.util.List;

/**
 *
 * @author rashim.dhaubanjar
 */
public interface GenericDAO<T> {

    boolean save(T entity);

    boolean modify(T entity);

    T getById(Long id);

    List<T> findAll();
}
