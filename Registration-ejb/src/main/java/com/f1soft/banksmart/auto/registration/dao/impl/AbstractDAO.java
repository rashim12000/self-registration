package com.f1soft.banksmart.auto.registration.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Narayan Joshi
 * <narayan.joshi@f1soft.com>
 */
public class AbstractDAO<T> {

    private static final Logger logger = LoggerFactory.getLogger(AbstractDAO.class);
    protected final EntityManagerFactory emf;
    private final Class<T> entityClass;

    public AbstractDAO(Class<T> entityClass, EntityManagerFactory emf) {
        this.entityClass = entityClass;
        this.emf = emf;
    }

    public void create(T entity) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(entity);
            em.getTransaction().commit();
        } catch (Exception e) {
            logger.error("Exception : {}", e);
        } finally {
            try {
                if (em != null) {
                    em.close();
                }
            } catch (NullPointerException e) {
            }
        }
    }

    public void edit(T entity) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            em.merge(entity);
            em.getTransaction().commit();
        } catch (Exception e) {
            logger.error("Exception : {}", e);
        } finally {
            try {
                if (em != null) {
                    em.close();
                }
            } catch (NullPointerException e) {
            }
        }
    }

    public void delete(T entity) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            em.remove(entity);
            em.getTransaction().commit();
        } catch (Exception e) {
            logger.error("Exception : {}", e);
        } finally {
            try {
                if (em != null) {
                    em.close();
                }
            } catch (NullPointerException e) {
            }
        }
    }

    public T find(Object id) {
        EntityManager em = null;
        try {
            return em.find(entityClass, id);
        } catch (Exception e) {
            logger.error("Exception : {}", e);
        } finally {
            try {
                if (em != null) {
                    em.close();
                }
            } catch (NullPointerException e) {
            }
        }
        return null;
    }
}
