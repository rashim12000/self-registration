package com.f1soft.banksmart.auto.registration.dao.impl;

import com.f1soft.banksmart.auto.registration.dao.ApplicationDAO;
import com.f1soft.banksmart.auto.registration.entities.AccessibleChannel;
import com.f1soft.banksmart.auto.registration.entities.AccountType;
import com.f1soft.banksmart.auto.registration.entities.ApplicationPattern;
import com.f1soft.banksmart.auto.registration.entities.ApplicationSetup;
import com.f1soft.banksmart.auto.registration.entities.ApplicationUser;
import com.f1soft.banksmart.auto.registration.entities.Branch;
import com.f1soft.banksmart.auto.registration.entities.CBSQuery;
import com.f1soft.banksmart.auto.registration.entities.ChargeType;
import com.f1soft.banksmart.auto.registration.entities.CustomerLogin;
import com.f1soft.banksmart.auto.registration.entities.MessageFormat;
import com.f1soft.banksmart.auto.registration.entities.Profile;
import com.f1soft.banksmart.auto.registration.entities.ProfileCharge;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Narayan Joshi
 * <narayan.joshi@f1soft.com>
 */
@Slf4j
@Stateless
public class ApplicationDAOImpl implements ApplicationDAO {

    @PersistenceContext(unitName = "fonebankPU")
    private EntityManager em;

    @Override
    public Map<String, Branch> getBranchesMap() {
        Map<String, Branch> branches = new HashMap<>();

        try {
            Query query = em.createNamedQuery("Branch.findAll");
            List<Branch> branchList = query.getResultList();
            for (Branch branch : branchList) {
                if (branch.getActive() == 'Y') {
                    branches.put(branch.getBranchCode(), branch);
                }
            }
            return branches;
        } catch (Exception e) {
            log.error("Exception", e);
            return branches;
        }
    }

    @Override
    public Map<String, AccountType> getAccountTypeMap() {
        Map<String, AccountType> map = new HashMap<>();
        try {
            Query query = em.createNamedQuery("AccountType.findAll");
            List<AccountType> accountTypeList = query.getResultList();
            for (AccountType accountType : accountTypeList) {
                if (accountType.getActive() == 'Y') {
                    map.put(accountType.getType(), accountType);
                }
            }
            return map;
        } catch (Exception e) {
            log.error("Exception", e);
            return map;
        }
    }

    @Override
    public Map<String, ChargeType> chargeTypeMap() {
        Map<String, ChargeType> map = new HashMap<>();
        try {
            Query query = em.createNamedQuery("ChargeType.findAll");
            List<ChargeType> chargeTypeList = query.getResultList();
            for (ChargeType chargeType : chargeTypeList) {
                if (chargeType.getActive() == 'Y') {
                    map.put(chargeType.getType(), chargeType);
                }
            }
            return map;
        } catch (Exception e) {
            log.error("Exception", e);
            return map;
        }
    }

    @Override
    public Map<String, MessageFormat> messageFormatMap() {
        Map<String, MessageFormat> map = new HashMap<>();
        try {
            Query query = em.createQuery("Select m from MessageFormat m");
            List<MessageFormat> messageFormatList = query.getResultList();
            for (MessageFormat messageFormat : messageFormatList) {
                String msg = messageFormat.getMessage();
                messageFormat.setMessage(msg);
                map.put(messageFormat.getMessageCode().trim(), messageFormat);
            }
            return map;
        } catch (Exception e) {
            log.error("Exception", e);
            return map;
        }
    }

    @Override
    public List<ApplicationPattern> getAllApplicationPatterns() {
        List<ApplicationPattern> patternList = new ArrayList<>();
        try {
            Query query = em.createNamedQuery("ApplicationPattern.findAll");
            patternList = query.getResultList();
            return patternList;
        } catch (Exception e) {
            log.error("Exception", e);
            return patternList;
        }
    }

    @Override
    public Profile getProfileById(Long id) {
        Profile profile = new Profile();
        try {
            Query query = em.createNamedQuery("Profile.findById");
            query.setParameter("id", id);
            profile = (Profile) query.getSingleResult();
            return profile;
        } catch (Exception e) {
            log.error("Exception", e);
            return profile;
        }
    }

    @Override
    public ApplicationUser getApplicationUser(Long id) {
        ApplicationUser applicationUser = new ApplicationUser();
        try {
            Query query = em.createNamedQuery("ApplicationUser.findById");
            query.setParameter("id", id);
            applicationUser = (ApplicationUser) query.getSingleResult();
            return applicationUser;
        } catch (NoResultException nre) {
            return applicationUser;
        } catch (Exception e) {
            log.error("Exception", e);
            return applicationUser;
        }
    }

    @Override
    public CustomerLogin getCustomerLogin(String mobileNumber) {
        log.debug("Checking if Mobile Number already registered");
        try {
            Query query = em.createNamedQuery("CustomerLogin.findUnDeletedUserByUsername");
            query.setParameter("username", mobileNumber);
            CustomerLogin customerLogin = (CustomerLogin) query.getSingleResult();
            return customerLogin;
        } catch (NoResultException nre) {
            nre.printStackTrace();
            log.info("New Customer Registration");
            return null;
        }
    }

    @Override
    public List<AccessibleChannel> accessibleChannels() {
        List<AccessibleChannel> accessibleChannelList = new ArrayList<>();
        try {
            Query query = em.createNamedQuery("AccessibleChannel.findAll");
            accessibleChannelList = query.getResultList();
            return accessibleChannelList;
        } catch (Exception e) {
            log.error("Exception", e);
            return accessibleChannelList;
        }
    }

    @Override
    public List<ProfileCharge> getProfileChargeByProfileId(Long profileId) {
        List<ProfileCharge> profileChargeList = new ArrayList<>();
        try {
            Query query = em.createNamedQuery("ProfileCharge.findByProfileId");
            query.setParameter("profileId", profileId);
            profileChargeList = query.getResultList();
            return profileChargeList;
        } catch (Exception e) {
            log.error("Exception", e);
            return profileChargeList;
        }
    }

    @Override
    public List<CBSQuery> getAllCBSQuery() {
        List<CBSQuery> cbsQueries = new ArrayList<>();
        try {
            Query query = em.createNamedQuery("CBSQuery.findAll");
            cbsQueries = query.getResultList();
        } catch (Exception e) {
            log.error("Exception", e);
            return null;
        }
        return cbsQueries;
    }

    @Override
    public List<ApplicationSetup> getAllApplicationSetups() {
        List<ApplicationSetup> applicationSetups = new ArrayList<>();
        try {
            Query query = em.createNamedQuery("ApplicationSetup.findAll");
            applicationSetups = query.getResultList();
        } catch (Exception e) {
            log.error("Exception", e);
            return null;
        }
        return applicationSetups;
    }

}
