package com.f1soft.banksmart.auto.registration.dao.impl;

import com.f1soft.banksmart.auto.registration.dao.CustomerDAO;
import com.f1soft.banksmart.auto.registration.entities.BankAccount;
import com.f1soft.banksmart.auto.registration.entities.Customer;
import com.f1soft.banksmart.auto.registration.entities.CustomerApprovalHistory;
import com.f1soft.banksmart.auto.registration.entities.CustomerLogin;
import com.f1soft.banksmart.auto.registration.entities.LoginAccessChannel;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Narayan Joshi
 * <narayan.joshi@f1soft.com>
 */
@Slf4j
@Stateless
@Transactional
public class CustomerDAOImpl extends GenericDAOImpl<Customer> implements CustomerDAO {

    public CustomerDAOImpl() {
        super(Customer.class);
    }

    @Override
    public boolean registerCustomer(Customer customer, List<CustomerLogin> customerLogins, List<LoginAccessChannel> loginAccessChannels, List<BankAccount> bankAccounts) {
        log.info("Saving customer information to systsem");

        getEntityManager().persist(customer);
        log.info("Customer Registered with id: " + customer.getId());

        for (BankAccount bankAccount : bankAccounts) {
            bankAccount.setCustomer(customer);
            getEntityManager().persist(bankAccount);
        }

        for (CustomerLogin customerLogin : customerLogins) {
            customerLogin.setCustomer(customer);
            getEntityManager().persist(customerLogin);
        }

        for (LoginAccessChannel loginAccessChannel : loginAccessChannels) {
            loginAccessChannel.setCustomerLogin(customerLogins.get(0));
            getEntityManager().persist(loginAccessChannel);
        }

        CustomerApprovalHistory customerApprovalHistory = new CustomerApprovalHistory();
        customerApprovalHistory.setCustomerId(customer.getId());
        customerApprovalHistory.setApprovedDate(new Date());
        getEntityManager().persist(customerApprovalHistory);

        return true;
    }

}
