package com.f1soft.banksmart.auto.registration.dao.impl;

import com.f1soft.banksmart.auto.registration.dao.GenericDAO;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author rashim.dhaubanjar
 */
public class GenericDAOImpl<T> implements GenericDAO<T> {

    private Class<T> persistenceClass;

    @PersistenceContext
    private EntityManager entityManager;

    public GenericDAOImpl() {
    }

    public GenericDAOImpl(Class<T> persistenceClass) {
        this.persistenceClass = persistenceClass;
    }

    public Class<T> getPersistenceClass() {
        return persistenceClass;
    }

    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public boolean save(T entity) {
        try {
            getEntityManager().persist(entity);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean modify(T entity) {
        try {
            getEntityManager().merge(entity);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public T getById(Long id) {
        T entity = (T) getEntityManager()
                .createQuery("Select x FROM " + getPersistenceClass().getSimpleName() + " x where x.id = :id")
                .setParameter("id", id)
                .getSingleResult();
        return entity;
    }

    @Override
    public List<T> findAll() {
        return getEntityManager()
                .createQuery("Select x FROM " + getPersistenceClass().getSimpleName() + " x")
                .getResultList();
    }
}
