package com.f1soft.banksmart.auto.registration.dto;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author sandhya.pokhrel
 */
@Getter
@Setter
public class AccountBranchDTO extends ModelBase {

    private String accountNumber;
    private String branchCode;
    private String cbsId;

}
