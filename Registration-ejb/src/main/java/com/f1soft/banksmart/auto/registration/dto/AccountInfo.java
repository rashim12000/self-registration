package com.f1soft.banksmart.auto.registration.dto;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author sandhya.pokhrel
 */
@Getter
@Setter
public class AccountInfo extends ModelBase {

    private String accountNumber;
    private String cbsId;
    private String accountType;
    private String branchCode;
    private String cardNumber;
    private boolean thirdParty;

}
