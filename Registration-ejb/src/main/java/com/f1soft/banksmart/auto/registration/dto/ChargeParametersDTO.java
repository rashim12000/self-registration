package com.f1soft.banksmart.auto.registration.dto;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Narayan Joshi
 * <narayan.joshi@f1soft.com>
 */
@Getter
@Setter
public class ChargeParametersDTO extends ModelBase {

    private Double charge;
    private String code;
    private Long chargeTypeId;
    private boolean hasCharge;
}
