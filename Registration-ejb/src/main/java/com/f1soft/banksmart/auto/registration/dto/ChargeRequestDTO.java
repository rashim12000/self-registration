package com.f1soft.banksmart.auto.registration.dto;

import com.f1soft.banksmart.auto.registration.entities.ApplicationUser;
import com.f1soft.banksmart.auto.registration.entities.Customer;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Narayan Joshi
 * <narayan.joshi@f1soft.com>
 */
@Getter
@Setter
public class ChargeRequestDTO extends ModelBase {

    private Long bankAccountId;
    private Double amount;
    private Double chargeAmount;
    private String chargeCode;
    private Long chargeTypeId;
    private ApplicationUser initiator;
    private Character trialPeriod;
    private String accountNumber;
    private Customer customer;
    private Integer renewInterval;
    private Date moduleExpiryDate;
    private String remarks;
    private String chargeGroup;
}
