package com.f1soft.banksmart.auto.registration.dto;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Narayan Joshi
 * <narayan.joshi@f1soft.com>
 */
@Getter
@Setter
public class ConnectionParameter extends ModelBase {

    private String url;
    private String driver;
    private String user;
    private String password;
}
