package com.f1soft.banksmart.auto.registration.dto;

import com.f1soft.banksmart.auto.registration.entities.BankAccount;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Narayan Joshi
 * <narayan.joshi@f1soft.com>
 */
@Getter
@Setter
public class CustomerApprovalDTO extends ModelBase {

    private String nonEcryptedPassword;
    private String bcryptedPassword;
    private String bcryptedTxnPassword;
    private String encryptedPassword;
    private String remarksMessage;
    private char approveStatus;
    private String smsFooter;
    private List<ChargeRequestDTO> chargeRequestDTOList;
    private Map<String, String> loginPasswordMap;
    private Map<String, String> passwordMap;
    private Double charge;
    private BankAccount bankAccount;
}
