package com.f1soft.banksmart.auto.registration.dto;

import com.f1soft.banksmart.auto.registration.entities.ApplicationUser;
import com.f1soft.banksmart.auto.registration.entities.BankAccount;
import com.f1soft.banksmart.auto.registration.entities.Branch;
import com.f1soft.banksmart.auto.registration.entities.CustomerLogin;
import com.f1soft.banksmart.auto.registration.entities.LoginAccessChannel;
import com.f1soft.banksmart.auto.registration.entities.Profile;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author sandhya.pokhrel
 */
@Getter
@Setter
public class CustomerInformationDTO extends ModelBase {

    private String firstName;
    private String middleName;
    private String lastName;
    private String cbsId;
    private String gender;
    private String emailAddress;
    private Character accountHolder;
    private Character active;
    private Branch branch;
    private String mobileNumber;
    private Character approved;
    private String latestActivityRemarks;
    private String address;
    private String contactNumber;
    private Profile profile;
    private ApplicationUser createdBy;

    private List<CustomerLogin> customerLogins;
    private List<LoginAccessChannel> loginAccessChannels;
    private List<BankAccount> accounts;

}
