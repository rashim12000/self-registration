package com.f1soft.banksmart.auto.registration.dto;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author rashim.dhaubanjar
 */
@Getter
@Setter
public class ErrorResponse extends ModelBase {

    private boolean success;
    private int status;
    private int code;
    private String message;
    private String link;
    private String developerMessage;

    public ErrorResponse(int status, int code, String message, String link, String developerMessage) {
        this.success = false;
        this.status = status;
        this.code = code;
        this.message = message;
        this.link = link;
        this.developerMessage = developerMessage;
    }
}
