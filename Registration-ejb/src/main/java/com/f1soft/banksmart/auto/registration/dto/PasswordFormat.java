package com.f1soft.banksmart.auto.registration.dto;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Narayan Joshi
 * <narayan.joshi@f1soft.com>
 */
@Getter
@Setter
public class PasswordFormat extends ModelBase {

    private int loginAlphaLength;
    private int loginNumericLength;
    private int loginSpecialCharLength;
    private int txnAlphaLength;
    private int txnNumericLength;
    private int txnSpecialCharLength;
    private String specialChar;
}
