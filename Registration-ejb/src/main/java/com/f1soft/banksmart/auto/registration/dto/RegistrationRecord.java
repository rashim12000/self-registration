package com.f1soft.banksmart.auto.registration.dto;

import com.f1soft.banksmart.auto.registration.entities.ApplicationUser;
import lombok.Getter;
import lombok.Setter;
import com.f1soft.banksmart.auto.registration.entities.Profile;

/**
 *
 * @author Narayan Joshi
 * <narayan.joshi@f1soft.com>
 */
@Getter
@Setter
public class RegistrationRecord extends ModelBase {

    private Long id;
    private String accountNumber;
    private String branchCode;
    private String cbsId;
    private String emailAddress;
    private String mobileNumber;
    private Long applicationUserId;
    private Long profileId;
    private String failureRemarks;
    private String referenceNumber;
    private Profile profile;
    private ApplicationUser applicationUser;

}
