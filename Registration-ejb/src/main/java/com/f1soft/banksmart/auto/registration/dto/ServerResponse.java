package com.f1soft.banksmart.auto.registration.dto;

/**
 *
 * @author sandhya.pokhrel
 */
public class ServerResponse extends ModelBase {

    private boolean success;
    private String remarks;
    private Object obj;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return remarks;
    }

    public void setMessage(String remarks) {
        this.remarks = remarks;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

}
