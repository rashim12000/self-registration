package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "ACCESSIBLE_CHANNEL")
@NamedQueries({
    @NamedQuery(name = "AccessibleChannel.findAll", query = "SELECT b FROM AccessibleChannel b"),
    @NamedQuery(name = "AccessibleChannel.findByStatus", query = "SELECT b FROM AccessibleChannel b WHERE b.active = :active"),
    @NamedQuery(name = "AccessibleChannel.findByChannelKey", query = "SELECT b FROM AccessibleChannel b WHERE b.channelKey = :channelKey "),
    @NamedQuery(name = "AccessibleChannel.findById", query = "SELECT b FROM AccessibleChannel b WHERE b.id = :id")
})
public class AccessibleChannel implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @Basic(optional = false)
    @Column(name = "CHANNEL_KEY", nullable = false, length = 50)
    private String channelKey;
    @Basic(optional = false)
    @Column(name = "CHANNEL_NAME", nullable = false, length = 100)
    private String channelName;
    @Basic(optional = false)
    @Column(name = "ACTIVE", nullable = false, length = 1)
    private Character active;
    @Basic(optional = false)
    @Column(name = "ALLOW_MULTIPLE_LOGIN", nullable = false, length = 1)
    private Character allowMultipleLogin;
    @OneToMany(mappedBy = "accessibleChannel", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<ChannelLoginPattern> channelLoginPatternList;

    public AccessibleChannel() {
    }

    public AccessibleChannel(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChannelKey() {
        return channelKey;
    }

    public void setChannelKey(String channelKey) {
        this.channelKey = channelKey;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public Character getAllowMultipleLogin() {
        return allowMultipleLogin;
    }

    public void setAllowMultipleLogin(Character allowMultipleLogin) {
        this.allowMultipleLogin = allowMultipleLogin;
    }

    public List<ChannelLoginPattern> getChannelLoginPatternList() {
        return channelLoginPatternList;
    }

    public void setChannelLoginPatternList(List<ChannelLoginPattern> channelLoginPatternList) {
        this.channelLoginPatternList = channelLoginPatternList;
    }
}
