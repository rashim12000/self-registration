package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "ACCOUNT_TYPE")
@NamedQueries({
    @NamedQuery(name = "AccountType.findAll", query = "SELECT b FROM AccountType b"),
    @NamedQuery(name = "AccountType.findByType", query = "SELECT b FROM AccountType b WHERE UPPER(b.type) = :type and b.active !='D'"),
    @NamedQuery(name = "AccountType.findByStatus", query = "SELECT b FROM AccountType b WHERE b.active != :active"),
    @NamedQuery(name = "AccountType.findById", query = "SELECT b FROM AccountType b WHERE b.id = :id")
})
public class AccountType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @Basic(optional = false)
    @Column(name = "TYPE", nullable = false, length = 50)
    private String type;
    @Basic(optional = false)
    @Column(name = "ACCOUNT_TYPE_NAME", nullable = false, length = 100)
    private String accountTypeName;
    @Basic(optional = false)
    @Column(name = "ACTIVE", nullable = false, length = 1)
    private char active;
    @Column(name = "ALLOW_TXN", length = 1, nullable = false)
    private char allowTxn;
    @Column(name = "ALLOW_REQUEST", length = 1, nullable = false)
    private char allowRequest;
    @Column(name = "ALLOW_TXN_ALERT", length = 1, nullable = true)
    private char allowTxnAlert;
    

    public AccountType() {
    }

    public AccountType(Long id) {
        this.id = id;
    }
    
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public char getAllowRequest() {
        return allowRequest;
    }

    public void setAllowRequest(char allowRequest) {
        this.allowRequest = allowRequest;
    }

    public char getAllowTxn() {
        return allowTxn;
    }

    public void setAllowTxn(char allowTxn) {
        this.allowTxn = allowTxn;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAccountTypeName() {
        return accountTypeName;
    }

    public void setAccountTypeName(String accountTypeName) {
        this.accountTypeName = accountTypeName;
    }

    public char getActive() {
        return active;
    }

    public void setActive(char active) {
        this.active = active;
    }

    public char getAllowTxnAlert() {
        return allowTxnAlert;
    }

    public void setAllowTxnAlert(char allowTxnAlert) {
        this.allowTxnAlert = allowTxnAlert;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccountType)) {
            return false;
        }
        AccountType other = (AccountType) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.f1soft.banksmart.auto.registration.entities.AccountType[id=" + id + "]";
    }
}
