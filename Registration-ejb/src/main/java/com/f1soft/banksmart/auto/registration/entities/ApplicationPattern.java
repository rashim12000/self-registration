package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "APPLICATION_PATTERN")
@NamedQueries({
    @NamedQuery(name = "ApplicationPattern.findAll", query = "SELECT s FROM ApplicationPattern s"),
    @NamedQuery(name = "ApplicationPattern.findById", query = "SELECT s FROM ApplicationPattern s WHERE s.id = :id"),
    @NamedQuery(name = "ApplicationPattern.findByPattern", query = "SELECT s FROM ApplicationPattern s WHERE s.pattern = :pattern"),
    @NamedQuery(name = "ApplicationPattern.findByChannelProvider", query = "SELECT s FROM ApplicationPattern s WHERE s.channelProvider.id = :channelProviderId")})
public class ApplicationPattern implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @JoinColumn(name = "CHANNEL_PROVIDER_ID", referencedColumnName = "ID", nullable = true)
    @ManyToOne(fetch = FetchType.EAGER)
    private ChannelProvider channelProvider;
    @Column(name = "PATTERN", nullable = true, length = 255)
    private String pattern;
    @Column(name = "PATTERN_CODE", length = 10)
    private String patternCode;
    @Column(name = "NAME", nullable = false, length = 50)
    private String name;
    @JoinColumn(name = "LOGIN_TYPE_ID", referencedColumnName = "ID", nullable = true)
    @ManyToOne()
    private LoginType loginType;
    @Column(name = "VALIDATOR_MESSAGE", length = 255)
    private String validatorMessage;
    @OneToMany(mappedBy = "applicationPattern", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<PasswordNotification> passwordNotificationList;

    public ApplicationPattern() {
    }

    public ApplicationPattern(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ChannelProvider getChannelProvider() {
        return channelProvider;
    }

    public void setChannelProvider(ChannelProvider channelProvider) {
        this.channelProvider = channelProvider;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getPatternCode() {
        return patternCode;
    }

    public void setPatternCode(String patternCode) {
        this.patternCode = patternCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LoginType getLoginType() {
        return loginType;
    }

    public void setLoginType(LoginType loginType) {
        this.loginType = loginType;
    }

    public String getValidatorMessage() {
        return validatorMessage;
    }

    public void setValidatorMessage(String validatorMessage) {
        this.validatorMessage = validatorMessage;
    }

    public List<PasswordNotification> getPasswordNotificationList() {
        return passwordNotificationList;
    }

    public void setPasswordNotificationList(List<PasswordNotification> passwordNotificationList) {
        this.passwordNotificationList = passwordNotificationList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ApplicationPattern)) {
            return false;
        }
        ApplicationPattern other = (ApplicationPattern) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.f1soft.banksmart.auto.registration.entities.ServicePattern[id=" + id + "]";
    }
}
