package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "APPLICATION_USER")
@NamedQueries({
    @NamedQuery(name = "ApplicationUser.findAll", query = "SELECT a FROM ApplicationUser a"),
    @NamedQuery(name = "ApplicationUser.findById", query = "SELECT a FROM ApplicationUser a WHERE a.id = :id"),
    @NamedQuery(name = "ApplicationUser.findByPassword", query = "SELECT a FROM ApplicationUser a WHERE a.password = :password"),
    @NamedQuery(name = "ApplicationUser.findByUsername", query = "SELECT a FROM ApplicationUser a WHERE a.username = :username"),
    @NamedQuery(name = "ApplicationUser.findByActive", query = "SELECT a FROM ApplicationUser a WHERE a.active = :active"),
    @NamedQuery(name = "ApplicationUser.findByCreatedDate", query = "SELECT a FROM ApplicationUser a WHERE a.createdDate = :createdDate"),
    @NamedQuery(name = "ApplicationUser.findByApprovalDate", query = "SELECT a FROM ApplicationUser a WHERE a.approvalDate = :approvalDate"),
    @NamedQuery(name = "ApplicationUser.findByLastLogintime", query = "SELECT a FROM ApplicationUser a WHERE a.lastLogintime = :lastLogintime"),
    @NamedQuery(name = "ApplicationUser.findByUndeletedStatus", query = "SELECT a FROM ApplicationUser a WHERE a.userDeleted != 'Y'"),
    @NamedQuery(name = "ApplicationUser.findByUndeletedStatusAndUsername", query = "SELECT a FROM ApplicationUser a WHERE a.userDeleted != 'Y' and a.username = :username"),
    @NamedQuery(name = "ApplicationUser.findByLoginStatus", query = "SELECT a FROM ApplicationUser a WHERE a.loginStatus = :loginStatus")
})
public class ApplicationUser implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @Basic(optional = false)
    @Column(name = "PASSWORD", nullable = false, length = 200)
    private String password;
    @Basic(optional = false)
    @Column(name = "USERNAME", nullable = false, length = 50)
    private String username;
    @Basic(optional = false)
    @Column(name = "NAME", nullable = false, length = 200)
    private String name;
    @Basic(optional = true)
    @Column(name = "EMAIL_ADDRESS", nullable = true, length = 200)
    private String emailAddress;
    @Basic(optional = false)
    @Column(name = "ACTIVE", nullable = false)
    private char active;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne()
    private ApplicationUser createdBy;
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;
    @JoinColumn(name = "LAST_MODIFIED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne()
    private ApplicationUser lastModifiedBy;
    @Column(name = "APPROVAL_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date approvalDate;
    @JoinColumn(name = "APPROVED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne()
    private ApplicationUser approvedBy;
    @Column(name = "LAST_LOGIN_TIME", length = 50)
    private String lastLogintime;
    @Column(name = "LOGIN_STATUS")
    private Integer loginStatus;
    @Column(name = "DELETED", length = 50)
    private Character userDeleted;
    @JoinColumn(name = "DELETED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne()
    private ApplicationUser userDeletedBy;
    @Column(name = "DELETED_DATE", length = 50)
    @Temporal(TemporalType.TIMESTAMP)
    private Date userdeletedDate;
    @Column(name = "BULK_SMS_LIMIT", length = 25)
    private Integer bulkSmsLimit;
    @Column(name = "CENTRALIZED_OPERATION_CONTROL", length = 50)
    private Character centralizedOperationControl;
    @JoinColumn(name = "BRANCH_ID", referencedColumnName = "ID", nullable = true)
    @ManyToOne()
    private Branch branch;
    @JoinColumn(name = "PROFILE_ID", referencedColumnName = "ID", nullable = true)
    @ManyToOne()
    private Profile profile;

    public ApplicationUser() {
    }

    public ApplicationUser(Long id) {
        this.id = id;
    }
    
    public ApplicationUser(String username, String name, String emailAddress) {
        this.username = username;
        this.name = name;
        this.emailAddress = emailAddress;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getBulkSmsLimit() {
        return bulkSmsLimit;
    }

    public void setBulkSmsLimit(Integer bulkSmsLimit) {
        this.bulkSmsLimit = bulkSmsLimit;
    }

    public Character getCentralizedOperationControl() {
        return centralizedOperationControl;
    }

    public void setCentralizedOperationControl(Character centralizedOperationControl) {
        this.centralizedOperationControl = centralizedOperationControl;
    }

    public Date getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    public char getActive() {
        return active;
    }

    public void setActive(char active) {
        this.active = active;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastLogintime() {
        return lastLogintime;
    }

    public void setLastLogintime(String lastLogintime) {
        this.lastLogintime = lastLogintime;
    }

    public Integer getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(Integer loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Character getUserDeleted() {
        return userDeleted;
    }

    public void setUserDeleted(Character userDeleted) {
        this.userDeleted = userDeleted;
    }

    public ApplicationUser getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(ApplicationUser approvedBy) {
        this.approvedBy = approvedBy;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public ApplicationUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(ApplicationUser createdBy) {
        this.createdBy = createdBy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ApplicationUser getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(ApplicationUser lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ApplicationUser getUserDeletedBy() {
        return userDeletedBy;
    }

    public void setUserDeletedBy(ApplicationUser userDeletedBy) {
        this.userDeletedBy = userDeletedBy;
    }

    public Date getUserdeletedDate() {
        return userdeletedDate;
    }

    public void setUserdeletedDate(Date userdeletedDate) {
        this.userdeletedDate = userdeletedDate;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ApplicationUser)) {
            return false;
        }
        ApplicationUser other = (ApplicationUser) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.f1soft.banksmart.auto.registration.entities.ApplicationUser[id=" + id + "]";
    }
}
