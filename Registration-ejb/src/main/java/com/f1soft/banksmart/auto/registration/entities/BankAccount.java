package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "BANK_ACCOUNT")
@NamedQueries({
    @NamedQuery(name = "BankAccount.findAll", query = "SELECT b FROM BankAccount b"),
    @NamedQuery(name = "BankAccount.findById", query = "SELECT b FROM BankAccount b WHERE b.id = :id"),
    @NamedQuery(name = "BankAccount.findByAccountNumber", query = "SELECT b FROM BankAccount b WHERE b.accountNumber = :accountNumber"),
    @NamedQuery(name = "BankAccount.findByAccountAlias", query = "SELECT b FROM BankAccount b WHERE b.accountAlias = :accountAlias"),
    @NamedQuery(name = "BankAccount.findByCustomerId", query = "SELECT b FROM BankAccount b WHERE b.customer.id = :customerId And b.active = :active"),
    @NamedQuery(name = "BankAccount.findByCustomerIdAndPrimary", query = "SELECT b FROM BankAccount b WHERE b.customer.id = :customerId And b.isPrimary = :isPrimary"),
    @NamedQuery(name = "BankAccount.findByIsPrimary", query = "SELECT b FROM BankAccount b WHERE b.isPrimary = :isPrimary")})
public class BankAccount implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 38, scale = 0)
    private Long id;
    @Basic(optional = false)
    @Column(name = "ACCOUNT_NUMBER", nullable = false, length = 50)
    private String accountNumber;
    @Column(name = "TXN_ENABLED", nullable = true, length = 2)
    private Character txnEnabled;
    @Column(name = "ALERT_ENABLED", nullable = true, length = 2)
    private Character alertEnabled;
    @Basic(optional = false)
    @Column(name = "ACCOUNT_ALIAS", nullable = false, length = 5)
    private String accountAlias;
    @Basic(optional = false)
    @Column(name = "IS_PRIMARY", nullable = false, length = 1)
    private char isPrimary;
    @Basic(optional = true)
    @Column(name = "IS_ACCOUNT_OWNER", nullable = true, length = 1)
    private char isAccountOwner;
    @Column(name = "ACCOUNT_OWNER_CBS_ID", length = 50)
    private String accountOwnerCBSId;
    @JoinColumn(name = "ACCOUNT_TYPE_ID", referencedColumnName = "ID")
    @ManyToOne(optional = true)
    private AccountType accountType;
    @Basic(optional = false)
    @JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne()
    private Customer customer;
    @JoinColumn(name = "BRANCH_ID")
    @ManyToOne(optional = false)
    private Branch branch;
    @Basic(optional = true)
    @Column(name = "CARD_NUMBER", nullable = true, length = 50)
    private String cardNumber;
    @Basic(optional = false)
    @Column(name = "ADDED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date addedDate;
    @JoinColumn(name = "ADDED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicationUser addedBy;
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;
    @JoinColumn(name = "LAST_MODIFIED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicationUser lastModifiedBy;
    @Column(name = "ACTIVE", length = 1)
    private Character active;
    @Column(name = "SERVICE_FLAG")
    private Character serviceFlag;
    @Column(name = "SERVICE_CODE", nullable = true, length = 5)
    private String serviceCode;
    @Column(name = "SERVICE_REMARKS", nullable = true, length = 80)
    private String serviceRemarks;
    @Column(name = "RENEW_DATE", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date renewDate;
    @Basic(optional = true)
    @Column(name = "EXPIRY_DATE", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiryDate;
    @Column(name = "SERVICE_BLOCK_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date serviceBlockDate;
    @Column(name = "IS_TRIAL_PERIOD", nullable = true, length = 1)
    private Character isTrialPeriod;

    public BankAccount() {
    }

    public BankAccount(Long id) {
        this.id = id;
    }

    public BankAccount(Long id, String accountNumber, String accountAlias, char isPrimary) {
        this.id = id;
        this.accountNumber = accountNumber;
        this.accountAlias = accountAlias;
        this.isPrimary = isPrimary;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountAlias() {
        return accountAlias;
    }

    public void setAccountAlias(String accountAlias) {
        this.accountAlias = accountAlias;
    }

    public char getIsPrimary() {
        return isPrimary;
    }

    public void setIsPrimary(char isPrimary) {
        this.isPrimary = isPrimary;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public String getAccountOwnerCBSId() {
        return accountOwnerCBSId;
    }

    public void setAccountOwnerCBSId(String accountOwnerCBSId) {
        this.accountOwnerCBSId = accountOwnerCBSId;
    }

    public char getIsAccountOwner() {
        return isAccountOwner;
    }

    public void setIsAccountOwner(char isAccountOwner) {
        this.isAccountOwner = isAccountOwner;
    }

    public Character getTxnEnabled() {
        return txnEnabled;
    }

    public void setTxnEnabled(Character txnEnabled) {
        this.txnEnabled = txnEnabled;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public ApplicationUser getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(ApplicationUser addedBy) {
        this.addedBy = addedBy;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public ApplicationUser getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(ApplicationUser lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Character getAlertEnabled() {
        return alertEnabled;
    }

    public void setAlertEnabled(Character alertEnabled) {
        this.alertEnabled = alertEnabled;
    }

    public Character getServiceFlag() {
        return serviceFlag;
    }

    public void setServiceFlag(Character serviceFlag) {
        this.serviceFlag = serviceFlag;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceRemarks() {
        return serviceRemarks;
    }

    public void setServiceRemarks(String serviceRemarks) {
        this.serviceRemarks = serviceRemarks;
    }

    public Date getRenewDate() {
        return renewDate;
    }

    public void setRenewDate(Date renewDate) {
        this.renewDate = renewDate;
    }

    public Date getServiceBlockDate() {
        return serviceBlockDate;
    }

    public void setServiceBlockDate(Date serviceBlockDate) {
        this.serviceBlockDate = serviceBlockDate;
    }

    public Character getIsTrialPeriod() {
        return isTrialPeriod;
    }

    public void setIsTrialPeriod(Character isTrialPeriod) {
        this.isTrialPeriod = isTrialPeriod;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BankAccount)) {
            return false;
        }
        BankAccount other = (BankAccount) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.f1soft.banksmart.auto.registration.entities.BankAccount[id=" + id + "]";
    }
}
