package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author bijay.shrestha
 */
@Entity
@Table(name = "CBS_CONNECTION")
@NamedQueries({
    @NamedQuery(name = "CBSConnection.findAll", query = "SELECT c from CBSConnection c"),
    @NamedQuery(name = "CBSConnection.findById", query = "SELECT c from CBSConnection c WHERE id=:id")
})
public class CBSConnection implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "CBS_CONNECTION_URL")
    private String cbsConnectionURL;
    @Column(name = "CBS_DRIVER_NAME")
    private String cbsDriverName;
    @Column(name = "CBS_USERNAME")
    private String cbsUsername;
    @Column(name = "CBS_PASSWORD")
    private String cbsPassword;
    @OneToMany(mappedBy = "cbsConnection")
    private List<CBSQuery> cbsQuery;

    public CBSConnection() {
    }

    public CBSConnection(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCbsConnectionURL() {
        return cbsConnectionURL;
    }

    public void setCbsConnectionURL(String cbsConnectionURL) {
        this.cbsConnectionURL = cbsConnectionURL;
    }

    public String getCbsDriverName() {
        return cbsDriverName;
    }

    public void setCbsDriverName(String cbsDriverName) {
        this.cbsDriverName = cbsDriverName;
    }

    public String getCbsUsername() {
        return cbsUsername;
    }

    public void setCbsUsername(String cbsUsername) {
        this.cbsUsername = cbsUsername;
    }

    public String getCbsPassword() {
        return cbsPassword;
    }

    public void setCbsPassword(String cbsPassword) {
        this.cbsPassword = cbsPassword;
    }

    public List<CBSQuery> getCbsQuery() {
        return cbsQuery;
    }

    public void setCbsQuery(List<CBSQuery> cbsQuery) {
        this.cbsQuery = cbsQuery;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CBSConnection)) {
            return false;
        }
        CBSConnection other = (CBSConnection) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.f1soft.fonebank.adminejb.entities.CBSConnection[ id=" + id + " ]";
    }
}
