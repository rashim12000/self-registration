package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Kailash
 */
@Entity
@Table(name = "CBS_QUERY")
@NamedQueries({
    @NamedQuery(name = "CBSQuery.findAll", query = "SELECT f FROM CBSQuery f"),
    @NamedQuery(name = "CBSQuery.findById", query = "SELECT f FROM CBSQuery f WHERE f.id = :id"),
    @NamedQuery(name = "CBSQuery.findByQueryCode", query = "SELECT f FROM CBSQuery f WHERE f.queryCode = :queryCode"),
    @NamedQuery(name = "CBSQuery.findBySqlQuery", query = "SELECT f FROM CBSQuery f WHERE f.sqlQuery = :sqlQuery"),
    @NamedQuery(name = "CBSQuery.findByQueryDescription", query = "SELECT f FROM CBSQuery f WHERE f.queryDescription = :queryDescription")})
public class CBSQuery implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "QUERY_CODE")
    private String queryCode;
    @Column(name = "SQL_QUERY")
    private String sqlQuery;
    @Column(name = "QUERY_DESCRIPTION")
    private String queryDescription;
    @Basic(optional = false)
    @JoinColumn(name = "CBS_CONNECTION_ID", referencedColumnName = "ID")
    @ManyToOne
    private CBSConnection cbsConnection;
    @Column(name = "CBS_DISTRIBUTION")
    private String cbsDistribution;

    public CBSQuery() {
    }

    public CBSQuery(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQueryCode() {
        return queryCode;
    }

    public void setQueryCode(String queryCode) {
        this.queryCode = queryCode;
    }

    public String getSqlQuery() {
        return sqlQuery;
    }

    public void setSqlQuery(String sqlQuery) {
        this.sqlQuery = sqlQuery;
    }

    public String getQueryDescription() {
        return queryDescription;
    }

    public void setQueryDescription(String queryDescription) {
        this.queryDescription = queryDescription;
    }

    public CBSConnection getCbsConnection() {
        return cbsConnection;
    }

    public void setCbsConnection(CBSConnection cbsConnection) {
        this.cbsConnection = cbsConnection;
    }

    public String getCbsDistribution() {
        return cbsDistribution;
    }

    public void setCbsDistribution(String cbsDistribution) {
        this.cbsDistribution = cbsDistribution;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CBSQuery)) {
            return false;
        }
        CBSQuery other = (CBSQuery) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.f1soft.fonebank.adminejb.entities[id=" + id + "]";
    }
}
