package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "CHANNEL_LOGIN_PATTERN")
@NamedQueries({
    @NamedQuery(name = "ChannelLoginPattern.findByAccessChannel", query = "SELECT c FROM ChannelLoginPattern c WHERE c.accessibleChannel.id=:accessibleChannelId")
})
public class ChannelLoginPattern implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @JoinColumn(name = "ACCESSIBLE_CHANNEL_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private AccessibleChannel accessibleChannel;
    @JoinColumn(name = "APPLICATION_PATTERN_ID", referencedColumnName = "ID")
    @ManyToOne()
    private ApplicationPattern applicationPattern;

    public ChannelLoginPattern() {
    }

    public ChannelLoginPattern(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AccessibleChannel getAccessibleChannel() {
        return accessibleChannel;
    }

    public void setAccessibleChannel(AccessibleChannel accessibleChannel) {
        this.accessibleChannel = accessibleChannel;
    }

    public ApplicationPattern getApplicationPattern() {
        return applicationPattern;
    }

    public void setApplicationPattern(ApplicationPattern applicationPattern) {
        this.applicationPattern = applicationPattern;
    }
}
