package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "CHANNEL_PROVIDER")
@NamedQueries({
    @NamedQuery(name = "ChannelProvider.findAll", query = "SELECT s FROM ChannelProvider s"),
    @NamedQuery(name = "ChannelProvider.findById", query = "SELECT s FROM ChannelProvider s WHERE s.id = :id"),
    @NamedQuery(name = "ChannelProvider.findByActive", query = "SELECT s FROM ChannelProvider s WHERE s.active = :active"),
    @NamedQuery(name = "ChannelProvider.findByServiceProviderName", query = "SELECT s FROM ChannelProvider s WHERE s.serviceProviderName = :serviceProviderName"),
    @NamedQuery(name = "ChannelProvider.findByAbbreviation", query = "SELECT s FROM ChannelProvider s WHERE s.abbreviation = :abbreviation")})
public class ChannelProvider implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;
    @Column(name = "SERVICE_PROVIDER_NAME", length = 50)
    private String serviceProviderName;
    @Column(name = "ABBREVIATION", length = 25)
    private String abbreviation;
    @Basic(optional = false)
    @Column(name = "ACTIVE", length = 1)
    private Character active;
    @OneToMany(mappedBy = "channelProvider", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<ApplicationPattern> applicationPatternList;

    public ChannelProvider() {
    }

    public ChannelProvider(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getServiceProviderName() {
        return serviceProviderName;
    }

    public void setServiceProviderName(String serviceProviderName) {
        this.serviceProviderName = serviceProviderName;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public List<ApplicationPattern> getApplicationPatternList() {
        return applicationPatternList;
    }

    public void setApplicationPatternList(List<ApplicationPattern> applicationPatternList) {
        this.applicationPatternList = applicationPatternList;
    }
}
