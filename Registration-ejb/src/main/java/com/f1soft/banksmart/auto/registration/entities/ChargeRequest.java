package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "CHARGE_REQUEST")
@NamedQueries({
    @NamedQuery(name = "ChargeRequest.findAll", query = "SELECT r FROM ChargeRequest r"),
    @NamedQuery(name = "ChargeRequest.findById", query = "SELECT r FROM ChargeRequest r WHERE r.id = :id")
})
public class ChargeRequest implements Serializable {

   private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 38, scale = 0)
    private Long id;
    @Column(name = "BANK_ACCOUNT_ID", nullable = true, precision = 38, scale = 0)
    private Long bankAccountId;
    @Column(name = "CHARGE_TYPE_ID", nullable = true, precision = 38, scale = 0)
    private Long chargeTypeId;
    @JoinColumn(name = "CHARGE_TYPE_ID", referencedColumnName = "ID", nullable = true, insertable = false, updatable = false)
    @ManyToOne()
    private ChargeType chargeType;
    @Column(name = "CHARGE_TYPE_CODE", length = 20)
    private String chargeTypeCode;
    @Basic(optional = true)
    @Column(name = "FROM_ACCOUNT", nullable = true, length = 50)
    private String fromAccount;
    @Basic(optional = true)
    @Column(name = "TO_ACCOUNT", nullable = true, length = 50)
    private String toAccount;
    @Column(name = "CHARGE_AMOUNT", precision = 17, scale = 2)
    private Double chargeAmount;
    @Column(name = "AMOUNT", precision = 17, scale = 2)
    private Double amount;
    @Column(name = "AMOUNT_CHANGED_REMARKS", length = 255)
    private String amountChangedRemarks;
    @Basic(optional = false)
    @Column(name = "INITIATOR_BRANCH_ID", nullable = false)
    private Long initiatorBranchId;
    @Basic(optional = false)
    @Column(name = "RECORDED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date recordedDate;
    @Column(name = "RECORDED_BY", nullable = false, precision = 22)
    private Long recordedBy;
    @Column(name = "PROCESSED_BY", nullable = true, precision = 22)
    private Long processedBy;
    @Column(name = "PROCESSED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date processedDate;
    @Column(name = "PROCESS_STATUS")
    private Integer processStatus;
    @Column(name = "DO_TXN", length = 1)
    private Character doTxn;
    @Column(name = "TXN_INITIATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date txnInitiatedDate;
    @Column(name = "EXPIRED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiredDate;
    @Column(name = "REMARKS", length = 255)
    private String remarks;
    @Column(name = "RENEW_INTERVAL")
    private Integer renewInterval;
    @Column(name = "EXPIRY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiryDate;
    @JoinColumn(name = "CHARGE_REQUEST_REF_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ChargeRequest chargeRequestRef;
    @Column(name = "CHARGE_GROUP", nullable = true)
    private String chargeGroup;
    @JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne()
    private Customer customer;
    
    public ChargeRequest() {
    }

    public ChargeRequest(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChargeTypeCode() {
        return chargeTypeCode;
    }

    public void setChargeTypeCode(String chargeTypeCode) {
        this.chargeTypeCode = chargeTypeCode;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(String fromAccount) {
        this.fromAccount = fromAccount;
    }

    public Integer getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(Integer processStatus) {
        this.processStatus = processStatus;
    }

    public Date getProcessedDate() {
        return processedDate;
    }

    public void setProcessedDate(Date processedDate) {
        this.processedDate = processedDate;
    }

    public Long getRecordedBy() {
        return recordedBy;
    }

    public void setRecordedBy(Long recordedBy) {
        this.recordedBy = recordedBy;
    }

    public Date getRecordedDate() {
        return recordedDate;
    }

    public void setRecordedDate(Date recordedDate) {
        this.recordedDate = recordedDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getToAccount() {
        return toAccount;
    }

    public void setToAccount(String toAccount) {
        this.toAccount = toAccount;
    }

    public Date getTxnInitiatedDate() {
        return txnInitiatedDate;
    }

    public void setTxnInitiatedDate(Date txnInitiatedDate) {
        this.txnInitiatedDate = txnInitiatedDate;
    }

    public String getAmountChangedRemarks() {
        return amountChangedRemarks;
    }

    public void setAmountChangedRemarks(String amountChangedRemarks) {
        this.amountChangedRemarks = amountChangedRemarks;
    }

    public Double getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(Double chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Long getInitiatorBranchId() {
        return initiatorBranchId;
    }

    public void setInitiatorBranchId(Long initiatorBranchId) {
        this.initiatorBranchId = initiatorBranchId;
    }

    public Long getProcessedBy() {
        return processedBy;
    }

    public void setProcessedBy(Long processedBy) {
        this.processedBy = processedBy;
    }

    public Character getDoTxn() {
        return doTxn;
    }

    public void setDoTxn(Character doTxn) {
        this.doTxn = doTxn;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Integer getRenewInterval() {
        return renewInterval;
    }

    public void setRenewInterval(Integer renewInterval) {
        this.renewInterval = renewInterval;
    }

    public ChargeRequest getChargeRequestRef() {
        return chargeRequestRef;
    }

    public void setChargeRequestRef(ChargeRequest chargeRequestRef) {
        this.chargeRequestRef = chargeRequestRef;
    }

    public Long getChargeTypeId() {
        return chargeTypeId;
    }

    public void setChargeTypeId(Long chargeTypeId) {
        this.chargeTypeId = chargeTypeId;
    }

    public Long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public ChargeType getChargeType() {
        return chargeType;
    }

    public void setChargeType(ChargeType chargeType) {
        this.chargeType = chargeType;
    }

    public String getChargeGroup() {
        return chargeGroup;
    }

    public void setChargeGroup(String chargeGroup) {
        this.chargeGroup = chargeGroup;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ChargeRequest)) {
            return false;
        }
        ChargeRequest other = (ChargeRequest) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.f1soft.banksmart.auto.registration.entities.ChargeRequest[id=" + id + "]";
    }
}
