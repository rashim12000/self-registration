package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "CHARGE_TYPE")
@NamedQueries({
    @NamedQuery(name = "ChargeType.findAll", query = "SELECT b FROM ChargeType b"),
    @NamedQuery(name = "ChargeType.findByStatus", query = "SELECT b FROM ChargeType b WHERE b.active = :active"),
    @NamedQuery(name = "ChargeType.findById", query = "SELECT b FROM ChargeType b WHERE b.id = :id"),
    @NamedQuery(name = "ChargeType.findByCode", query = "SELECT b FROM ChargeType b WHERE b.code = :code")
})
public class ChargeType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;
    @Basic(optional = false)
    @Column(name = "NAME", length = 80)
    private String name;
    @Basic(optional = false)
    @Column(name = "TYPE", length = 2)
    private String type;
    @Basic(optional = false)
    @Column(name = "CODE", length = 10)
    private String code;
    @Basic(optional = false)
    @Column(name = "ACTIVE", nullable = false, length = 1)
    private Character active;
    @Column(name = "CHARGE_REMARKS", length = 30)
    private String chargeRemarks;
    @Column(name = "FAILURE_CODE", length = 20)
    private String failureCode;
    @Column(name = "FAILURE_REMARKS", length = 255)
    private String failureRemarks;

    public ChargeType() {
    }

    public ChargeType(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChargeRemarks() {
        return chargeRemarks;
    }

    public void setChargeRemarks(String chargeRemarks) {
        this.chargeRemarks = chargeRemarks;
    }

    public String getFailureCode() {
        return failureCode;
    }

    public void setFailureCode(String failureCode) {
        this.failureCode = failureCode;
    }

    public String getFailureRemarks() {
        return failureRemarks;
    }

    public void setFailureRemarks(String failureRemarks) {
        this.failureRemarks = failureRemarks;
    }

}
