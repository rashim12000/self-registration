package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "CUSTOMER")
@NamedQueries({
    @NamedQuery(name = "Customer.findAll", query = "SELECT c FROM Customer c"),
    @NamedQuery(name = "Customer.findById", query = "SELECT c FROM Customer c WHERE c.id = :id"),
    @NamedQuery(name = "Customer.findByFirstName", query = "SELECT c FROM Customer c WHERE c.firstName = :firstName"),
    @NamedQuery(name = "Customer.findByMiddleName", query = "SELECT c FROM Customer c WHERE c.middleName = :middleName"),
    @NamedQuery(name = "Customer.findByLastName", query = "SELECT c FROM Customer c WHERE c.lastName = :lastName"),
    @NamedQuery(name = "Customer.findByAddress", query = "SELECT c FROM Customer c WHERE c.address = :address"),
    @NamedQuery(name = "Customer.findByActive", query = "SELECT c FROM Customer c WHERE c.active = :active"),
    @NamedQuery(name = "Customer.findCustomerByApprovedAndBranch", query = "SELECT c FROM Customer c WHERE c.approved = :approved AND c.branchId = :branchId"),
    @NamedQuery(name = "Customer.findByApproved", query = "SELECT c FROM Customer c WHERE c.approved = :approved"),
    @NamedQuery(name = "Customer.findByContactNo", query = "SELECT c FROM Customer c WHERE c.contactNo = :contactNo")})
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 38, scale = 0)
    private Long id;
    @Column(name = "FIRST_NAME", length = 50)
    private String firstName;
    @Column(name = "MIDDLE_NAME", length = 50)
    private String middleName;
    @Column(name = "LAST_NAME", length = 50)
    private String lastName;
    @Column(name = "ADDRESS", length = 100)
    private String address;
    @Column(name = "EMAIL_ADDRESS", length = 100)
    private String emailAddress;
    @Column(name = "ACTIVE")
    private Character active;
    @Basic(optional = false)
    @Column(name = "BRANCH_ID", nullable = false)
    private Long branchId;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne(optional = false)
    private ApplicationUser createdBy;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "REJECT_REMARKS", nullable = true, length = 255)
    private String rejectRemarks;
    @Column(name = "APPROVED", length = 1)
    private Character approved;
    @Column(name = "GENDER", length = 2)
    private String gender;
    @JoinColumn(name = "APPROVED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne(optional = true)
    private ApplicationUser approvedBy;
    @Column(name = "APPROVED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date approvedDate;
    @JoinColumn(name = "REJECTED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne(optional = true)
    private ApplicationUser rejectedBy;
    @Column(name = "REJECTED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date rejectedDate;
    @JoinColumn(name = "DELETED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne(optional = true)
    private ApplicationUser deletedBy;
    @Column(name = "DELETED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedDate;
    @Column(name = "DELETE_REMARKS", length = 255)
    private String deleteRemarks;
    @JoinColumn(name = "DELETE_APPROVED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne(optional = true)
    private ApplicationUser deletedApprovedBy;
    @Column(name = "DELETED_APPROVED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedApprovedDate;
    @Column(name = "EDIT_APPROVED_BY", length = 25)
    private String editApprovedBy;
    @Column(name = "EDIT_APPROVED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date editApprovedDate;
    @Column(name = "LAST_MODIFIED_REMARKS", length = 100)
    private String lastModifiedRemarks;
    @Column(name = "LATEST_ACTIVITY_REMARKS", length = 100)
    private String latestActivityRemarks;
    @JoinColumn(name = "LAST_MODIFIED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne(optional = true)
    private ApplicationUser lastModifiedBy;
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;
    @Column(name = "CONTACT_NO", length = 15)
    private String contactNo;
    @Column(name = "MOBILE_NUMBER", length = 15)
    private String mobileNumber;
    @Column(name = "LATEST_CHANGED_MOBILE", length = 15)
    private String latestChangedMobile;
    @Column(name = "CBS_ID", length = 50)
    private String cbsId;
    @Column(name = "AC_HOLDER", length = 1)
    private char acHolder;
    @OneToMany(mappedBy = "customer", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<BankAccount> bankAccountList;
    @OneToMany(mappedBy = "customer", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<CustomerLinkedAccount> customerLinkedAccountList;
    @OneToMany(mappedBy = "customer", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<CustomerLogin> customerLoginList;
    @JoinColumn(name = "BRANCH_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne()
    private Branch branch;
    @JoinColumn(name = "PROFILE_ID", referencedColumnName = "ID", nullable = true)
    @OneToOne()
    private Profile profile;

    public Customer() {
    }

    public Customer(Long id) {
        this.id = id;
    }

    public String getFullname() {
        StringBuilder fullName = new StringBuilder(firstName);
        if (middleName != null) {
            fullName.append(" ").append(middleName).append(" ").append(lastName);
        } else {
            fullName.append(" ").append(lastName);
        }
        return fullName.toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getCbsId() {
        return cbsId;
    }

    public void setCbsId(String cbsId) {
        this.cbsId = cbsId;
    }

    public String getRejectRemarks() {
        return rejectRemarks;
    }

    public void setRejectRemarks(String rejectRemarks) {
        this.rejectRemarks = rejectRemarks;
    }

    public Date getRejectedDate() {
        return rejectedDate;
    }

    public void setRejectedDate(Date rejectedDate) {
        this.rejectedDate = rejectedDate;
    }

    public String getEditApprovedBy() {
        return editApprovedBy;
    }

    public void setEditApprovedBy(String editApprovedBy) {
        this.editApprovedBy = editApprovedBy;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public ApplicationUser getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(ApplicationUser approvedBy) {
        this.approvedBy = approvedBy;
    }

    public ApplicationUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(ApplicationUser createdBy) {
        this.createdBy = createdBy;
    }

    public ApplicationUser getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(ApplicationUser lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public ApplicationUser getRejectedBy() {
        return rejectedBy;
    }

    public void setRejectedBy(ApplicationUser rejectedBy) {
        this.rejectedBy = rejectedBy;
    }

    public List<BankAccount> getBankAccountList() {
        return bankAccountList;
    }

    public void setBankAccountList(List<BankAccount> bankAccountList) {
        this.bankAccountList = bankAccountList;
    }

    public List<CustomerLinkedAccount> getCustomerLinkedAccountList() {
        return customerLinkedAccountList;
    }

    public void setCustomerLinkedAccountList(List<CustomerLinkedAccount> customerLinkedAccountList) {
        this.customerLinkedAccountList = customerLinkedAccountList;
    }

    public List<CustomerLogin> getCustomerLoginList() {
        return customerLoginList;
    }

    public void setCustomerLoginList(List<CustomerLogin> customerLoginList) {
        this.customerLoginList = customerLoginList;
    }

    public Character getApproved() {
        return approved;
    }

    public void setApproved(Character approved) {
        this.approved = approved;
    }

    public ApplicationUser getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(ApplicationUser deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public Date getDeletedApprovedDate() {
        return deletedApprovedDate;
    }

    public void setDeletedApprovedDate(Date deletedApprovedDate) {
        this.deletedApprovedDate = deletedApprovedDate;
    }

    public ApplicationUser getDeletedApprovedBy() {
        return deletedApprovedBy;
    }

    public void setDeletedApprovedBy(ApplicationUser deletedApprovedBy) {
        this.deletedApprovedBy = deletedApprovedBy;
    }

    public Date getEditApprovedDate() {
        return editApprovedDate;
    }

    public void setEditApprovedDate(Date editApprovedDate) {
        this.editApprovedDate = editApprovedDate;
    }

    public String getLastModifiedRemarks() {
        return lastModifiedRemarks;
    }

    public void setLastModifiedRemarks(String lastModifiedRemarks) {
        this.lastModifiedRemarks = lastModifiedRemarks;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public String getLatestActivityRemarks() {
        return latestActivityRemarks;
    }

    public void setLatestActivityRemarks(String latestActivityRemarks) {
        this.latestActivityRemarks = latestActivityRemarks;
    }

    public String getDeleteRemarks() {
        return deleteRemarks;
    }

    public void setDeleteRemarks(String deleteRemarks) {
        this.deleteRemarks = deleteRemarks;
    }

    public char getAcHolder() {
        return acHolder;
    }

    public void setAcHolder(char acHolder) {
        this.acHolder = acHolder;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getLatestChangedMobile() {
        return latestChangedMobile;
    }

    public void setLatestChangedMobile(String latestChangedMobile) {
        this.latestChangedMobile = latestChangedMobile;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Customer)) {
            return false;
        }
        Customer other = (Customer) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.f1soft.banksmart.auto.registration.entities.Customer[id=" + id + "]";
    }
}
