package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "CUSTOMER_LINKED_ACCOUNT")
@NamedQueries({
    @NamedQuery(name = "CustomerLinkedAccount.findAll", query = "SELECT c FROM CustomerLinkedAccount c"),
    @NamedQuery(name = "CustomerLinkedAccount.findById", query = "SELECT c FROM CustomerLinkedAccount c WHERE c.id = :id"),
    @NamedQuery(name = "CustomerLinkedAccount.findByAccountNumber", query = "SELECT c FROM CustomerLinkedAccount c WHERE c.accountNumber = :accountNumber"),
    @NamedQuery(name = "CustomerLinkedAccount.findByCustomerId", query = "SELECT c FROM  CustomerLinkedAccount c WHERE c.customer= :customer And c.active = :active"),
    @NamedQuery(name = "CustomerLinkedAccount.findByAccountAlias", query = "SELECT c FROM CustomerLinkedAccount c WHERE c.accountAlias = :accountAlias")})
public class CustomerLinkedAccount implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @Column(name = "ACCOUNT_NUMBER", length = 50)
    private String accountNumber;
    @Column(name = "ACCOUNT_ALIAS", length = 5)
    private String accountAlias;
    @Column(name = "ACCOUNT_OWNER_CBS_ID", length = 50)
    private String accountOwnerCBSId;
    @JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Customer customer;
    @Column(name = "ACTIVE", length = 1)
    private Character active;

    public CustomerLinkedAccount() {
    }

    public CustomerLinkedAccount(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountAlias() {
        return accountAlias;
    }

    public void setAccountAlias(String accountAlias) {
        this.accountAlias = accountAlias;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public String getAccountOwnerCBSId() {
        return accountOwnerCBSId;
    }

    public void setAccountOwnerCBSId(String accountOwnerCBSId) {
        this.accountOwnerCBSId = accountOwnerCBSId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CustomerLinkedAccount)) {
            return false;
        }
        CustomerLinkedAccount other = (CustomerLinkedAccount) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.f1soft.banksmart.auto.registration.entities.CustomerLinkedAccount[id=" + id + "]";
    }
}
