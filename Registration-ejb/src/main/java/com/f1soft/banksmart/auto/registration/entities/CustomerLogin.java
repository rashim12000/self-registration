package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "CUSTOMER_LOGIN")
@NamedQueries({
    @NamedQuery(name = "CustomerLogin.findAll", query = "SELECT c FROM CustomerLogin c"),
    @NamedQuery(name = "CustomerLogin.findById", query = "SELECT c FROM CustomerLogin c WHERE c.id = :id"),
    @NamedQuery(name = "CustomerLogin.findByUsername", query = "SELECT c FROM CustomerLogin c WHERE c.username = :username"),
    @NamedQuery(name = "CustomerLogin.findByPassword", query = "SELECT c FROM CustomerLogin c WHERE c.password = :password"),
    @NamedQuery(name = "CustomerLogin.findUnDeletedUserWithId", query = "SELECT c FROM CustomerLogin c WHERE ((UPPER(c.username) = :username or UPPER(c.latestChangedUsername) = :username)) and c.id != :id and c.customerDelete = 'N'"),
    @NamedQuery(name = "CustomerLogin.findUnDeletedUser", query = "SELECT c FROM CustomerLogin c WHERE ((UPPER(c.username) = :username or UPPER(c.latestChangedUsername) = :username)) and c.customerDelete = 'N'"),
    @NamedQuery(name = "CustomerLogin.findUnDeletedUserByUsername", query = "SELECT c FROM CustomerLogin c WHERE c.username = :username AND (c.customerDelete IS NULL OR c.customerDelete != 'Y')"),
    @NamedQuery(name = "CustomerLogin.findByActive", query = "SELECT c FROM CustomerLogin c WHERE c.active = :active")
})
public class CustomerLogin implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 38, scale = 0)
    private Long id;
    @Basic(optional = false)
    @Column(name = "USERNAME", nullable = false, length = 20)
    private String username;
    @Column(name = "PASSWORD", length = 200)
    private String password;
    @Column(name = "TXN_PASSWORD", length = 200)
    private String txnPassword;
    @Column(name = "ACTIVE")
    private Character active;
    @Column(name = "PASSWORD_FLAG")
    private String passwordFlag;
    @Column(name = "CUSTOMER_ID", insertable = false, updatable = false)
    private Long customerId;
    @Column(name = "CUSTOMER_DELETE")
    private Character customerDelete;
    @Column(name = "LATEST_CHANGED_USERNAME", nullable = true, length = 20)
    private String latestChangedUsername;
    @Column(name = "LAST_MODIFIED_DATE", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date lastModifiedDate;
    @JoinColumn(name = "LAST_MODIFIED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne(optional = true)
    private ApplicationUser lastModifiedBy;
    @JoinColumn(name = "ADDED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne(optional = true)
    private ApplicationUser addedBy;
    @Column(name = "ADDED_DATE", nullable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date addedDate;
    @JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "ID")
    @ManyToOne()
    private Customer customer;
    @JoinColumn(name = "APPLICATION_PATTERN_ID")
    @ManyToOne()
    private ApplicationPattern applicationPattern;
    @Column(name = "IS_INITIAL_PASSWORD", length = 1)
    private Character isInitialPassword;
    @Column(name = "IS_INITIAL_TXN_PASSWORD", length = 1)
    private Character isInitialTxnPassword;
    @Column(name = "LAST_LOGIN_PASSWORD_CHANGED", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLoginPasswordChanged;
    @Column(name = "LAST_TXN_PASSWORD_CHANGED", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastTxnPasswordChanged;
    @Column(name = "IS_PRIMARY")
    private Character isPrimary;
    @OneToMany(mappedBy = "customerLogin", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<LoginAccessChannel> loginAccessChannels;

    public CustomerLogin() {
    }

    public CustomerLogin(Long id) {
        this.id = id;
    }

    public CustomerLogin(Long id, String username) {
        this.id = id;
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public String getLatestChangedUsername() {
        return latestChangedUsername;
    }

    public void setLatestChangedUsername(String latestChangedUsername) {
        this.latestChangedUsername = latestChangedUsername;
    }

    public Character getCustomerDelete() {
        return customerDelete;
    }

    public void setCustomerDelete(Character customerDelete) {
        this.customerDelete = customerDelete;
    }

    public Long getCustomerID() {
        return customerId;
    }

    public void setCustomerID(Long customerID) {
        this.customerId = customerID;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public ApplicationPattern getApplicationPattern() {
        return applicationPattern;
    }

    public void setApplicationPattern(ApplicationPattern applicationPattern) {
        this.applicationPattern = applicationPattern;
    }

    public String getPasswordFlag() {
        return passwordFlag;
    }

    public void setPasswordFlag(String passwordFlag) {
        this.passwordFlag = passwordFlag;
    }

    public ApplicationUser getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(ApplicationUser addedBy) {
        this.addedBy = addedBy;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public Character getIsInitialPassword() {
        return isInitialPassword;
    }

    public void setIsInitialPassword(Character isInitialPassword) {
        this.isInitialPassword = isInitialPassword;
    }

    public String getTxnPassword() {
        return txnPassword;
    }

    public void setTxnPassword(String txnPassword) {
        this.txnPassword = txnPassword;
    }

    public Character getIsInitialTxnPassword() {
        return isInitialTxnPassword;
    }

    public void setIsInitialTxnPassword(Character isInitialTxnPassword) {
        this.isInitialTxnPassword = isInitialTxnPassword;
    }

    public Date getLastLoginPasswordChanged() {
        return lastLoginPasswordChanged;
    }

    public void setLastLoginPasswordChanged(Date lastLoginPasswordChanged) {
        this.lastLoginPasswordChanged = lastLoginPasswordChanged;
    }

    public Date getLastTxnPasswordChanged() {
        return lastTxnPasswordChanged;
    }

    public void setLastTxnPasswordChanged(Date lastTxnPasswordChanged) {
        this.lastTxnPasswordChanged = lastTxnPasswordChanged;
    }

    public Character getIsPrimary() {
        return isPrimary;
    }

    public void setIsPrimary(Character isPrimary) {
        this.isPrimary = isPrimary;
    }

    public List<LoginAccessChannel> getLoginAccessChannels() {
        return loginAccessChannels;
    }

    public void setLoginAccessChannels(List<LoginAccessChannel> loginAccessChannels) {
        this.loginAccessChannels = loginAccessChannels;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public ApplicationUser getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(ApplicationUser lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CustomerLogin)) {
            return false;
        }
        CustomerLogin other = (CustomerLogin) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.f1soft.banksmart.auto.registration.entities.CustomerLogin[id=" + id + "]";
    }
}
