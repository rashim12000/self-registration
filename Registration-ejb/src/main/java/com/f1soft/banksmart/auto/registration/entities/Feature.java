package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "FEATURE")
@NamedQueries({
    @NamedQuery(name = "Feature.findAll", query = "SELECT s FROM Feature s"),
    @NamedQuery(name = "Feature.findByFeaturekey", query = "SELECT s FROM Feature s WHERE s.featurekey = :featurekey ")
})
public class Feature implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;
    @Column(name = "NAME", length = 80)
    private String name;
    @Column(name = "FEATURE_KEY", length = 10)
    private String featurekey;
    @Column(name = "ACTIVE", length = 1)
    private Character active;
    @Column(name = "TXN_INVOLVED", length = 1)
    private Character txnInvolved;
    @Column(name = "COMMISSION")
    private Character commission;
    @Column(name = "ALLOW_MULTIPLE_PARTNER")
    private Character allowMultiplePartner;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "feature")
    @Fetch(FetchMode.SUBSELECT)
    List<SmsKeyword> smsKeywords;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "feature")
    @Fetch(FetchMode.SUBSELECT)
    List<SubFeature> subFeatures;

    public Feature() {
    }

    public Feature(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public String getFeaturekey() {
        return featurekey;
    }

    public void setFeaturekey(String featurekey) {
        this.featurekey = featurekey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Character getTxnInvolved() {
        return txnInvolved;
    }

    public void setTxnInvolved(Character txnInvolved) {
        this.txnInvolved = txnInvolved;
    }

    public Character getCommission() {
        return commission;
    }

    public void setCommission(Character commission) {
        this.commission = commission;
    }

    public Character getAllowMultiplePartner() {
        return allowMultiplePartner;
    }

    public void setAllowMultiplePartner(Character allowMultiplePartner) {
        this.allowMultiplePartner = allowMultiplePartner;
    }

    public List<SmsKeyword> getSmsKeywords() {
        return smsKeywords;
    }

    public void setSmsKeywords(List<SmsKeyword> smsKeywords) {
        this.smsKeywords = smsKeywords;
    }

    public List<SubFeature> getSubFeatures() {
        return subFeatures;
    }

    public void setSubFeatures(List<SubFeature> subFeatures) {
        this.subFeatures = subFeatures;
    }
}
