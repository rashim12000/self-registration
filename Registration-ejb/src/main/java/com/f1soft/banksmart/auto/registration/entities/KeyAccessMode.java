package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "KEY_ACCESS_MODE")
@NamedQueries({
    @NamedQuery(name = "KeyAccessMode.findAll", query = "SELECT k from KeyAccessMode k"),
    @NamedQuery(name = "KeyAccessMode.findById", query = "SELECT k from KeyAccessMode k where k.id= :id"),
    @NamedQuery(name = "KeyAccessMode.findByAbbreviation", query = "SELECT k from KeyAccessMode k where k.abbreviation= :abbreviation")
})
public class KeyAccessMode implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;
    @Column(name = "NAME", length = 20)
    private String name;
    @Column(name = "ABBREVIATION")
    private String abbreviation;

    public KeyAccessMode() {
    }

    public KeyAccessMode(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KeyAccessMode)) {
            return false;
        }
        KeyAccessMode other = (KeyAccessMode) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.f1soft.banksmart.auto.registration.entities.FeatureAccessibleMode[ id=" + id + " ]";
    }
}
