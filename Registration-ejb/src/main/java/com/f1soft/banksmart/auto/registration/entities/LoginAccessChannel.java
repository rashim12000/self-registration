package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "LOGIN_ACCESS_CHANNEL")
@NamedQueries({
    @NamedQuery(name = "LoginAccessChannel.findAll", query = "SELECT b FROM LoginAccessChannel b"),
    @NamedQuery(name = "LoginAccessChannel.findById", query = "SELECT b FROM LoginAccessChannel b WHERE b.id = :id"),
    @NamedQuery(name = "LoginAccessChannel.findByCustomerLoginId", query = "SELECT b FROM LoginAccessChannel b WHERE b.customerLogin.id = :customerLoginId")})
public class LoginAccessChannel implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 38, scale = 0)
    private Long id;
    @Basic(optional = false)
    @JoinColumn(name = "ACCESSIBLE_CHANNEL_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne()
    private AccessibleChannel accessibleChannel;
    @JoinColumn(name = "CUSTOMER_LOGIN_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private CustomerLogin customerLogin;
    @Basic(optional = false)
    @Column(name = "ADDED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date addedDate;
    @JoinColumn(name = "ADDED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicationUser addedBy;
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;
    @JoinColumn(name = "LAST_MODIFIED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicationUser lastModifiedBy;
    @Column(name = "ACTIVE", length = 1)
    private Character active;
    @Column(name = "LOGIN_BLOCK_DATE")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date loginBlockDate;
    @Column(name = "REQUEST_ATTEMPT")
    private Integer requestAttempt;
    @Column(name = "LOGIN_BLOCKED")
    private String loginBlocked;
    @Column(name = "LOGIN_BLOCK_REMARKS", nullable = true, length = 80)
    private String loginBlockRemarks;

    public LoginAccessChannel() {
    }

    public LoginAccessChannel(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public ApplicationUser getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(ApplicationUser addedBy) {
        this.addedBy = addedBy;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public ApplicationUser getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(ApplicationUser lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public AccessibleChannel getAccessibleChannel() {
        return accessibleChannel;
    }

    public void setAccessibleChannel(AccessibleChannel accessibleChannel) {
        this.accessibleChannel = accessibleChannel;
    }

    public CustomerLogin getCustomerLogin() {
        return customerLogin;
    }

    public void setCustomerLogin(CustomerLogin customerLogin) {
        this.customerLogin = customerLogin;
    }

    public Date getLoginBlockDate() {
        return loginBlockDate;
    }

    public void setLoginBlockDate(Date loginBlockDate) {
        this.loginBlockDate = loginBlockDate;
    }

    public Integer getRequestAttempt() {
        return requestAttempt;
    }

    public void setRequestAttempt(Integer requestAttempt) {
        this.requestAttempt = requestAttempt;
    }

    public String getLoginBlocked() {
        return loginBlocked;
    }

    public void setLoginBlocked(String loginBlocked) {
        this.loginBlocked = loginBlocked;
    }

    public String getLoginBlockRemarks() {
        return loginBlockRemarks;
    }

    public void setLoginBlockRemarks(String loginBlockRemarks) {
        this.loginBlockRemarks = loginBlockRemarks;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LoginAccessChannel)) {
            return false;
        }
        LoginAccessChannel other = (LoginAccessChannel) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.f1soft.banksmart.auto.registration.entities.LoginAccessChannel[id=" + id + "]";
    }
}
