package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "LOGIN_TYPE")
@NamedQueries({
    @NamedQuery(name = "LoginType.findAll", query = "SELECT l FROM LoginType l"),
    @NamedQuery(name = "LoginType.findByNotifier", query = "SELECT l FROM LoginType l where l.isNotifier =:isNotifier")
})
public class LoginType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 12)
    private Long id;
    @Basic(optional = false)
    @Column(name = "TYPE", nullable = true, length = 20)
    private String type;
    @Column(name = "IS_NOTIFIER")
    private Character isNotifier;

    public LoginType() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Character getIsNotifier() {
        return isNotifier;
    }

    public void setIsNotifier(Character isNotifier) {
        this.isNotifier = isNotifier;
    }
}
