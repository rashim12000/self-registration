package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "MESSAGE_FORMAT")
@NamedQueries({
    @NamedQuery(name = "MessageFormat.findAll", query = "SELECT m FROM MessageFormat m order by m.message asc"),
    @NamedQuery(name = "MessageFormat.findById", query = "SELECT m FROM MessageFormat m WHERE m.id = :id"),
    @NamedQuery(name = "MessageFormat.findByMessage", query = "SELECT m FROM MessageFormat m WHERE m.message = :message"),
    @NamedQuery(name = "MessageFormat.findByMessageType", query = "SELECT m FROM MessageFormat m WHERE m.messageType = :messageType"),
    @NamedQuery(name = "MessageFormat.findByMessageCode", query = "SELECT m FROM MessageFormat m WHERE m.messageCode = :messageCode"),
    @NamedQuery(name = "MessageFormat.findByDescription", query = "SELECT m FROM MessageFormat m WHERE m.description = :description")})
public class MessageFormat implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 19, scale = 0)
    private Long id;
    @Basic(optional = false)
    @Column(name = "MESSAGE", nullable = false, length = 250)
    private String message;
    @Basic(optional = false)
    @Column(name = "MESSAGE_CODE", nullable = false, length = 50)
    private String messageCode;
    @Column(name = "DESCRIPTION", length = 200)
    private String description;
    @Column(name = "PROVIDED_MODIFY_LENGTH")
    private Integer modifyLength;
    @OneToMany(mappedBy = "messageFormat", fetch = FetchType.LAZY)
    private List<SmsSent> smsSentCollection;
    @JoinColumn(name = "MESSAGE_TYPE", referencedColumnName = "ID")
    @ManyToOne()
    private MessageType messageType;
    @Transient
    private Long messageTypeID;

    public MessageFormat() {
    }

    public MessageFormat(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<SmsSent> getSmsSentCollection() {
        return smsSentCollection;
    }

    public void setSmsSentCollection(List<SmsSent> smsSentCollection) {
        this.smsSentCollection = smsSentCollection;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public Integer getModifyLength() {
        return modifyLength;
    }

    public void setModifyLength(Integer modifyLength) {
        this.modifyLength = modifyLength;
    }

    public Long getMessageTypeID() {
        return messageTypeID;
    }

    public void setMessageTypeID(Long messageTypeID) {
        this.messageTypeID = messageTypeID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MessageFormat)) {
            return false;
        }
        MessageFormat other = (MessageFormat) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.f1soft.banksmart.auto.registration.entities.MessageFormat[id=" + id + "]";
    }
}
