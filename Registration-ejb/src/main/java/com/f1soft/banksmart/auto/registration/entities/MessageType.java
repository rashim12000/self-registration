package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "MESSAGE_TYPE")
@NamedQueries({
    @NamedQuery(name = "MessageType.findAll", query = "SELECT m FROM MessageType m"),
    @NamedQuery(name = "MessageType.findById", query = "SELECT m FROM MessageType m WHERE m.id = :id"),
    @NamedQuery(name = "MessageType.findByType", query = "SELECT m FROM MessageType m WHERE m.type = :type"),
    @NamedQuery(name = "MessageType.findByDescription", query = "SELECT m FROM MessageType m WHERE m.description = :description")
})
@SqlResultSetMapping(name = "smsReceivedTypeNCount",
entities =
@EntityResult(entityClass = MessageType.class),
columns = {
    @ColumnResult(name = "NtcCount"),
    @ColumnResult(name = "NcellCount")})
public class MessageType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @Basic(optional = false)
    @Column(name = "TYPE", nullable = false, length = 50)
    private String type;
    @Basic(optional = false)
    @Column(name = "DESCRIPTION", nullable = false, length = 200)
    private String description;
    @OneToMany(mappedBy = "messageType", fetch = FetchType.LAZY)
    private List<SmsSent> smsSentCollection;
    @OneToMany(mappedBy = "messageType", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<MessageFormat> messageFormatCollection;
    @Transient
    private int count;
    @Transient
    private int ntcCount;
    @Transient
    private int ncellCount;

    public MessageType() {
    }

    public MessageType(Long id) {
        this.id = id;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<SmsSent> getSmsSentCollection() {
        return smsSentCollection;
    }

    public void setSmsSentCollection(List<SmsSent> smsSentCollection) {
        this.smsSentCollection = smsSentCollection;
    }

    public List<MessageFormat> getMessageFormatCollection() {
        return messageFormatCollection;
    }

    public void setMessageFormatCollection(List<MessageFormat> messageFormatCollection) {
        this.messageFormatCollection = messageFormatCollection;
    }

    public int getNcellCount() {
        return ncellCount;
    }

    public void setNcellCount(int ncellCount) {
        this.ncellCount = ncellCount;
    }

    public int getNtcCount() {
        return ntcCount;
    }

    public void setNtcCount(int ntcCount) {
        this.ntcCount = ntcCount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MessageType)) {
            return false;
        }
        MessageType other = (MessageType) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.f1soft.banksmart.auto.registration.entities.MessageType[id=" + id + "]";
    }
}
