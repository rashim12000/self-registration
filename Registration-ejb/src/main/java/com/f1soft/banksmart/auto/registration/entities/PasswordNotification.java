package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "PASSWORD_NOTIFICATION")
public class PasswordNotification implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @JoinColumn(name = "APPLICATION_PATTERN_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicationPattern applicationPattern;
    @Column(name = "NOTIFY_IN", nullable = true, length = 20)
    private String notifyIn;

    public PasswordNotification() {
    }

    public PasswordNotification(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ApplicationPattern getApplicationPattern() {
        return applicationPattern;
    }

    public void setApplicationPattern(ApplicationPattern applicationPattern) {
        this.applicationPattern = applicationPattern;
    }

    public String getNotifyIn() {
        return notifyIn;
    }

    public void setNotifyIn(String notifyIn) {
        this.notifyIn = notifyIn;
    }
}
