package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "PROFILE")
@NamedQueries({
    @NamedQuery(name = "Profile.findAll", query = "SELECT b FROM Profile b"),
    @NamedQuery(name = "Profile.findById", query = "SELECT b FROM Profile b WHERE b.id = :id"),
    @NamedQuery(name = "Profile.findByTypeAndName", query = "SELECT b FROM Profile b where b.profileType = :profileType and UPPER(b.profileName) =:profileName"),
    @NamedQuery(name = "Profile.findUndeletedProfile", query = "SELECT b FROM Profile b where b.profileType = :profileType and b.active !='D'"),
    @NamedQuery(name = "Profile.findByName", query = "SELECT b FROM Profile b where b.profileName=:profileName")
})
public class Profile implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 38, scale = 0)
    private Long id;
    @Basic(optional = false)
    @Column(name = "PROFILE_NAME", nullable = false, length = 80)
    private String profileName;
    @Column(name = "PROFILE_DESCRIPTION", length = 255)
    private String profileDescription;
    @Column(name = "REMARKS", length = 255)
    private String remarks;
    @Basic(optional = false)
    @Column(name = "PROFILE_TYPE", nullable = false)
    private String profileType;
    @Basic(optional = false)
    @Column(name = "ACTIVE", nullable = false)
    private Character active;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicationUser createdBy;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @JoinColumn(name = "LAST_MODIFIED_BY", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicationUser lastModifiedBy;
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;
    @OneToOne(mappedBy = "profile")
    private ProfileAttribute profileAttribute;
    @OneToOne(mappedBy = "profile")
    private ProfileMainTxnLimit profileMainTxnLimit;
    @OneToMany(mappedBy = "profile", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<ProfileFeature> profileFeatures;
    @OneToMany(mappedBy = "profile", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<ProfileAccessibleChannel> profileAccessibleChannelList;
    @OneToMany(mappedBy = "profile", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<ProfileCharge> profileChargeList;

    public Profile() {
    }

    public Profile(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public ApplicationUser getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(ApplicationUser lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getProfileDescription() {
        return profileDescription;
    }

    public void setProfileDescription(String profileDescription) {
        this.profileDescription = profileDescription;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getProfileType() {
        return profileType;
    }

    public void setProfileType(String profileType) {
        this.profileType = profileType;
    }

    public ApplicationUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(ApplicationUser createdBy) {
        this.createdBy = createdBy;
    }

    public ProfileAttribute getProfileAttribute() {
        return profileAttribute;
    }

    public void setProfileAttribute(ProfileAttribute profileAttribute) {
        this.profileAttribute = profileAttribute;
    }

    public ProfileMainTxnLimit getProfileMainTxnLimit() {
        return profileMainTxnLimit;
    }

    public void setProfileMainTxnLimit(ProfileMainTxnLimit profileMainTxnLimit) {
        this.profileMainTxnLimit = profileMainTxnLimit;
    }

    public List<ProfileAccessibleChannel> getProfileAccessibleChannelList() {
        return profileAccessibleChannelList;
    }

    public void setProfileAccessibleChannelList(List<ProfileAccessibleChannel> profileAccessibleChannelList) {
        this.profileAccessibleChannelList = profileAccessibleChannelList;
    }

    public List<ProfileCharge> getProfileChargeList() {
        return profileChargeList;
    }

    public void setProfileChargeList(List<ProfileCharge> profileChargeList) {
        this.profileChargeList = profileChargeList;
    }

    public List<ProfileFeature> getProfileFeatures() {
        return profileFeatures;
    }

    public void setProfileFeatures(List<ProfileFeature> profileFeatures) {
        this.profileFeatures = profileFeatures;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Profile)) {
            return false;
        }
        Profile other = (Profile) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.f1soft.banksmart.auto.registration.entities.Profile[ id=" + id + " ]";
    }
}
