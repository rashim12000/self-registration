package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "PROFILE_ACCESSIBLE_CHANNEL")
public class ProfileAccessibleChannel implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @JoinColumn(name = "ACCESSIBLE_CHANNEL_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private AccessibleChannel accessibleChannel;
    @Column(name = "ACCESSIBLE_CHANNEL_ID", nullable = false, precision = 38, scale = 0, insertable = false, updatable = false)
    private Long accessibleChannelId;
    @Basic(optional = false)
    @Column(name = "ACTIVE", nullable = false)
    private Character active;
    @JoinColumn(name = "PROFILE_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Profile profile;

    public ProfileAccessibleChannel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AccessibleChannel getAccessibleChannel() {
        return accessibleChannel;
    }

    public void setAccessibleChannel(AccessibleChannel accessibleChannel) {
        this.accessibleChannel = accessibleChannel;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public Long getAccessibleChannelId() {
        return accessibleChannelId;
    }

    public void setAccessibleChannelId(Long accessibleChannelId) {
        this.accessibleChannelId = accessibleChannelId;
    }
}
