package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "PROFILE_ATTRIBUTE")
@NamedQueries({
    @NamedQuery(name = "ProfileAttribute.findAll", query = "SELECT b FROM ProfileAttribute b"),
    @NamedQuery(name = "ProfileAttribute.findByProfileId", query = "SELECT b FROM ProfileAttribute b where b.profile.id=:profileId")
})
public class ProfileAttribute implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "CHARGE_ACCOUNT", length = 20)
    private String chargeAccount;
    @Column(name = "TRIAL_INTERVAL")
    private Integer trialInterval;
    @Column(name = "RENEW_INTERVAL")
    private Integer renewInterval;
    @Column(name = "HAS_OPEN_TRANSFER", length = 1)
    private Character hasOpenTransfer;
    @JoinColumn(name = "PROFILE_ID", referencedColumnName = "ID", nullable = true)
    @OneToOne(fetch = FetchType.LAZY)
    private Profile profile;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "ID", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicationUser createdBy;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @JoinColumn(name = "LAST_MODIFIED_BY", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicationUser lastModifiedBy;
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;
    @JoinColumn(name = "OTP_DELIVERY_OPTION_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    private OtpDeliveryOption otpDeliveryOption;

    public ProfileAttribute() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRenewInterval() {
        return renewInterval;
    }

    public void setRenewInterval(Integer renewInterval) {
        this.renewInterval = renewInterval;
    }

    public Character getHasOpenTransfer() {
        return hasOpenTransfer;
    }

    public void setHasOpenTransfer(Character hasOpenTransfer) {
        this.hasOpenTransfer = hasOpenTransfer;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public ApplicationUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(ApplicationUser createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public ApplicationUser getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(ApplicationUser lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Integer getTrialInterval() {
        return trialInterval;
    }

    public void setTrialInterval(Integer trialInterval) {
        this.trialInterval = trialInterval;
    }

    public String getChargeAccount() {
        return chargeAccount;
    }

    public void setChargeAccount(String chargeAccount) {
        this.chargeAccount = chargeAccount;
    }

    public OtpDeliveryOption getOtpDeliveryOption() {
        return otpDeliveryOption;
    }

    public void setOtpDeliveryOption(OtpDeliveryOption otpDeliveryOption) {
        this.otpDeliveryOption = otpDeliveryOption;
    }

}
