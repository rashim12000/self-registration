package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "PROFILE_CHARGE")
@NamedQueries({
    @NamedQuery(name = "ProfileCharge.findAll", query = "SELECT pc FROM ProfileCharge pc"),
    @NamedQuery(name = "ProfileCharge.findByProfileId", query = "SELECT pc FROM ProfileCharge pc WHERE pc.profile.id = :profileId")
})
public class ProfileCharge implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;
    @Basic(optional = false)
    @Column(name = "CHARGE", precision = 18, scale = 2)
    private Double charge;
    @Basic(optional = false)
    @Column(name = "CODE", length = 10)
    private String code;
    @Basic(optional = false)
    @Column(name = "ACTIVE", nullable = false)
    private Character active;
    @JoinColumn(name = "PROFILE_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Profile profile;
    @JoinColumn(name = "CHARGE_TYPE_ID", referencedColumnName = "ID", nullable = true)
    @ManyToOne()
    private ChargeType chargeType;

    public ProfileCharge() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public Double getCharge() {
        return charge;
    }

    public void setCharge(Double charge) {
        this.charge = charge;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public ChargeType getChargeType() {
        return chargeType;
    }

    public void setChargeType(ChargeType chargeType) {
        this.chargeType = chargeType;
    }
    
    @Override
    public String toString() {
        return "com.f1soft.banksmart.auto.registration.entities.ProfileCharge[ id=" + id + " ]";
    }
}
