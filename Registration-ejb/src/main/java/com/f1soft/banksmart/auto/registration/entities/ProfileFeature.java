package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "PROFILE_FEATURE")
@NamedQueries({
    @NamedQuery(name = "ProfileFeature.findAll", query = "SELECT a FROM ProfileFeature a"),
    @NamedQuery(name = "ProfileFeature.findById", query = "SELECT a FROM ProfileFeature a WHERE a.id = :id"),
    @NamedQuery(name = "ProfileFeature.findByProfile", query = "SELECT a FROM ProfileFeature a WHERE a.profile = :profile")
})
public class ProfileFeature implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "FEATURE_ID")
    @ManyToOne()
    private Feature feature;
    @JoinColumn(name = "PROFILE_ID", referencedColumnName = "ID", nullable = true)
    @ManyToOne(fetch = FetchType.LAZY)
    private Profile profile;
    @Column(name = "ACTIVE", length = 1)
    private Character active;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "ID", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicationUser createdBy;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @JoinColumn(name = "LAST_MODIFIED_BY", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicationUser lastModifiedBy;
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;
    @OneToOne(mappedBy = "profileFeature")
    private ProfileTxnAlert profileTxnAlert;
    @OneToOne(mappedBy = "profileFeature")
    private ProfileTxnLimit profileTxnLimit;

    public ProfileFeature() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Feature getFeature() {
        return feature;
    }

    public void setFeature(Feature feature) {
        this.feature = feature;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public ProfileTxnAlert getProfileTxnAlert() {
        return profileTxnAlert;
    }

    public void setProfileTxnAlert(ProfileTxnAlert profileTxnAlert) {
        this.profileTxnAlert = profileTxnAlert;
    }

    public ProfileTxnLimit getProfileTxnLimit() {
        return profileTxnLimit;
    }

    public void setProfileTxnLimit(ProfileTxnLimit profileTxnLimit) {
        this.profileTxnLimit = profileTxnLimit;
    }

    public ApplicationUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(ApplicationUser createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public ApplicationUser getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(ApplicationUser lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }
}
