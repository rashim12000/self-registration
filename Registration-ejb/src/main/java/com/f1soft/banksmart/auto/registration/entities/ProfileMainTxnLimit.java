package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "PROFILE_MAIN_TXN_LIMIT")
public class ProfileMainTxnLimit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @JoinColumn(name = "PROFILE_ID", referencedColumnName = "ID", nullable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private Profile profile;
    @Basic(optional = false)
    @Column(name = "MAXIMUM_AMOUNT", nullable = false)
    private Double maximumAmount;
    @Basic(optional = false)
    @Column(name = "MAXIMUM_AMOUNT_PER_TRANSACTION", nullable = false)
    private Double maximumAmountPerTransaction;
    @Basic(optional = false)
    @Column(name = "TRANSACTION_COUNT", nullable = false)
    private Integer transactionCount;

    public ProfileMainTxnLimit() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getMaximumAmount() {
        return maximumAmount;
    }

    public void setMaximumAmount(Double maximumAmount) {
        this.maximumAmount = maximumAmount;
    }

    public Double getMaximumAmountPerTransaction() {
        return maximumAmountPerTransaction;
    }

    public void setMaximumAmountPerTransaction(Double maximumAmountPerTransaction) {
        this.maximumAmountPerTransaction = maximumAmountPerTransaction;
    }

    public Integer getTransactionCount() {
        return transactionCount;
    }

    public void setTransactionCount(Integer transactionCount) {
        this.transactionCount = transactionCount;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }
}
