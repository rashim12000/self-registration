package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "PROFILE_TXN_ALERT")
public class ProfileTxnAlert implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "CREDIT_ALERT")
    private Character creditAlert;
    @Basic(optional = false)
    @Column(name = "DEBIT_ALERT")
    private Character debitAlert;
    @Column(name = "MIN_CREDIT_AMT", precision = 53)
    private Double minCreditAmt;
    @Column(name = "MIN_DEBIT_AMT", precision = 53)
    private Double minDebitAmt;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "ID", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicationUser createdBy;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @JoinColumn(name = "LAST_MODIFIED_BY", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicationUser lastModifiedBy;
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;
    @Column(name = "ACTIVE", length = 1)
    private Character active;
    @Column(name = "SMS_LIMIT", length = 10)
    private Integer smsLimit;
    @JoinColumn(name = "PROFILE_FEATURE_ID")
    @OneToOne()
    private ProfileFeature profileFeature;

    public ProfileTxnAlert() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Character getCreditAlert() {
        return creditAlert;
    }

    public void setCreditAlert(Character creditAlert) {
        this.creditAlert = creditAlert;
    }

    public Character getDebitAlert() {
        return debitAlert;
    }

    public void setDebitAlert(Character debitAlert) {
        this.debitAlert = debitAlert;
    }

    public Double getMinCreditAmt() {
        return minCreditAmt;
    }

    public void setMinCreditAmt(Double minCreditAmt) {
        this.minCreditAmt = minCreditAmt;
    }

    public Double getMinDebitAmt() {
        return minDebitAmt;
    }

    public void setMinDebitAmt(Double minDebitAmt) {
        this.minDebitAmt = minDebitAmt;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public ProfileFeature getProfileFeature() {
        return profileFeature;
    }

    public void setProfileFeature(ProfileFeature profileFeature) {
        this.profileFeature = profileFeature;
    }

    public Integer getSmsLimit() {
        return smsLimit;
    }

    public void setSmsLimit(Integer smsLimit) {
        this.smsLimit = smsLimit;
    }

    public ApplicationUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(ApplicationUser createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public ApplicationUser getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(ApplicationUser lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }
}
