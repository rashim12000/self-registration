package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "PROFILE_TXN_LIMIT")
public class ProfileTxnLimit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @JoinColumn(name = "PROFILE_FEATURE_ID")
    @OneToOne()
    private ProfileFeature profileFeature;
    @Basic(optional = false)
    @Column(name = "MAXIMUM_AMOUNT", nullable = false)
    private Double maximumAmount;
    @Basic(optional = false)
    @Column(name = "MAXIMUM_AMOUNT_PER_TRANSACTION", nullable = false)
    private Double maximumAmountPerTransaction;
    @Basic(optional = false)
    @Column(name = "TRANSACTION_TYPE", nullable = false, length = 20)
    private String transactionType;
    @Basic(optional = false)
    @Column(name = "TRANSACTION_COUNT", nullable = false)
    private Integer transactionCount;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "ID", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicationUser createdBy;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @JoinColumn(name = "LAST_MODIFIED_BY", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicationUser lastModifiedBy;
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    public ProfileTxnLimit() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getMaximumAmount() {
        return maximumAmount;
    }

    public void setMaximumAmount(Double maximumAmount) {
        this.maximumAmount = maximumAmount;
    }

    public Double getMaximumAmountPerTransaction() {
        return maximumAmountPerTransaction;
    }

    public void setMaximumAmountPerTransaction(Double maximumAmountPerTransaction) {
        this.maximumAmountPerTransaction = maximumAmountPerTransaction;
    }

    public ProfileFeature getProfileFeature() {
        return profileFeature;
    }

    public void setProfileFeature(ProfileFeature profileFeature) {
        this.profileFeature = profileFeature;
    }

    public Integer getTransactionCount() {
        return transactionCount;
    }

    public void setTransactionCount(Integer transactionCount) {
        this.transactionCount = transactionCount;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public ApplicationUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(ApplicationUser createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public ApplicationUser getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(ApplicationUser lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }
}
