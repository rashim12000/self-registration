package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author root
 */
@Entity
@Table(name = "SMS_KEYWORD")
@NamedQueries({
    @NamedQuery(name = "SmsKeyword.findAll", query = "SELECT s FROM SmsKeyword s"),
    @NamedQuery(name = "SmsKeyword.findById", query = "SELECT s FROM SmsKeyword s WHERE s.id = :id"),
    @NamedQuery(name = "SmsKeyword.findByActive", query = "SELECT s FROM SmsKeyword s WHERE s.active = :active"),
    @NamedQuery(name = "SmsKeyword.findByDescription", query = "SELECT s FROM SmsKeyword s WHERE s.description = :description"),
    @NamedQuery(name = "SmsKeyword.findByKeyword", query = "SELECT s FROM SmsKeyword s WHERE s.keyword = :keyword"),
    @NamedQuery(name = "SmsKeyword.findByLastModifiedBy", query = "SELECT s FROM SmsKeyword s WHERE s.lastModifiedBy = :lastModifiedBy"),
    @NamedQuery(name = "SmsKeyword.findByLastModifiedDate", query = "SELECT s FROM SmsKeyword s WHERE s.lastModifiedDate = :lastModifiedDate"),
    @NamedQuery(name = "SmsKeyword.findByStaticId", query = "SELECT s FROM SmsKeyword s WHERE s.staticId = :staticId"),
    @NamedQuery(name = "SmsKeyword.findByStaticKeyword", query = "SELECT s FROM SmsKeyword s WHERE s.staticKeyword = :staticKeyword")})
public class SmsKeyword implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "ACTIVE")
    private char active;
    @Column(name = "DESCRIPTION")
    private String description;
    @Basic(optional = false)
    @Column(name = "KEYWORD")
    private String keyword;
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;
    @Column(name = "STATIC_ID")
    private Long staticId;
    @Column(name = "STATIC_KEYWORD")
    private String staticKeyword;
    @JoinColumn(name = "FEATURE_ID")
    @ManyToOne()
    private Feature feature;
    @JoinColumn(name = "KEY_ACCESS_MODE", referencedColumnName = "ID")
    @ManyToOne
    private KeyAccessMode keyAccessMode;

    public KeyAccessMode getKeyAccessMode() {
        return keyAccessMode;
    }

    public void setKeyAccessMode(KeyAccessMode keyAccessMode) {
        this.keyAccessMode = keyAccessMode;
    }

    public SmsKeyword() {
    }

    public SmsKeyword(Long id) {
        this.id = id;
    }

    public SmsKeyword(Long id, char active, String keyword) {
        this.id = id;
        this.active = active;
        this.keyword = keyword;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public char getActive() {
        return active;
    }

    public void setActive(char active) {
        this.active = active;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Long getStaticId() {
        return staticId;
    }

    public void setStaticId(Long staticId) {
        this.staticId = staticId;
    }

    public String getStaticKeyword() {
        return staticKeyword;
    }

    public void setStaticKeyword(String staticKeyword) {
        this.staticKeyword = staticKeyword;
    }

    public Feature getFeature() {
        return feature;
    }

    public void setFeature(Feature feature) {
        this.feature = feature;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SmsKeyword)) {
            return false;
        }
        SmsKeyword other = (SmsKeyword) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.f1soft.banksmart.auto.registration.entities.SmsKeyword[id=" + id + "]";
    }
}
