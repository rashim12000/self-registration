package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "SMS_RECEIVED")
@NamedQueries({
    @NamedQuery(name = "SmsReceived.findAll", query = "SELECT s FROM SmsReceived s"),
    @NamedQuery(name = "SmsReceived.findById", query = "SELECT s FROM SmsReceived s WHERE s.id = :id"),
    @NamedQuery(name = "SmsReceived.findByReceivedDate", query = "SELECT s FROM SmsReceived s WHERE s.receivedDate = :receivedDate"),
    @NamedQuery(name = "SmsReceived.findBySmsText", query = "SELECT s FROM SmsReceived s WHERE s.smsText = :smsText"),
    @NamedQuery(name = "SmsReceived.findByProcessStatus", query = "SELECT s FROM SmsReceived s WHERE s.processStatus = :processStatus"),
    @NamedQuery(name = "SmsReceived.findByMobileNumber", query = "SELECT s FROM SmsReceived s WHERE s.mobileNumber = :mobileNumber")})
@SqlResultSetMapping(name = "smsReceivedDetail", columns = {
    @ColumnResult(name = "MOBILE_NUMBER"),
    @ColumnResult(name = "SMS_REPORT_TEXT"),
    @ColumnResult(name = "REPORT_TEXT")
})
public class SmsReceived implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @Column(name = "RECEIVED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date receivedDate;
    @Column(name = "SMS_TEXT", length = 160)
    private String smsText;
    @Column(name = "SMS_REPORT_TEXT", length = 160)
    private String smsReportText;
    @Column(name = "PROCESS_STATUS")
    private Integer processStatus;
    @Column(name = "MOBILE_NUMBER", length = 15)
    private String mobileNumber;
    @Column(name = "REQUEST_MODE")
    private String requestMode;
    @JoinColumn(name = "CHANNEL_PROVIDER_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private ChannelProvider channelProviderId;
    @Column(name = "PROCESSED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date processedDate;
    @Column(name = "CUSTOMER_LOGIN_ID", precision = 38, scale = 0)
    private Long customerLoginId;
    @Column(name = "REQUEST_SMS_TEXT")
    private String requestSmsText;
    @Column(name = "REQUEST_INFO_ID")
    private Long requestInfoId;
    @Column(name = "PROVIDER_TRACE_ID")
    private String providerTraceId;

    public SmsReceived() {
    }

    public SmsReceived(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    public String getSmsText() {
        return smsText;
    }

    public void setSmsText(String smsText) {
        this.smsText = smsText;
    }

    public String getSmsReportText() {
        return smsReportText;
    }

    public void setSmsReportText(String smsReportText) {
        this.smsReportText = smsReportText;
    }

    public String getRequestMode() {
        return requestMode;
    }

    public void setRequestMode(String requestMode) {
        this.requestMode = requestMode;
    }

    public Integer getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(Integer processStatus) {
        this.processStatus = processStatus;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public ChannelProvider getChannelProviderId() {
        return channelProviderId;
    }

    public void setChannelProviderId(ChannelProvider channelProviderId) {
        this.channelProviderId = channelProviderId;
    }

    public Date getProcessedDate() {
        return processedDate;
    }

    public void setProcessedDate(Date processedDate) {
        this.processedDate = processedDate;
    }

    public Long getCustomerLoginId() {
        return customerLoginId;
    }

    public void setCustomerLoginId(Long customerLoginId) {
        this.customerLoginId = customerLoginId;
    }

    public String getRequestSmsText() {
        return requestSmsText;
    }

    public void setRequestSmsText(String requestSmsText) {
        this.requestSmsText = requestSmsText;
    }

    public Long getRequestInfoId() {
        return requestInfoId;
    }

    public void setRequestInfoId(Long requestInfoId) {
        this.requestInfoId = requestInfoId;
    }

    public String getProviderTraceId() {
        return providerTraceId;
    }

    public void setProviderTraceId(String providerTraceId) {
        this.providerTraceId = providerTraceId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SmsReceived)) {
            return false;
        }
        SmsReceived other = (SmsReceived) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.f1soft.banksmart.auto.registration.entities.SmsReceived[id=" + id + "]";
    }
}
