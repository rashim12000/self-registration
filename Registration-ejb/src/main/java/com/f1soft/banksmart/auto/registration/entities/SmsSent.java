package com.f1soft.banksmart.auto.registration.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "SMS_SENT")
@NamedQueries({
    @NamedQuery(name = "SmsSent.findAll", query = "SELECT s FROM SmsSent s"),
    @NamedQuery(name = "SmsSent.findById", query = "SELECT s FROM SmsSent s WHERE s.id = :id"),
    @NamedQuery(name = "SmsSent.findByDeliveredDate", query = "SELECT s FROM SmsSent s WHERE s.deliveredDate = :deliveredDate"),
    @NamedQuery(name = "SmsSent.findByMobileNumber", query = "SELECT s FROM SmsSent s WHERE s.mobileNumber = :mobileNumber"),
    @NamedQuery(name = "SmsSent.findBySmsMsgId", query = "SELECT s FROM SmsSent s WHERE s.smsMsgId = :smsMsgId"),
    @NamedQuery(name = "SmsSent.findBySendDate", query = "SELECT s FROM SmsSent s WHERE s.sendDate = :sendDate"),
    @NamedQuery(name = "SmsSent.findByStatus", query = "SELECT s FROM SmsSent s WHERE s.status = :status"),
    @NamedQuery(name = "SmsSent.findBySmsText", query = "SELECT s FROM SmsSent s WHERE s.smsText = :smsText"),
    @NamedQuery(name = "SmsSent.findByMessageFormatAndMobileNumber", query = "SELECT s FROM SmsSent s WHERE s.messageFormat.id = :messageFormat and s.mobileNumber=:mobileNumber and s.sendDate between :fromdate and :todate order by s.id desc"),
    @NamedQuery(name = "SmsSent.findByMinorPriority", query = "SELECT s FROM SmsSent s WHERE s.status =:status and s.smsReceived =null and s.channelProvider =:channelProvider and s.recordedDate  <= :recordedDate order by id "),
    @NamedQuery(name = "SmsSent.findByMajorPriority", query = "SELECT s FROM SmsSent s WHERE s.status =:status and s.smsReceived !=null and s.channelProvider =:channelProvider and s.recordedDate  <= :recordedDate order by id ")})
public class SmsSent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 19, scale = 0)
    private Long id;
    @Column(name = "DELIVERED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deliveredDate;
    @Column(name = "MOBILE_NUMBER", length = 15)
    private String mobileNumber;
    @Column(name = "SMS_MSG_ID", length = 10)
    private String smsMsgId;
    @Column(name = "SEND_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sendDate;
    @Column(name = "SEND_DATE", insertable = false, updatable = false)
    @Temporal(TemporalType.DATE)
    private Date recordedDate;
    @Column(name = "STATUS")
    private Integer status;
    @Column(name = "SMS_TEXT", length = 200)
    private String smsText;
    @Column(name = "REPORT_TEXT", length = 200)
    private String reportText;
    @Column(name = "RESENT_BY", length = 200)
    private String resentBy;
    @Column(name = "REMARKS", length = 200)
    private String remarks;
    @JoinColumn(name = "MESSAGE_FORMAT_ID", referencedColumnName = "ID")
    @ManyToOne(cascade = {CascadeType.MERGE})
    private MessageFormat messageFormat;
    @JoinColumn(name = "MESSAGE_TYPE_ID", referencedColumnName = "ID")
    @ManyToOne(cascade = {CascadeType.MERGE})
    private MessageType messageType;
    @JoinColumn(name = "SMS_RECEIVED_ID", referencedColumnName = "ID")
    @ManyToOne(cascade = {CascadeType.MERGE})
    private SmsReceived smsReceived;
    @JoinColumn(name = "CHANNEL_PROVIDER_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private ChannelProvider channelProvider;
    @Column(name = "CHANNEL_PROVIDER_ID", nullable = false, insertable = false, updatable = false)
    private Long channelProviderId;
    @Column(name = "PROCESSED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date processedDate;
    @Column(name = "PRIORITY")
    private Integer priority;
    @Column(name = "CHANNEL")
    private String channel;
    @Column(name = "INITIATED_BY", nullable = true, length = 80)
    private String initiatedBy;
    @Column(name = "FILE_NAME", nullable = true, length = 80)
    private String fileName;
    @Column(name = "ENCRYPTION_INDEX", nullable = true)
    private String encryptionIndex;
    @Column(name = "IS_ENCRYPTED", nullable = true)
    private Character isEncrypted;
    @Column(name = "BATCH_ID", nullable = true)
    private Long batchId;
    @Column(name = "ALERT_TRACKER_ID", nullable = true)
    private Long alertTrackerId;

    public SmsSent() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDeliveredDate() {
        return deliveredDate;
    }

    public void setDeliveredDate(Date deliveredDate) {
        this.deliveredDate = deliveredDate;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getSmsMsgId() {
        return smsMsgId;
    }

    public void setSmsMsgId(String smsMsgId) {
        this.smsMsgId = smsMsgId;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public Date getProcessedDate() {
        return processedDate;
    }

    public void setProcessedDate(Date processedDate) {
        this.processedDate = processedDate;
    }

    public Date getRecordedDate() {
        return recordedDate;
    }

    public void setRecordedDate(Date recordedDate) {
        this.recordedDate = recordedDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getSmsText() {
        return smsText;
    }

    public void setSmsText(String smsText) {
        this.smsText = smsText;
    }

    public MessageFormat getMessageFormat() {
        return messageFormat;
    }

    public void setMessageFormat(MessageFormat messageFormat) {
        this.messageFormat = messageFormat;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public SmsReceived getSmsReceived() {
        return smsReceived;
    }

    public void setSmsReceived(SmsReceived smsReceived) {
        this.smsReceived = smsReceived;
    }

    public ChannelProvider getChannelProvider() {
        return channelProvider;
    }

    public void setChannelProvider(ChannelProvider channelProvider) {
        this.channelProvider = channelProvider;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getReportText() {
        return reportText;
    }

    public void setReportText(String reportText) {
        this.reportText = reportText;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getResentBy() {
        return resentBy;
    }

    public void setResentBy(String resentBy) {
        this.resentBy = resentBy;
    }

    public String getInitiatedBy() {
        return initiatedBy;
    }

    public void setInitiatedBy(String initiatedBy) {
        this.initiatedBy = initiatedBy;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getEncryptionIndex() {
        return encryptionIndex;
    }

    public void setEncryptionIndex(String encryptionIndex) {
        this.encryptionIndex = encryptionIndex;
    }

    public Character getIsEncrypted() {
        return isEncrypted;
    }

    public void setIsEncrypted(Character isEncrypted) {
        this.isEncrypted = isEncrypted;
    }

    public Long getBatchId() {
        return batchId;
    }

    public void setBatchId(Long batchId) {
        this.batchId = batchId;
    }

    public Long getAlertTrackerId() {
        return alertTrackerId;
    }

    public void setAlertTrackerId(Long alertTrackerId) {
        this.alertTrackerId = alertTrackerId;
    }

    public Long getChannelProviderId() {
        return channelProviderId;
    }

    public void setChannelProviderId(Long channelProviderId) {
        this.channelProviderId = channelProviderId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SmsSent)) {
            return false;
        }
        SmsSent other = (SmsSent) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.f1soft.banksmart.auto.registration.entities.SmsSent[id=" + id + "]";
    }
}
