package com.f1soft.banksmart.charge.exception;

import javax.ws.rs.core.Response;

/**
 *
 * @author sadhana.dahal
 */
public abstract class AppException extends RuntimeException {

    public abstract Response.Status getStatus();

    public abstract String getExceptionMessage();

    public abstract String getLink();

    public abstract int getCode();

    public AppException(String message) {
        super(message);
    }

    public AppException(String message, Throwable e) {
        super(message, e);
    }
}
