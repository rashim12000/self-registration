package com.f1soft.banksmart.charge.exception;

import javax.ejb.ApplicationException;

/**
 *
 * @author sadhana.dahal
 */
@ApplicationException
public class HandleException extends RuntimeException {

    private static final long serialVersionUID = 796770993296843510L;

    public HandleException() {
    }

    public HandleException(String message) {
        super(message);
    }

    public HandleException(Exception ex) {
        super(ex);
    }

    public HandleException(String message, Exception ex) {
        super(message, ex);
    }

    public RuntimeException getCausedByException() {
        return (RuntimeException) getCause();
    }
}
