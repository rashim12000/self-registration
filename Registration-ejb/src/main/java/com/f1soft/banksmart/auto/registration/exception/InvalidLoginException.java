package com.f1soft.banksmart.charge.exception;

import javax.ws.rs.core.Response;

/**
 *
 * @author jikesh Apr 23, 2018 10:44:48 AM​
 */
public class InvalidLoginException extends AppException {

    public InvalidLoginException(String message) {
        super(message);
    }

    @Override
    public Response.Status getStatus() {
        return Response.Status.OK;
    }

    @Override
    public String getExceptionMessage() {
        return "UnAuthorized Access";
    }

    @Override
    public String getLink() {
        return "";
    }

    @Override
    public int getCode() {
        return 4001;
    }
}
