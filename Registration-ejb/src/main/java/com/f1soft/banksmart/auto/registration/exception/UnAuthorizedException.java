package com.f1soft.banksmart.charge.exception;

import javax.ws.rs.core.Response;

/**
 *
 * @author user
 */
public class UnAuthorizedException extends AppException {

    public UnAuthorizedException(String message) {
        super(message);
    }

    @Override
    public Response.Status getStatus() {
        return Response.Status.OK;
    }

    @Override
    public String getExceptionMessage() {
        return "UnAuthorized Access";
    }

    @Override
    public String getLink() {
        return "";
    }

    @Override
    public int getCode() {
        return 4001;
    }
}
