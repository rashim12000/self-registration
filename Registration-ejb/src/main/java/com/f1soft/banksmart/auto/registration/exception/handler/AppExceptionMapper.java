package com.f1soft.banksmart.auto.registration.exception.handler;

import com.f1soft.banksmart.auto.registration.dto.ErrorResponse;
import com.f1soft.banksmart.charge.exception.AppException;
import com.f1soft.banksmart.charge.exception.HandleException;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author user
 */
@Provider
@Produces(value = MediaType.APPLICATION_JSON)
public class AppExceptionMapper implements ExceptionMapper<Throwable> {

    private static final Logger logger = LoggerFactory.getLogger(AppExceptionMapper.class);

    @Override
    public Response toResponse(Throwable exception) {
        logger.error("Exception has been thrown by container");
        ErrorResponse errorResponse;
        if (exception instanceof AppException) {
            logger.debug("banksmart exception");
            AppException ex = (AppException) exception;
            errorResponse = new ErrorResponse(ex.getStatus().getStatusCode(), ex.getCode(), ex.getMessage(), ex.getLink(), ex.getExceptionMessage());
            return Response.status(ex.getStatus()).entity(errorResponse).build();
        } else if (exception instanceof HandleException) {
            errorResponse = new ErrorResponse(404, 404, exception.getMessage(), "", "Something went Wrong");
            return Response.status(Response.Status.BAD_REQUEST).entity(errorResponse).build();
        } else if (exception instanceof WebApplicationException) {
            logger.error("web application exception");
            WebApplicationException ex = (WebApplicationException) exception;
            int statuscode = ex.getResponse().getStatus();
            errorResponse = new ErrorResponse(statuscode, statuscode, ex.getMessage(), "", "Bad Request");
            return Response.status(statuscode).entity(errorResponse).build();
        }
        logger.error("Unchecked Exception", exception);
        errorResponse = new ErrorResponse(500, 5001, "Something went wrong", "", "Someting went wrong");
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(errorResponse).build();
    }
}
