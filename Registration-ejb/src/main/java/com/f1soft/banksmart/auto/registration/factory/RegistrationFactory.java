package com.f1soft.banksmart.auto.registration.factory;

import com.f1soft.banksmart.auto.registration.annotation.SelfRegistration;
import com.f1soft.banksmart.auto.registration.request.AbstractRegistration;
import javax.inject.Inject;

/**
 *
 * @author rashim.dhaubanjar
 */
public class RegistrationFactory {

    @Inject
    @SelfRegistration
    private AbstractRegistration abstractRegistration;

    public AbstractRegistration getRegistration() {
        return abstractRegistration;
    }

}
