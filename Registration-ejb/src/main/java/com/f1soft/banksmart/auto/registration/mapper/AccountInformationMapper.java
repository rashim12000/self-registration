package com.f1soft.banksmart.auto.registration.mapper;

import com.f1soft.banksmart.auto.registration.dto.AccountInfo;
import com.f1soft.banksmart.auto.registration.entities.AccountType;
import com.f1soft.banksmart.auto.registration.entities.ApplicationUser;
import com.f1soft.banksmart.auto.registration.entities.BankAccount;
import com.f1soft.banksmart.auto.registration.entities.Branch;
import com.f1soft.banksmart.auto.registration.entities.Profile;
import com.f1soft.banksmart.auto.registration.startup.ResourceLoader;
import com.f1soft.banksmart.auto.registration.utilities.Constant;
import com.f1soft.banksmart.auto.registration.utilities.DateHelper;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author sandhya.pokhrel
 */
public class AccountInformationMapper {

    private Logger logger = LoggerFactory.getLogger(AccountInformationMapper.class);
    private final ApplicationUser applicationUser;
    private final List<AccountInfo> accountInfos;

    private final Profile profile;

    public AccountInformationMapper(ApplicationUser applicationUser, Profile profile, List<AccountInfo> accountInfos) {
        this.applicationUser = applicationUser;
        this.profile = profile;
        this.accountInfos = accountInfos;

    }

    public List<BankAccount> setCustomeBankAccounts() {
        List<BankAccount> bankAccounts = new ArrayList<>();
        int counter = 1;
        for (AccountInfo accountInfo : accountInfos) {
            BankAccount bankAccount = new BankAccount();
            bankAccount.setAccountAlias(String.valueOf(counter));
            logger.info("Final Customer Account number in bank account  ******************" + accountInfo.getAccountNumber());

            bankAccount.setAccountNumber(accountInfo.getAccountNumber());
            bankAccount.setIsPrimary(isPrimary(counter) ? 'Y' : 'N');
            bankAccount.setIsTrialPeriod(isTrialPeriod() ? 'Y' : 'N');

            bankAccount.setAccountOwnerCBSId(accountInfo.getCbsId());
            bankAccount.setIsAccountOwner(accountInfo.isThirdParty() ? 'N' : 'Y');
            bankAccount.setAccountType(accountType(accountInfo.getAccountType()));
            bankAccount.setBranch(getBranch(accountInfo.getBranchCode()));

            bankAccount.setAlertEnabled('Y');
            bankAccount.setTxnEnabled('Y');
            bankAccount.setCardNumber(accountInfo.getCardNumber());
            bankAccount.setExpiryDate(expriyDate());
            bankAccount.setRenewDate(new Date());
            bankAccount.setActive('Y');
            bankAccount.setAddedBy(applicationUser);
            bankAccount.setAddedDate(new Date());
            bankAccount.setServiceCode(Constant.CHARGING_MODE_DISABLED);
            bankAccount.setServiceFlag(Constant.SERVICE_FLAG_ACTIVE);

            counter++;
            bankAccounts.add(bankAccount);
        }
        return bankAccounts;

    }

    private boolean isPrimary(int counter) {
        if (counter == 1) {
            return true;
        }
        return false;
    }

    private Date expriyDate() {
        Integer interval = profile.getProfileAttribute().getTrialInterval() > 0 ? profile.getProfileAttribute().getTrialInterval() : profile.getProfileAttribute().getRenewInterval();
        return DateHelper.addMonths(new DateTime(), interval).toDate();
    }

    private boolean isTrialPeriod() {
        if (profile.getProfileAttribute().getTrialInterval() > 0) {
            return true;
        }
        return false;
    }

    private AccountType accountType(String accType) {
        Map<String, AccountType> accountTypeMap = ResourceLoader.accountTypeMap;
        AccountType accountType = accountTypeMap.get(accType);
        return accountType;
    }

    private Branch getBranch(String branchCode) {
        Map<String, Branch> branchCodeMap = ResourceLoader.branchMap;
        Branch branch = branchCodeMap.get(branchCode);
        return branch;
    }

}
