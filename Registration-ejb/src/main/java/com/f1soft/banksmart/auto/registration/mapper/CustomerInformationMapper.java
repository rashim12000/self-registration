package com.f1soft.banksmart.auto.registration.mapper;

import com.f1soft.banksmart.auto.registration.dao.ApplicationDAO;
import com.f1soft.banksmart.auto.registration.dto.CustomerInformationDTO;
import com.f1soft.banksmart.auto.registration.dto.RegistrationRecord;
import com.f1soft.banksmart.auto.registration.entities.AccessibleChannel;
import com.f1soft.banksmart.auto.registration.entities.ApplicationPattern;
import com.f1soft.banksmart.auto.registration.entities.ApplicationUser;
import com.f1soft.banksmart.auto.registration.entities.CustomerLogin;
import com.f1soft.banksmart.auto.registration.entities.LoginAccessChannel;
import com.f1soft.banksmart.auto.registration.utilities.Validator;
import com.f1soft.cbs.common.dto.CustomerInformation;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author sandhya.pokhrel
 */
public class CustomerInformationMapper {

    @Inject
    private ApplicationDAO applicationDAO;

    private final Logger logger = LoggerFactory.getLogger(CustomerInformationMapper.class);
    private final RegistrationRecord registrationRecord;
    private final CustomerInformation customerInformation;
    private ApplicationUser applicationUser;

    public CustomerInformationMapper(RegistrationRecord registrationRecord, CustomerInformation customerInformation) {
        this.registrationRecord = registrationRecord;
        this.customerInformation = customerInformation;
    }

    public CustomerInformationDTO setCustomerInformation() {
        logger.info("**********Set customer information to customer dto *************************");
        CustomerInformationDTO customerInformationDTO = new CustomerInformationDTO();
        Object[] nameArray = formatCustomerInformation(customerInformation.getCustomerName());
        customerInformationDTO.setFirstName((String) nameArray[0]);
        customerInformationDTO.setMiddleName((String) nameArray[1]);
        customerInformationDTO.setLastName((String) nameArray[2]);
        customerInformationDTO.setGender("O");
        customerInformationDTO.setContactNumber(registrationRecord.getMobileNumber());
        customerInformationDTO.setProfile(registrationRecord.getProfile());
        applicationUser = registrationRecord.getApplicationUser();
        customerInformationDTO.setCreatedBy(applicationUser);
        customerInformationDTO.setAccountHolder('Y');
        customerInformationDTO.setActive('Y');
        customerInformationDTO.setApproved('N');
        customerInformationDTO.setCbsId(customerInformation.getCustomerId());
        customerInformationDTO.setMobileNumber(registrationRecord.getMobileNumber());
        customerInformationDTO.setAddress(customerInformation.getAddress());

        customerInformationDTO.setBranch(applicationDAO.getBranchesMap().get(customerInformation.getCustomerAccount().getBranchCode()));
        customerInformationDTO.setLatestActivityRemarks("Self Registered");
        customerInformationDTO.setCustomerLogins(getCustomerLogins());
        customerInformationDTO.setLoginAccessChannels(loginAccessChannels());

        return customerInformationDTO;
    }

    private List<CustomerLogin> getCustomerLogins() {
        List<ApplicationPattern> patternList = applicationDAO.getAllApplicationPatterns();
        ApplicationPattern applicationPattern = Validator.validateLoginPattern(registrationRecord.getMobileNumber(), patternList);

        List<CustomerLogin> customerLogins = new ArrayList<>();
        CustomerLogin customerLogin = new CustomerLogin();
        customerLogin.setUsername(registrationRecord.getMobileNumber());
        customerLogin.setIsPrimary('Y');
        customerLogin.setActive('Y');
        customerLogin.setCustomerDelete('N');

        customerLogin.setAddedBy(applicationUser);
        customerLogin.setAddedDate(new Date());
        customerLogin.setPasswordFlag("FL");
        customerLogin.setApplicationPattern(applicationPattern);
        customerLogin.setIsInitialPassword('N');
        customerLogin.setIsInitialTxnPassword('N');
        customerLogins.add(customerLogin);
        return customerLogins;
    }

    private List<LoginAccessChannel> loginAccessChannels() {
        List<LoginAccessChannel> loginAccessChannelList = new ArrayList<>();
        for (AccessibleChannel accessibleChannel : applicationDAO.accessibleChannels()) {
            LoginAccessChannel loginAccessChannel = new LoginAccessChannel();
            loginAccessChannel.setAccessibleChannel(accessibleChannel);
            loginAccessChannel.setActive('Y');
            loginAccessChannel.setAddedBy(applicationUser);
            loginAccessChannel.setAddedDate(new Date());
            loginAccessChannel.setRequestAttempt(0);
            loginAccessChannel.setLoginBlocked("N");
            loginAccessChannelList.add(loginAccessChannel);
        }
        return loginAccessChannelList;
    }

    private Object[] formatCustomerInformation(String customerName) {
        Object[] nameArray = new Object[3];
        String[] nameArr = customerName.split(" ");
        if (nameArr.length > 2) {
            nameArray[0] = nameArr[0];
            nameArray[1] = nameArr[1];
            nameArray[2] = "";
            for (int i = 2; i < nameArr.length; i++) {
                nameArray[2] = (new StringBuilder()).append(nameArray[2]).append(" ").append(nameArr[i]).toString();
            }
            return nameArray;
        }

        if (nameArr.length == 1) {
            nameArray[0] = nameArr[nameArr.length - 1];
            nameArray[1] = "N/A";
            nameArray[2] = "N/A";
            return nameArray;
        } else {
            nameArray[2] = nameArr[nameArr.length - 1];
            nameArray[0] = nameArr[nameArr.length - 2];
            nameArray[1] = "N/A";
            return nameArray;
        }
    }
}
