package com.f1soft.banksmart.auto.registration.mapper;

import com.f1soft.banksmart.auto.registration.dao.CustomerDAO;
import com.f1soft.banksmart.auto.registration.dto.CustomerInformationDTO;
import com.f1soft.banksmart.auto.registration.entities.Customer;
import java.util.Date;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author sandhya.pokhrel
 */
@Slf4j
public class CustomerRegistration {

    public static Customer register(CustomerInformationDTO customerInformationDTO) {
        Customer customer = new Customer();
        customer.setAcHolder(customerInformationDTO.getAccountHolder());
        customer.setActive(customerInformationDTO.getActive());
        customer.setAddress(customerInformationDTO.getAddress());
        customer.setApproved(customerInformationDTO.getApproved());
        customer.setBranchId(customerInformationDTO.getBranch().getId());
        customer.setCbsId(customerInformationDTO.getCbsId());
        customer.setContactNo(customerInformationDTO.getContactNumber());
        customer.setCreatedBy(customerInformationDTO.getCreatedBy());
        customer.setCreatedDate(new Date());
        customer.setEmailAddress(customerInformationDTO.getEmailAddress());
        customer.setFirstName(customerInformationDTO.getFirstName());
        customer.setGender(customerInformationDTO.getGender());
        customer.setLastName(customerInformationDTO.getLastName());
        customer.setMiddleName(customerInformationDTO.getMiddleName());
        customer.setMobileNumber(customerInformationDTO.getMobileNumber());
        customer.setProfile(customerInformationDTO.getProfile());
        
        return customer;
     

    }

}
