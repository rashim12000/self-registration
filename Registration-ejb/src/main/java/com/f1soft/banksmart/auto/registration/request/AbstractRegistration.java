package com.f1soft.banksmart.auto.registration.request;

import com.f1soft.banksmart.auto.registration.dto.RegistrationRecord;
import com.f1soft.banksmart.auto.registration.dto.ServerResponse;
import com.f1soft.banksmart.auto.registration.validator.CustomerRegistrationValidator;
import javax.enterprise.context.RequestScoped;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author rashim.dhaubanjar
 */
@Slf4j
@RequestScoped

public abstract class AbstractRegistration {

    RegistrationRecord registrationRecord;
    CustomerRegistrationValidator customerRegistrationValidator;

    public void initialize(RegistrationRecord registrationRecord) {
        this.registrationRecord = registrationRecord;
        this.customerRegistrationValidator = new CustomerRegistrationValidator(registrationRecord);

    }

    public ServerResponse register(RegistrationRecord registrationRecord) {

        initialize(registrationRecord);

        ServerResponse response = validate();
        if (response.isSuccess()) {
            log.info("validate customer info retrived " + response.isSuccess());
            response = getCustomerInformation();
            if (response.isSuccess()) {
                response = getCustomerAccounts();
                if (response.isSuccess()) {
                    log.info("retrieved customer info " + response.isSuccess());
                    response = save();
                }
            }
        }

        return response;
    }

    protected abstract ServerResponse validate();

    protected abstract ServerResponse getCustomerInformation();

    protected abstract ServerResponse getCustomerAccounts();

    protected abstract ServerResponse save();
}
