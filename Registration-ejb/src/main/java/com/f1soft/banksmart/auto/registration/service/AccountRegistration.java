package com.f1soft.banksmart.auto.registration.service;

import com.f1soft.banksmart.auto.registration.annotation.SoapRegistration;
import com.f1soft.banksmart.auto.registration.connector.SoapConnector;
import com.f1soft.banksmart.auto.registration.dto.CustomerInformationDTO;
import com.f1soft.banksmart.auto.registration.dto.RegistrationRecord;
import com.f1soft.banksmart.auto.registration.dto.ServerResponse;
import com.f1soft.banksmart.auto.registration.entities.BankAccount;
import com.f1soft.banksmart.auto.registration.mapper.AccountInformationMapper;
import com.f1soft.banksmart.auto.registration.mapper.CustomerInformationMapper;
import com.f1soft.banksmart.auto.registration.utilities.AccountComposer;
import com.f1soft.cbs.common.dto.CustomerInformation;
import java.util.List;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author sandhya.pokhrel
 */
@Slf4j
@SoapRegistration
public class AccountRegistration implements CbsRegistration {

    private CustomerInformationDTO customerInformationDTO;

    @Override
    public ServerResponse customerInformation(RegistrationRecord registrationRecord) {

        log.info("****************************Customer Registration by Account number called*****************************");
        ServerResponse response = new ServerResponse();
        CustomerInformation customerInformation = SoapConnector.getInstance().getCustomerInformationFromAccountNumber(registrationRecord.getAccountNumber());

        log.info("Registering customer mobile number :" + registrationRecord.getMobileNumber());
        if (customerInformation == null) {
            response.setSuccess(false);
            response.setMessage("Failed to obtain response from cbs.");
            return response;
        }

        CustomerInformationMapper customerInformationMapper = new CustomerInformationMapper(registrationRecord, customerInformation);
        customerInformationDTO = customerInformationMapper.setCustomerInformation();

        response.setSuccess(true);
        response.setObj(customerInformationDTO);
        return response;

    }

    @Override
    public ServerResponse getAccountList(RegistrationRecord registrationRecord) {
        log.info("******************************Customer account list by account number called****************************");

        AccountComposer accountComposer = new AccountComposer(registrationRecord, customerInformationDTO.getCbsId());
        ServerResponse response = accountComposer.process();
        if (!response.isSuccess()) {
            return response;
        }

        AccountInformationMapper accountInformationMapper = new AccountInformationMapper(customerInformationDTO.getCreatedBy(), customerInformationDTO.getProfile(), accountComposer.getAccountInfos());
        List<BankAccount> bankAccounts = accountInformationMapper.setCustomeBankAccounts();
        if (bankAccounts.isEmpty()) {
            response.setSuccess(false);
            response.setMessage("Invalid account");
            return response;
        }
        customerInformationDTO.setAccounts(bankAccounts);
        response.setSuccess(true);
        response.setObj(bankAccounts);
        return response;
    }

    @Override
    public CustomerInformationDTO getCustomerInformationDTO() {
        return customerInformationDTO;
    }
}
