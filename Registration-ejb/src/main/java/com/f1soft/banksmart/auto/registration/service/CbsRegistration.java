package com.f1soft.banksmart.auto.registration.service;

import com.f1soft.banksmart.auto.registration.dto.CustomerInformationDTO;
import com.f1soft.banksmart.auto.registration.dto.RegistrationRecord;
import com.f1soft.banksmart.auto.registration.dto.ServerResponse;

/**
 *
 * @author sandhya.pokhrel
 */
public interface CbsRegistration {

    ServerResponse customerInformation(RegistrationRecord registrationRecord);

    ServerResponse getAccountList(RegistrationRecord registrationRecord);

    CustomerInformationDTO getCustomerInformationDTO();
}
