package com.f1soft.banksmart.auto.registration.service;

import com.f1soft.banksmart.auto.registration.annotation.SelfRegistration;
import com.f1soft.banksmart.auto.registration.annotation.SoapRegistration;
import com.f1soft.banksmart.auto.registration.dao.ApplicationDAO;
import com.f1soft.banksmart.auto.registration.dao.CustomerDAO;
import com.f1soft.banksmart.auto.registration.dto.CustomerInformationDTO;
import com.f1soft.banksmart.auto.registration.dto.RegistrationRecord;
import com.f1soft.banksmart.auto.registration.dto.ServerResponse;
import com.f1soft.banksmart.auto.registration.entities.Customer;
import com.f1soft.banksmart.auto.registration.entities.CustomerLogin;
import com.f1soft.banksmart.auto.registration.mapper.CustomerRegistration;
import com.f1soft.banksmart.auto.registration.request.AbstractRegistration;
import com.f1soft.banksmart.auto.registration.validator.CustomerRegistrationValidator;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author sandhya.pokhrel
 */
@Slf4j
@SelfRegistration
public class CustomerRegistrationFacade extends AbstractRegistration {

    @Inject
    @SoapRegistration
    private CbsRegistration cbsRegistration;

    @Inject
    private CustomerDAO customerDAO;

    @Inject
    private ApplicationDAO applicationDAO;

    private RegistrationRecord registrationRecord;
    private CustomerInformationDTO customerInformation;
    private CustomerRegistrationValidator customerRegistrationValidator;

    @Override
    public ServerResponse validate() {
        ServerResponse response = customerRegistrationValidator.validateMobileNumber();

        if (response.isSuccess()) {
            log.info("Getting customer for mobile number from banksamrt database : " + registrationRecord.getMobileNumber());
            log.debug("Inject  application DAO " + applicationDAO);
            CustomerLogin customerLogin = applicationDAO.getCustomerLogin(registrationRecord.getMobileNumber());
            response.setSuccess(true);
            if (customerLogin != null) {
                response.setSuccess(false);
                response.setMessage("Customer already registered");
                return response;
            }
        }
        return response;
    }

    @Override
    public ServerResponse getCustomerInformation() {
        ServerResponse response = cbsRegistration.customerInformation(registrationRecord);
        return response;
    }

    @Override
    public ServerResponse getCustomerAccounts() {
        ServerResponse response = cbsRegistration.getAccountList(registrationRecord);
        return response;
    }

    @Override
    public ServerResponse save() {
        ServerResponse response = new ServerResponse();
        log.info("********************** Saving customer Information ********************************");
        customerInformation = cbsRegistration.getCustomerInformationDTO();
        Customer customer = CustomerRegistration.register(customerInformation);
        boolean isRegistered = customerDAO.registerCustomer(customer, customerInformation.getCustomerLogins(), customerInformation.getLoginAccessChannels(), customerInformation.getAccounts());
        log.info("Boolean is Registered :" + isRegistered);
        //update successs status
        if (isRegistered) {
            response.setSuccess(true);
            response.setMessage("Registration Successful.");
        } else {
            response.setSuccess(false);
            response.setMessage("Some problem occurred registering customer");
        }
        return response;
    }
}
