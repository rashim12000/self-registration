package com.f1soft.banksmart.auto.registration.startup;

import com.f1soft.banksmart.auto.registration.dao.ApplicationDAO;
import com.f1soft.banksmart.auto.registration.entities.AccessibleChannel;
import com.f1soft.banksmart.auto.registration.entities.AccountType;
import com.f1soft.banksmart.auto.registration.entities.ApplicationPattern;
import com.f1soft.banksmart.auto.registration.entities.ApplicationSetup;
import com.f1soft.banksmart.auto.registration.entities.Branch;
import com.f1soft.banksmart.auto.registration.entities.ChannelLoginPattern;
import com.f1soft.banksmart.auto.registration.entities.ChargeType;
import com.f1soft.banksmart.auto.registration.entities.MessageFormat;
import com.f1soft.banksmart.auto.registration.utilities.Constant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.persistence.Persistence;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author Narayan Joshi
 * <narayan.joshi@f1soft.com>
 */
@Slf4j
@Singleton
@Startup
public class ResourceLoader {

    @Inject
    private ApplicationDAO applicationDAO;

    public static List<ApplicationPattern> patternList;

    public static Map<String, Branch> branchMap;
    public static Map<String, AccountType> accountTypeMap;
    public static Map<String, ChargeType> chargeTypeMap;
    private static Map<String, ApplicationSetup> applicationSetupMap;
    public static Map<String, MessageFormat> messageFormatMap;

    @PostConstruct
    public void bootstrap() {
        log.info("Initializing resources...");
        PropertyConfigurator.configure("log4j.properties");
        branchMap = new HashMap<>();
        accountTypeMap = new HashMap<>();
        chargeTypeMap = new HashMap<>();
        messageFormatMap = new HashMap<>();

        loadBanner();
        loadHibernateProperties();
        loadApplicationData();
        loadPatternList();
        loadApplicationSetups();
    }

    private void loadApplicationSetups() {
        applicationSetupMap = new HashMap<>();
        List<ApplicationSetup> applicationSetups = applicationDAO.getAllApplicationSetups();
        applicationSetups.stream().forEach((applicationSetup) -> {
            applicationSetupMap.put(applicationSetup.getApplicationKey(), applicationSetup);
        });
    }

    private void loadApplicationData() {
        log.info("loadApplicationData********************************");
        branchMap = applicationDAO.getBranchesMap();
        accountTypeMap = applicationDAO.getAccountTypeMap();
        messageFormatMap = applicationDAO.messageFormatMap();
    }

    public List<ApplicationPattern> loadPatternList() {
        List<AccessibleChannel> accessibleChannels = getAccessibleChannels();
        patternList = new ArrayList<>();
        for (AccessibleChannel accessibleChannel : accessibleChannels) {
            for (ChannelLoginPattern channelLoginPattern : accessibleChannel.getChannelLoginPatternList()) {
                patternList.add(channelLoginPattern.getApplicationPattern());
            }
        }

        return patternList;
    }

    private List<AccessibleChannel> getAccessibleChannels() {
        return applicationDAO.accessibleChannels();
    }

    public static Map<String, ApplicationSetup> getApplicationSetupMap() {
        return applicationSetupMap;
    }

    public void setApplicationSetupMap(Map<String, ApplicationSetup> applicationSetupMap) {
        ResourceLoader.applicationSetupMap = applicationSetupMap;
    }

    private void loadBanner() {
        log.info("\n"
                + " _______          _________ _______      _______  _______  _______ _________ _______ _________ _______  _______ __________________ _______  _            _______           _        _       _________ _        _______ \n"
                + "(  ___  )|\\     /|\\__   __/(  ___  )    (  ____ )(  ____ \\(  ____ \\\\__   __/(  ____ \\\\__   __/(  ____ )(  ___  )\\__   __/\\__   __/(  ___  )( (    /|    (  ____ )|\\     /|( (    /|( (    /|\\__   __/( (    /|(  ____ \\\n"
                + "| (   ) || )   ( |   ) (   | (   ) |    | (    )|| (    \\/| (    \\/   ) (   | (    \\/   ) (   | (    )|| (   ) |   ) (      ) (   | (   ) ||  \\  ( |    | (    )|| )   ( ||  \\  ( ||  \\  ( |   ) (   |  \\  ( || (    \\/\n"
                + "| (___) || |   | |   | |   | |   | |    | (____)|| (__    | |         | |   | (_____    | |   | (____)|| (___) |   | |      | |   | |   | ||   \\ | |    | (____)|| |   | ||   \\ | ||   \\ | |   | |   |   \\ | || |      \n"
                + "|  ___  || |   | |   | |   | |   | |    |     __)|  __)   | | ____    | |   (_____  )   | |   |     __)|  ___  |   | |      | |   | |   | || (\\ \\) |    |     __)| |   | || (\\ \\) || (\\ \\) |   | |   | (\\ \\) || | ____ \n"
                + "| (   ) || |   | |   | |   | |   | |    | (\\ (   | (      | | \\_  )   | |         ) |   | |   | (\\ (   | (   ) |   | |      | |   | |   | || | \\   |    | (\\ (   | |   | || | \\   || | \\   |   | |   | | \\   || | \\_  )\n"
                + "| )   ( || (___) |   | |   | (___) |    | ) \\ \\__| (____/\\| (___) |___) (___/\\____) |   | |   | ) \\ \\__| )   ( |   | |   ___) (___| (___) || )  \\  |    | ) \\ \\__| (___) || )  \\  || )  \\  |___) (___| )  \\  || (___) |\n"
                + "|/     \\|(_______)   )_(   (_______)    |/   \\__/(_______/(_______)\\_______/\\_______)   )_(   |/   \\__/|/     \\|   )_(   \\_______/(_______)|/    )_)    |/   \\__/(_______)|/    )_)|/    )_)\\_______/|/    )_)(_______)\n"
                + "                                                                                                                                                                                                                       "
                + "\n");
    }

    private void loadHibernateProperties() {
        Persistence.createEntityManagerFactory("fonebankPU", getProperties());
    }

    public Map getProperties() {
        try {
            Map map = new HashMap();

            String vendor = "mssql";
            log.info("Database vendor  :" + vendor);
            if (vendor.trim().equalsIgnoreCase("oracle")) {
                map.put("hibernate.dialect", "org.hibernate.dialect.Oracle9Dialect");
            } else if (vendor.trim().equalsIgnoreCase("mysql")) {

                map.put("hibernate.dialect", "org.hibernate.dialect.MySQLInnoDBDialect");
            } else if (vendor.trim().equalsIgnoreCase("mssql")) {
                map.put("hibernate.dialect", "org.hibernate.dialect.SQLServerDialect");
            }
            log.info("Hibernate properties in gprs server map :" + map);

            return map;
        } catch (Exception e) {
            log.error("Exception", e);
        }
        return null;
    }

    public static List<ApplicationPattern> getPatternList() {
        return patternList;
    }

    public static Map<String, Branch> getBranchMap() {
        return branchMap;
    }

    public static Map<String, AccountType> getAccountTypeMap() {
        return accountTypeMap;
    }

    public static Map<String, ChargeType> getChargeTypeMap() {
        return chargeTypeMap;
    }

    public static Map<String, MessageFormat> getMessageFormatMap() {
        return messageFormatMap;
    }

}
