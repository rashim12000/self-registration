package com.f1soft.banksmart.auto.registration.utilities;

//import com.f1soft.banksmart.auto.registration.connector.CbsConnector;
import com.f1soft.banksmart.auto.registration.connector.SoapConnector;
import com.f1soft.banksmart.auto.registration.dto.AccountInfo;
import com.f1soft.banksmart.auto.registration.dto.RegistrationRecord;
import com.f1soft.banksmart.auto.registration.dto.ServerResponse;
import com.f1soft.cbs.common.dto.CustomerAccount;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author sandhya.pokhrel
 */
public class AccountComposer {

    private final Logger logger = LoggerFactory.getLogger(AccountComposer.class);
    private final RegistrationRecord registrationRecord;
    private final List<CustomerAccount> cbsAccountList = new ArrayList<>();
    private final String primaryCbsId;
    private List<AccountInfo> accountInfos;
    List<AccountInfo> ownerAccountInfos;

    public AccountComposer(RegistrationRecord registrationRecord, String primaryCbsId) {
        this.registrationRecord = registrationRecord;
        this.primaryCbsId = primaryCbsId;
    }

    public ServerResponse process() {
        logger.info("Composer for  account records by account number called**********************");
        ServerResponse response = new ServerResponse();
        accountInfos = new ArrayList<>();
        response = fetchAccountsFromCbs();
        if (!response.isSuccess()) {
            return response;
        }
        composeRecords();

        response.setSuccess(true);
        accountInfos.addAll(ownerAccountInfos);

        logger.info("Total account infos :" + accountInfos.size());
        return response;
    }

    private ServerResponse fetchAccountsFromCbs() {
        logger.info("Fetching customer accounts by account number                 :" + registrationRecord.getAccountNumber());
        ServerResponse response = new ServerResponse();
        List<CustomerAccount> customerAccounts = SoapConnector.getInstance().getInstance().getCustomerAccountsFromCBS(primaryCbsId);
        if (customerAccounts == null || customerAccounts.isEmpty()) {
            response.setSuccess(false);
            response.setMessage("Error obtaning customer accounts from cbs");
            return response;
        }
        response.setSuccess(true);
        cbsAccountList.addAll(customerAccounts);
        return response;
    }

    private void composeRecords() {
        logger.info("**********************Composing account lists**********************");
        ownerAccountInfos = new ArrayList<>();
        for (CustomerAccount account : cbsAccountList) {
            if (registrationRecord.getAccountNumber().equalsIgnoreCase(account.getAccountNumber())) {
                AccountInfo accountInfo = new AccountInfo();
                accountInfo.setAccountNumber(account.getAccountNumber());
                accountInfo.setCbsId(primaryCbsId);
                accountInfo.setAccountType(account.getAccountType());
                accountInfo.setCardNumber(account.getCardNumber());
                accountInfo.setBranchCode(account.getBranchCode());
                accountInfo.setThirdParty(false);
                ownerAccountInfos.add(accountInfo);
            }
        }

        logger.info("Owner account infos size :" + ownerAccountInfos.size());

    }

    public List<AccountInfo> getAccountInfos() {
        return accountInfos;
    }

}
