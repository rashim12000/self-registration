package com.f1soft.banksmart.auto.registration.utilities;

/**
 *
 * @author Narayan Joshi
 * <narayan.joshi@f1soft.com>
 */
public class Constant {

    //Record status codes
    public static final Integer INVALID_AUTO_REG = 3;
    public static final Integer SUCCESS_AUTO_REG = 2;

    //Charge
    public static final String REGISTRATION_CHARGE = "RC";
    public static final String MODULE_CHARGE_GROUP = "MC";

    public static final String ACCOUNT_WISE_CHARGING = "ACCOUNT";
    public static final String MODULE_WISE_CHARGING = "MODULE";

    public static final String REGISTRATION_CHARGE_REMARKS = "Registration Charge";

    //CHARGING MODES
    public static final String CHARGING_MODE_ENABLED = "CME";
    public static final String CHARGING_MODE_DISABLED = "CMD";
    public static final Character SERVICE_FLAG_ACTIVE = 'E';

    public static final Integer CHARGE_REQUEST_APPROVED = 1;

    public static final String CUSTOMER_APPROVED_REMARKS = "Customer Approved";

    //Database Messages Flag
    public static final Integer MESSAGE_FORMAT_DOES_NOT_EXISTS = 0;
    public static final Integer MESSAGE_FORMAT_EXISTS = 1;
    //SMS Footer
    public static final String NEXT_LINE = "\n";

    public static final String SMS_GPRS_PIN_REQUEST_MESSAGE = "SMS_GPRS_PIN_REQUEST";

    //Sms
    public static final String GPRS_CHANNEL = "GPRS";
    public static final String SMS_CHANNEL = "SMS";
    public static final Integer LOW_PRIORITY = 2;

    //cbs Mode
    public static final String TEMENOS_MODE = "TEMENOS";
    public static final String CBS_MODE = "CBS";
    //Databse vendor

    public static final String DATABSE_VENDOR = "DATABSE_VENDOR";

    public static final String DEFAULT_APPLICATION_USER = "DEFAULT_APPLICATION_USER";
    public static final String DEFAULT_PROFILE = "DEFAULT_PROFILE";

}
