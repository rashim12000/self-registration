//package com.f1soft.banksmart.auto.registration.utilities;
//
//import com.f1soft.banksmart.auto.registration.dto.AccountBranchDTO;
//import com.f1soft.banksmart.auto.registration.dto.RegistrationRecord;
//import com.f1soft.banksmart.auto.registration.entities.ApplicationPattern;
//import com.f1soft.banksmart.auto.registration.entities.ApplicationUser;
//import com.f1soft.banksmart.auto.registration.entities.BankAccount;
//import com.f1soft.banksmart.auto.registration.entities.CustomerLogin;
//import com.f1soft.banksmart.auto.registration.entities.Profile;
//import com.f1soft.banksmart.auto.registration.factory.DAOFactory;
//import com.f1soft.banksmart.auto.registration.jdbcdao.JdbcTemplate;
//import com.f1soft.banksmart.auto.registration.startup.ResourceLoader;
//import java.util.List;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
///**
// *
// * @author sandhya.pokhrel
// */
//public class CustomerRegistration {
//
//    private final Logger logger = LoggerFactory.getLogger(CustomerRegistration.class);
//    
//    public void register() {
//        JdbcTemplate jdbcTemplate = ConnectionUtil.jdbcTemplate();
//        List<RegistrationRecord> records = DAOFactory.getInstance().getCustomerDAO().customerRecordToRegisterList(tableName, ResourceLoader.autoRegistrationConfig.getNumberOfRecordsToProcess(), jdbcTemplate);
//        String tableName = ResourceLoader.autoRegistrationConfig.getRecordTable();
//        logger.info("Customer Registration records : {}", records);
//
//        for (RegistrationRecord registrationRecord : records) {
//            logger.info("Processing for record : {}", registrationRecord.getMobileNumber());
//            try {
//                List<AccountBranchDTO> accountLists = DAOFactory.getInstance().getCustomerDAO().accountListByMobileNumber(registrationRecord.getMobileNumber(), tableName, jdbcTemplate);
//                initialize(accountLists);
//
//                ApplicationUser creator = DAOFactory.getInstance().getApplicationDAO().getApplicationUser(registrationRecord.getApplicationUserId());
//                Profile profile = DAOFactory.getInstance().getApplicationDAO().getProfileById(registrationRecord.getApplicationUserId());
//                List<ApplicationPattern> patternList = DAOFactory.getInstance().getApplicationDAO().getAllApplicationPatterns();
//
//                ApplicationPattern applicationPattern = Validator.validateLoginPattern(registrationRecord.getMobileNumber(), patternList);
//                if (applicationPattern == null) {
//                    logger.info("Service Provider not found.");
//                    DAOFactory.getInstance().getCustomerDAO().setRecordUpdatedStatus(tableName, registrationRecord.getId(), Constant.INVALID_AUTO_REG, "Mobile Number Is Not Valid.", jdbcTemplate);
//                    continue;
//                }
//
//                CustomerLogin customerLogin = DAOFactory.getInstance().getApplicationDAO().getCustomerLogin(registrationRecord.getMobileNumber());
//                if (customerLogin != null && customerLogin.getId() != null) {
//                    logger.info("Customer Already Registered.");
//                    DAOFactory.getInstance().getCustomerDAO().setRecordUpdatedStatus(tableName, registrationRecord.getId(), Constant.INVALID_AUTO_REG, "Mobile Already Registered.", jdbcTemplate);
//                    continue;
//                }
//
//                BankAccountUtil bankAccountUtil = bankAccountUtil(creator, profile);
//                List<BankAccount> bankAccounts = bankAccountUtil.getBankAccounts();
//                if (bankAccounts.isEmpty()) {
//                    logger.info("Bank account is not valid");
//                    if (RegistrationUtil.isInvalidAccountType()) {
//                        DAOFactory.getInstance().getCustomerDAO().setRecordUpdatedStatus(tableName, registrationRecord.getId(), Constant.INVALID_AUTO_REG, "Account Type Not Valid.", jdbcTemplate);
//                    } else if (RegistrationUtil.isInvalidBranchCode()) {
//                        DAOFactory.getInstance().getCustomerDAO().setRecordUpdatedStatus(tableName, registrationRecord.getId(), Constant.INVALID_AUTO_REG, "Branch not registered.", jdbcTemplate);
//                    } else {
//                        DAOFactory.getInstance().getCustomerDAO().setRecordUpdatedStatus(tableName, registrationRecord.getId(), Constant.INVALID_AUTO_REG, "Bank Account Not Valid.", jdbcTemplate);
//                    }
//                    continue;
//                }
//
//                boolean registered = RegistrationUtil.registerCustomerInformationToSystem(creator, profile, registrationRecord, applicationPattern, bankAccount);
//                if (!registered) {
//                    logger.info("Customer Not Registered for record: {}", registrationRecord.getId());
//                    DAOFactory.getInstance().getCustomerDAO().setRecordUpdatedStatus(tableName, registrationRecord.getId(), Constant.INVALID_AUTO_REG, "Exception", jdbcTemplate);
//                } else {
//                    logger.info("Customer Registered for record: {}", registrationRecord.getId());
//                    DAOFactory.getInstance().getCustomerDAO().setRecordUpdatedStatus(tableName, registrationRecord.getId(), Constant.SUCCESS_AUTO_REG, "Registration Successful", jdbcTemplate);
//                }
//
//            } catch (Exception e) {
//                logger.info("Exception occured during processing record : {}", registrationRecord.getId());
//                DatabaseManager.getInstance().setRecordUpdatedStatus(tableName, registrationRecord.getId(), Constant.INVALID_AUTO_REG, "Exception");
//                logger.error("Exception: {}", e);
//            }
//
//        }
//
//    }
//
//    private BankAccountUtil bankAccountUtil(ApplicationUser applicationUser, Profile profile) {
//        BankAccountUtil bankAccountUtil = new BankAccountUtil(accountList, applicationUser, profile);
//        return bankAccountUtil;
//    }
//
//}
