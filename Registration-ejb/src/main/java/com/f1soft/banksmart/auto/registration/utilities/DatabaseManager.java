package com.f1soft.banksmart.auto.registration.utilities;

import com.f1soft.banksmart.auto.registration.dto.RegistrationRecord;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Narayan Joshi
 * <narayan.joshi@f1soft.com>
 */
public class DatabaseManager {

    private static final Logger logger = LoggerFactory.getLogger(DatabaseManager.class);

    private static DatabaseManager databaseManager;
    private String bankServerDriver;
    private String bankServerUrl;
    private String bankServerUsername;
    private String bankServerPassword;

    public DatabaseManager() {
    }

    public static DatabaseManager getInstance() {
        if (databaseManager == null) {
            synchronized (DatabaseManager.class) {
                if (databaseManager == null) {
                    databaseManager = new DatabaseManager();
                }
            }
        }
        return databaseManager;
    }

    public void initBankServerConfig(String driver, String url, String username, String password) {
        bankServerDriver = driver;
        bankServerUrl = url;
        bankServerUsername = username;
        bankServerPassword = password;
    }

    public Connection getConnection() {
        Connection conn = null;

        try {
            Class.forName(bankServerDriver);
            conn = DriverManager.getConnection(bankServerUrl, bankServerUsername, bankServerPassword);
            return conn;
        } catch (SQLException e) {
            e.printStackTrace();
            return conn;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return conn;
        }
    }

    public List<RegistrationRecord> customerRecordToRegisterList(String tblName, int numerRecordToProcess) {
        logger.info("customerRecordToRegisterList-------------------------------------");
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        List<RegistrationRecord> recordList = null;

        String viewQuery = "SELECT ID,ACCOUNT_NUMBER,MOBILE_NUMBER,CARD_NUMBER,REQUESTED_DATE,ACCOUNT_TYPE,APPROVED_DATE,BRANCH_CODE,"
                + "CBS_ID,FULL_NAME,PROCESS_STATUS,REJECTED_DATE,APPROVED_BY,REJECTED_BY FROM " + tblName + " WHERE PROCESS_STATUS = '0'";
        logger.info("CUSTOMER_REGISTRATION_RECORD query : {}", viewQuery);
        try {
            recordList = new ArrayList<>();

            conn = getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(viewQuery);
            while (rs.next()) {
                if (recordList.size() < numerRecordToProcess) {
//                    RegistrationRecord record = new RegistrationRecord();
//                    record.setAccountNumber(rs.getString("ACCOUNT_NUMBER").trim());
//                    record.setClientCode(rs.getString("CBS_ID").trim());
//                    record.setMobileNumber(rs.getString("MOBILE_NUMBER").trim());
//                    record.setCardNumber(rs.getString("CARD_NUMBER").trim());
//                    record.setAccountType(rs.getString("ACCOUNT_TYPE").trim());
//                    record.setBranchCode(rs.getString("BRANCH_CODE").trim());
//                    record.setCustomerName(rs.getString("FULL_NAME").trim());
//                    record.setApprovedDate(rs.getDate("APPROVED_DATE"));
//                    record.setRequestedDate(rs.getDate("REQUESTED_DATE"));
//                    record.setRejectedDate(rs.getDate("REJECTED_DATE"));
//                    record.setId(rs.getLong("ID"));
//                    record.setProcessStatus(rs.getLong("PROCESS_STATUS"));
//                    record.setApprovedBy(rs.getLong("APPROVED_BY"));
//                    record.setRejectedBy(rs.getLong("REJECTED_BY"));
//                    recordList.add(record);
                }
            }
            logger.info("customerRecordToRegisterList size: {}", recordList.size());
            return recordList;
        } catch (SQLException e) {
            e.printStackTrace();
            return recordList;
        } catch (Exception e) {
            e.printStackTrace();
            return recordList;
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean setRecordUpdatedStatus(String tblName, long id, int status, String remarks) {
        Connection conn = null;
        Statement stmt = null;
        try {
            conn = getConnection();
            stmt = conn.createStatement();
            String query = "UPDATE " + tblName + " SET PROCESS_STATUS=" + status + ",REMARKS='" + remarks + "' WHERE ID=" + id;
            stmt.executeUpdate(query);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
