package com.f1soft.banksmart.auto.registration.utilities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author user
 */
public class DateHelper {

    private static final Logger logger = LoggerFactory.getLogger(DateHelper.class);
    public static final String sqlDateFormat = "yyyy-MM-dd";
    public static final String cbsDateFormat = "dd-MMMM-yyyy";

    public static DateTime currentDateTime() {
        DateTime dateTime = DateTime.now();
        return dateTime;
    }

    public static java.sql.Date utilDateToSqlDate(java.util.Date uDate) {
        DateFormat sqlDateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        return java.sql.Date.valueOf(sqlDateFormatter.format(uDate));
    }

    public static String convertToString(DateTime dt, String format) {
        DateTimeFormatter fmt = DateTimeFormat.forPattern(format);
        return fmt.print(dt);
    }

    public static DateTime convertToDate(String date, String format) {
        DateTimeFormatter fmt = DateTimeFormat.forPattern(format);
        return fmt.parseDateTime(date);
    }

    public static String convertToString(Date date, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }

    public static Date changeFormat(Date date, String format) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            String formatedDate = simpleDateFormat.format(date);
            return simpleDateFormat.parse(formatedDate);
        } catch (ParseException e) {
            logger.error("Exception", e);
            return null;
        }
    }

    public static long getMinutesDifference(Date startDate, Date endDate) {
        long diff = endDate.getTime() - startDate.getTime();
        long min = diff / (60 * 1000);
        return min;
    }

    public static long getSecondsDifference(Date startDate, Date endDate) {
        long diff = endDate.getTime() - startDate.getTime();
        long min = diff / 1000;
        return min;
    }

    public static DateTime addDays(DateTime dt, int no) {
        return dt.plus(Period.days(no));
    }

    public static DateTime addMonths(DateTime dt, int no) {
        return dt.plus(Period.months(no));
    }

    public static DateTime addMinutes(DateTime dt, int no) {
        return dt.plus(Period.minutes(no));
    }

    public static DateTime addSeconds(DateTime dt, int no) {
        return dt.plus(Period.seconds(no));
    }

    public static int getDayOfWeek(DateTime dt) {
        return dt.getDayOfWeek();
    }

    public static int getHourOfDay(DateTime dt) {
        return dt.getHourOfDay();
    }

    public static int getMinuteOfHour(DateTime dt) {
        return dt.getMinuteOfHour();
    }

    public static String getMonthName(Date date, boolean shortName) {
        DateTime dt = new DateTime();
        if (shortName) {
            return dt.monthOfYear().getAsShortText();
        } else {
            return dt.monthOfYear().getAsText();
        }
    }

    public static java.util.Date getDateFromString(String from) {
        DateFormat df = new SimpleDateFormat();
        if (from.contains("-")) {
            df = new SimpleDateFormat("yyyy-MM-dd");
        } else if (from.contains("/")) {
            df = new SimpleDateFormat("yyyy/MM/dd");
        } else if (from.contains("\\")) {
            df = new SimpleDateFormat("yyyy\\MM\\dd");
        }
        java.util.Date today;
        try {
            today = df.parse(from);
            today.setDate(today.getDate());
            today.setHours(0);
            today.setMinutes(0);
            today.setSeconds(0);
        } catch (ParseException e) {
            return null;
        }
        return today;
    }

    public static java.util.Date getNextSchedulePaymentDate(Date today, String time) {
        String[] split = time.split(":");
        int hr = Integer.parseInt(split[0]);
        int min = Integer.parseInt(split[1]);

        today.setDate(today.getDate());
        today.setHours(hr);
        today.setMinutes(min);
        today.setSeconds(0);
        return today;
    }

    public static String getCBSFormattedDate(java.util.Date date) {
        String strDate = convertToString(date, sqlDateFormat);
        DateFormat df = new SimpleDateFormat();
        if (strDate.contains("-")) {
            df = new SimpleDateFormat("yyyy-MM-dd");
        } else if (strDate.contains("/")) {
            df = new SimpleDateFormat("yyyy/MM/dd");
        } else if (strDate.contains("\\")) {
            df = new SimpleDateFormat("yyyy\\MM\\dd");
        }
        java.util.Date today;
        try {
            today = df.parse(strDate);
            today.setDate(today.getDate());
            today.setHours(0);
            today.setMinutes(0);
            today.setSeconds(0);
        } catch (ParseException e) {
            logger.error("Exception : {}", e);
            return null;
        }
        return convertToString(today, sqlDateFormat);
    }

    public static String getCBSFormattedDate(String date) {
        DateFormat df = new SimpleDateFormat();
        if (date.contains("-")) {
            df = new SimpleDateFormat("yyyy-MM-dd");
        } else if (date.contains("/")) {
            df = new SimpleDateFormat("yyyy/MM/dd");
        } else if (date.contains("\\")) {
            df = new SimpleDateFormat("yyyy\\MM\\dd");
        }
        java.util.Date today;
        try {
            today = df.parse(date);
            today.setDate(today.getDate());
            today.setHours(0);
            today.setMinutes(0);
            today.setSeconds(0);
        } catch (ParseException e) {
            logger.error("Exception : {}", e);
            return null;
        }
        return convertToString(today, sqlDateFormat);
    }

    public static java.util.Date getUtilDateFromString(String date, String format) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            return simpleDateFormat.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    public static boolean isValidDate(String dateToValidate, String dateFromat) {
        if (dateToValidate == null) {
            return false;
        }
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
            sdf.setLenient(false);
            java.util.Date date = sdf.parse(dateToValidate);
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
