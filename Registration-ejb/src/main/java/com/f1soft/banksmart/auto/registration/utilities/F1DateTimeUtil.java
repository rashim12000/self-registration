package com.f1soft.banksmart.auto.registration.utilities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.sql.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author root
 */
public class F1DateTimeUtil {

    private static final Logger logger = LoggerFactory.getLogger(F1DateTimeUtil.class);

    private static volatile F1DateTimeUtil f1DateTimeUtil;
    private static final DateFormat utilDateFormatter = new SimpleDateFormat("dd-MM-yyyy");
    private static final DateFormat sqlDateFormatter = new SimpleDateFormat("yyyy-MM-dd");

    private F1DateTimeUtil() {
    }

    /**
     *
     * @return
     */
    public static F1DateTimeUtil getF1DateTimeUtil() {
        if (f1DateTimeUtil == null) {
            synchronized (F1DateTimeUtil.class) {
                if (f1DateTimeUtil == null) {
                    f1DateTimeUtil = new F1DateTimeUtil();
                }
            }
        }
        return f1DateTimeUtil;
    }

    /**
     *
     * @param uDate
     * @return
     * @throws ParseException
     */
    public static Date utilDateToSqlDate(java.util.Date uDate)
            throws ParseException {
        DateFormat sqlDateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Date sqlDate = null;
        try {
            sqlDate = Date.valueOf(sqlDateFormatter.format(uDate));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sqlDate;
    }

    /**
     *
     * @param sDate
     * @return
     * @throws ParseException
     */
    public static java.util.Date sqlDateToutilDate(Date sDate)
            throws ParseException {
        DateFormat utilDateFormatter = new SimpleDateFormat("dd-MM-yyyy");

        java.util.Date utilDate = null;
        try {
            utilDate = utilDateFormatter.parse(utilDateFormatter.format(sDate));
        } catch (ParseException parseException) {
            parseException.printStackTrace();
        }

        return utilDate;
    }

    public static String getTodayDate() {

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String todayDate = null;
        try {
            todayDate = dateFormat.format(calendar.getTime());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return todayDate;

    }

    /**
     *
     * @param oldDate
     * @param days
     * @return
     */
    public static java.util.Date addDay(java.util.Date oldDate, int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(oldDate);
        calendar.add(5, days);
        return calendar.getTime();
    }

    public static java.util.Date addMonth(java.util.Date oldDate, int months) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(oldDate);
        calendar.add(Calendar.MONTH, months);
        return calendar.getTime();
    }

    public static java.util.Date addYear(java.util.Date oldDate, int years) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(oldDate);
        calendar.add(Calendar.YEAR, years);
        return calendar.getTime();
    }

    /**
     *
     * @param date
     * @return
     */
    public static java.util.Date getLastDateOfMonth(java.util.Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int lastDate = calendar.getActualMaximum(5);
        calendar.set(5, lastDate);

        return calendar.getTime();
    }

    public static java.util.Date getDateOnly(java.util.Date date) {
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        return date;
    }

    /**
     *
     * @param date
     * @return
     */
    public static java.util.Date getFirstDateOfMonth(java.util.Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int firstDate = calendar.getActualMinimum(5);
        calendar.set(5, firstDate);
        return calendar.getTime();
    }

    public static String getUtilDateToString(java.util.Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    public static String getUtilDateToStringWithFormat(java.util.Date date, String dateFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        return sdf.format(date);
    }

    public static java.util.Date getFormattedDate(Date date) {
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            String strDate = dateformat.format(date);
            java.util.Date result = dateformat.parse(strDate);
            return result;
        } catch (Exception ex) {
            return null;
        }
    }

    public static java.util.Date changeFormat(java.util.Date date, String format) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            String formatedDate = simpleDateFormat.format(date);
            return simpleDateFormat.parse(formatedDate);
        } catch (ParseException e) {
            return null;
        }
    }

    public static java.util.Date getUtilDateFromString(String date, String format) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            return simpleDateFormat.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    public static java.util.Date getDateFromString(String from) {
        DateFormat df = new SimpleDateFormat();
        if (from.contains("-")) {
            df = new SimpleDateFormat("yyyy-MM-dd");
        } else if (from.contains("/")) {
            df = new SimpleDateFormat("yyyy/MM/dd");
        } else if (from.contains("\\")) {
            df = new SimpleDateFormat("yyyy\\MM\\dd");
        }
        java.util.Date today;
        try {
            today = df.parse(from);
            today.setDate(today.getDate());
            today.setHours(0);
            today.setMinutes(0);
            today.setSeconds(0);
        } catch (ParseException e) {
            return null;
        }
        return today;
    }

    public static String parseDateString(String dateStr) {
        try {
            String[] dateArr = dateStr.split("/");
            String formattedDate = dateArr[2] + "-" + dateArr[1] + "-" + dateArr[0];
            return formattedDate;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * return elapsed time
     */
    public static String elapsedTime(java.util.Date startDate, java.util.Date endDate) {
        try {
            logger.info("Date1: {}", startDate);
            logger.info("Date2: {}", endDate);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            String start1 = sdf.format(startDate);

            String start2 = sdf.format(endDate);

            java.util.Date startD = sdf.parse(start1);
            java.util.Date endD = sdf.parse(start2);

            long l1 = startD.getTime();

            long l2 = endD.getTime();

            long diff = l2 - l1;

            //convert second to millisecond
            long secondInMillis = 1000;

            //convert minute to millisecond
            long minuteInMillis = secondInMillis * 60;

            //convert hour to millsecond
            long hourInMillis = minuteInMillis * 60;

            //convert day to millisecond
            long dayInMillis = hourInMillis * 24;

            //convert year to millisecond
            long yearInMillis = dayInMillis * 365;

            long elapsedYears = diff / yearInMillis;

            diff = diff % yearInMillis;

            long elapsedDays = diff / dayInMillis;

            diff = diff % dayInMillis;

            long elapsedHours = diff / hourInMillis;

            diff = diff % hourInMillis;

            long elapsedMinutes = diff / minuteInMillis;

            diff = diff % minuteInMillis;

            long elapsedSeconds = diff / secondInMillis;

            String elapsedPeriod = "";

            if (elapsedDays > 0) {
                elapsedPeriod = elapsedDays + " Day ";
            }

            if (elapsedHours > 0) {
                if (elapsedHours == 1) {
                    elapsedPeriod += elapsedHours + " Hour ";
                } else {
                    elapsedPeriod += elapsedHours + " Hours ";
                }
            }

            if (elapsedMinutes > 0) {
                if (elapsedMinutes == 1) {
                    elapsedPeriod += elapsedMinutes + " Minute";
                } else {
                    elapsedPeriod += elapsedMinutes + " Minutes";
                }
            }

            if (elapsedPeriod.equals("")) {
                elapsedPeriod = "Less than a minute";
            }

            logger.info("elapsedPeriod: {}", elapsedPeriod);

            return elapsedPeriod;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     *
     * @param args
     */
    public static void main(String args[]) {
        String sample = "12/12/13";
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            java.util.Date date = sdf.parse(sample);
            logger.info("date: {}", date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        boolean isValidDate = isValidDate(sample, "dd/MM/yyyy");

    }

    public static boolean isValidDate(String dateToValidate, String dateFromat) {
        if (dateToValidate == null) {
            return false;
        }
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
            sdf.setLenient(false);
            java.util.Date date = sdf.parse(dateToValidate);
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
