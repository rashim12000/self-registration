package com.f1soft.banksmart.auto.registration.utilities;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author yogesh
 */
@Getter
@Setter
public class Message implements Serializable {

    private String message;
    private String reportMessage;
    private Long messageFormatId;
    private Long messageTypeId;
    private Integer messageFlag;
    private String username;
    private Long channelProviderId;
    private boolean isCard;
    private String cardNumber = "";
    private String encryptionIndex;
    private String isEncrypted;

    public Message() {
    }
}
