package com.f1soft.banksmart.auto.registration.utilities;

import com.f1soft.banksmart.auto.registration.entities.Customer;
import com.f1soft.banksmart.auto.registration.entities.CustomerLogin;
import com.f1soft.banksmart.auto.registration.entities.MessageFormat;
import com.f1soft.banksmart.auto.registration.startup.ResourceLoader;
import java.util.Map;

/**
 *
 * @author sandhya.pokhrel
 */
public class MessageComposer {

    public Message getMessage(Map<String, String> passwordMap, CustomerLogin customerLogin) {
        Map<String, MessageFormat> messageMap = ResourceLoader.messageFormatMap;
        Message msg = new Message();
        String reportMessage = "";
        String indexBuffer = "";
        String isEncrypted = "";

        if (passwordMap.size() > 0) {
            NewStandardEncryption decryption = new NewStandardEncryption();
            decryption.encrypt(String.valueOf(passwordMap.get("txnPin")));
            String encryptedPin = decryption.getCipherText();

            NewStandardEncryption gprsLogindecryption = new NewStandardEncryption();
            gprsLogindecryption.encrypt(String.valueOf(passwordMap.get("gprsLoginPassword")));
            String gprsLoginencryptedPin = gprsLogindecryption.getCipherText();

            Customer customer = customerLogin.getCustomer();
            String customerName = customer.getFirstName();

            if (passwordMap.size() == 2) {
                msg = MessageFormater.getMessage(messageMap, Constant.SMS_GPRS_PIN_REQUEST_MESSAGE, MessageFormater.getParameters(Parameter.CUSTOMER_PARAMTER, customerName, Parameter.PIN_PARAMETER, encryptedPin, Parameter.GPRS_LOGIN_PIN_PARAMETER, gprsLoginencryptedPin,
                        Parameter.CHANNEL_NAME, Constant.SMS_CHANNEL, Parameter.GPRS_CHANNEL_PARAMETER, Constant.GPRS_CHANNEL));
                reportMessage = msg.getMessage().replace(encryptedPin, "#######").replace(gprsLoginencryptedPin, "#######");

                int encrypteIndex = msg.getMessage().indexOf(encryptedPin);
                int endIndex = encrypteIndex + encryptedPin.length();
                indexBuffer = String.valueOf(encrypteIndex) + "-" + String.valueOf(endIndex);

                int gprsLoginencryptedStartIndex = msg.getMessage().indexOf(gprsLoginencryptedPin);
                int gprsLoginencryptedEndIndex = gprsLoginencryptedStartIndex + gprsLoginencryptedPin.length();

                indexBuffer += "," + String.valueOf(gprsLoginencryptedStartIndex) + "-" + String.valueOf(gprsLoginencryptedEndIndex);
                isEncrypted = "Y";
            }
        }
        msg.setReportMessage(reportMessage);
        msg.setEncryptionIndex(indexBuffer);
        msg.setIsEncrypted(isEncrypted);
        return msg;
    }
}
