package com.f1soft.banksmart.auto.registration.utilities;

import com.f1soft.banksmart.auto.registration.entities.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author yogesh
 */
public class MessageFormater {

    public MessageFormater() {
    }

    public static Message messageChecker(Map<String, MessageFormat> messages, String key) {
        Message message = new Message();
        if (messages.containsKey(key)) {
            MessageFormat messageFormat = messages.get(key);
            message.setMessageFlag(Constant.MESSAGE_FORMAT_EXISTS);
            message.setMessage(messageFormat.getMessage());
            message.setMessageFormatId(messageFormat.getId());
            message.setMessageTypeId(messageFormat.getMessageType().getId());
            return message;
        } else {
            message.setMessageFlag(Constant.MESSAGE_FORMAT_DOES_NOT_EXISTS);
            return message;
        }
    }

    public static Message concatList(Map<String, MessageFormat> messageMap, String key, String paramKey1, String paramKey2, String parameter1, String parameter2, List<String> parameter3) {
        Message formatedMessage = messageChecker(messageMap, key);
        if (formatedMessage.getMessageFlag().equals(Constant.MESSAGE_FORMAT_EXISTS)) {
            String format = formatedMessage.getMessage().replace(paramKey1, parameter1);
            String refinedMessage = format.replace(paramKey2, parameter2);
            for (String txnInfo : parameter3) {
                refinedMessage = refinedMessage + txnInfo + Constant.NEXT_LINE;
            }
            String message = refinedMessage;
            formatedMessage.setMessage(message);
            formatedMessage.setReportMessage(message);
            return formatedMessage;
        } else {
            return formatedMessage;
        }
    }

    public static Message concatStatement(Map<String, MessageFormat> messageMap, String key, String paramKey1, String paramKey2, String parameter1, String parameter2, List<Map<String, String>> parameter3) {
        Message formatedMessage = messageChecker(messageMap, key);
        if (formatedMessage.getMessageFlag().equals(Constant.MESSAGE_FORMAT_EXISTS)) {
            String format = formatedMessage.getMessage().replace(paramKey1, parameter1);
            String refinedMessage = format.replace(paramKey2, parameter2);
            for (Map txnInfo : parameter3) {
                refinedMessage = refinedMessage + txnInfo + Constant.NEXT_LINE;
            }
            String message = refinedMessage;
            formatedMessage.setMessage(message);
            formatedMessage.setReportMessage(message);
            return formatedMessage;
        } else {
            return formatedMessage;
        }
    }

    public static Map<String, String> getParameters(String... args) {
        Map<String, String> parameters = new HashMap<String, String>();
        int loop = args.length / 2;
        int j = 0;
        for (int i = 0; i < loop; i++) {
            parameters.put(args[j], args[++j]);
            j++;
        }
        return parameters;
    }

    public static Message getMessage(Map<String, MessageFormat> messageMap, String key, Map<String, String> parameters) {

        Message formatedMessage = messageChecker(messageMap, key);
        if (formatedMessage.getMessageFlag().equals(Constant.MESSAGE_FORMAT_EXISTS)) {
            StringBuffer messageFormat = new StringBuffer(formatedMessage.getMessage());
            for (String paramKey : parameters.keySet()) {
                String paramValue = parameters.get(paramKey);
                int from = messageFormat.indexOf(paramKey);
                int paramLength = paramKey.length();
                if (from > 0) {
                    int to = paramLength + from;
                    messageFormat.replace(from, to, paramValue);
                }
            }
            String message = messageFormat.toString();
            formatedMessage.setMessage(message);
            formatedMessage.setReportMessage(message);
            return formatedMessage;
        } else {
            return formatedMessage;
        }

    }

    public static Message attach(Message message, String attachment, Map<String, String> parameters) {
        String messageToFormat = message.getMessage().replace("", " ");
        StringBuffer messageFormat = new StringBuffer(messageToFormat.trim() + "," + Constant.NEXT_LINE + "Correct Format is : " + Constant.NEXT_LINE);
        StringBuffer attachmentBuffer = new StringBuffer(attachment);
        for (String paramKey : parameters.keySet()) {
            String paramValue = parameters.get(paramKey);
            int from = attachmentBuffer.indexOf(paramKey);
            int paramLength = paramKey.length();
            if (from > 0) {
                int to = paramLength + from;
                attachmentBuffer.replace(from, to, paramValue);
            }
        }
        String refinedMessage = messageFormat.toString() + attachmentBuffer.toString() + Constant.NEXT_LINE + "Footer";
        message.setMessage(refinedMessage);
        message.setReportMessage(refinedMessage);
        return message;
    }

    public static Message transactionLimitInfo(Map<String, MessageFormat> messageMap, String key, String paramKey, String paramter, String paramKey1, String parameter1, String paramKey2, String parameter2, String paramKey3, String parameter3) {
        Message formatedMessage = messageChecker(messageMap, key);
        if (formatedMessage.getMessageFlag().equals(Constant.MESSAGE_FORMAT_EXISTS)) {
            String format = formatedMessage.getMessage().replace(paramKey, paramter).replace(paramKey1, parameter1 + "\n").replace(paramKey2, parameter2 + "\n").replace(paramKey3, parameter3 + "\n");
            formatedMessage.setMessage(format);
            formatedMessage.setReportMessage(format);
            return formatedMessage;
        } else {
            return formatedMessage;
        }
    }
}
