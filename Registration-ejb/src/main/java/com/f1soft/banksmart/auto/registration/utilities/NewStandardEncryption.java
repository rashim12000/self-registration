package com.f1soft.banksmart.auto.registration.utilities;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author root
 */
public class NewStandardEncryption {

    private String plainText;
    private String cipherText;
    private boolean status;
    private static String sharedKeyForEncryption = "123000000000000000000xyz";
    private static byte[] sharedkey = {
        0x01, 0x02, 0x03, 0x05, 0x07, 0x0B, 0x0D, 0x11,
        0x12, 0x11, 0x0D, 0x0B, 0x07, 0x02, 0x04, 0x08,
        0x01, 0x02, 0x03, 0x05, 0x07, 0x0B, 0x0D, 0x11
    };
    private static byte[] sharedvector = {
        10, 10, 10, 20, 20, 20, 55, 8
    };

    public NewStandardEncryption encrypt(String plaintext) {

        try {
            Cipher c = Cipher.getInstance("DESede/ECB/PKCS5Padding");
            c.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(sharedKeyForEncryption.getBytes(), "DESede"));
            byte[] encrypted = c.doFinal(plaintext.getBytes("UTF-8"));

            this.setCipherText(Base64.encode(encrypted));
            this.setStatus(true);
            return this;
        } catch (Exception ex) {
            this.setStatus(false);
            return this;
        }
    }

    public NewStandardEncryption decrypt(String ciphertext) {
        NewStandardEncryption decryption = new NewStandardEncryption();
        try {
            Cipher c = Cipher.getInstance("DESede/ECB/PKCS5Padding");
            c.init(Cipher.DECRYPT_MODE, new SecretKeySpec(sharedKeyForEncryption.getBytes(), "DESede"));
            byte[] decrypted = c.doFinal(Base64.decode(ciphertext));
            this.setPlainText(new String(decrypted, "UTF-8"));
            this.setStatus(true);
            return this;
        } catch (Exception ex) {
            this.setStatus(false);
            return decryption;
        }
    }

    public String getCipherText() {
        return cipherText;
    }

    public void setCipherText(String cipherText) {
        this.cipherText = cipherText;
    }

    public String getPlainText() {
        return plainText;
    }

    public void setPlainText(String plainText) {
        this.plainText = plainText;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
