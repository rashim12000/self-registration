package com.f1soft.banksmart.auto.registration.utilities;

/**
 *
 * @author Kailash
 */
public class Parameter {

    public static final String CUSTOMER_PARAMTER = "{param1}";
    public static final String ACCOUNT1_PARAMETER = "{param2}";
    public static final String ACCOUNT2_PARAMETER = "{param3}";
    public static final String ACCOUNT3_PARAMETER = "{param4}";
    public static final String ACCOUNT4_PARAMETER = "{param5}";
    public static final String BALANCE_PARAMETER = "{param6}";
    public static final String AMOUNT_PARAMETER = "{param7}";
    public static final String INVALID_AMOUNT_PARAMETER = "{param8}";
    public static final String MERCHANT_PARAMETER = "{param9}";
    public static final String SERVICE_PARAMETER = "{param10}";
    public static final String ACCOUNT_ALIAS_PARAMETER = "{param11}";
    public static final String KEYWORD_PARAMETER = "{param12}";
    public static final String CARD_TYPE_PARAMETER = "{param13}";
    public static final String PIN_PARAMETER = "{param14}";
    public static final String RECHARGE_CARD_PARAMETER = "{param15}";
    public static final String USER_DEFINED1_PARAMETER = "{param16}";
    public static final String USER_DEFINED2_PARAMETER = "{param17}";
    public static final String USER_DEFINED3_PARAMETER = "{param18}";
    public static final String SERVICE_ATTRIBUTE_PARAMETER = "{param19}";
    public static final String CURRENCY_TYPE_PARAMETER = "{param21}";
    public static final String CURRENCY_RATE_PARAMETER = "{param22}";
    public static final String MOBILE_PARAMETER = "{param23}";
    public static final String DATE_PARAMETER = "{param24}";
    public static final String BRANCH_PARAMETER = "{param25}";
    public static final String LEDGER_BALANCE_PARAMETER = "{param26}";
    public static final String MOBILE_NUMBER_PARAMETER = "{param27}";
    public static final String TRANSACTION_COUNT = "{param28}";
    public static final String TRANSACTION_AMOUNT = "{param29}";
    public static final String PER_TRANSACTION_AMOUNT = "{param30}";
    public static final String SOURCE_ACCOUNTS = "{param31}";
    public static final String COMMISSION_AMOUNT_PARAMETER = "{param45}";
    public static final String FONEPAY_TRANSACTION_ID = "{param46}";
    public static final String REMARKS = "{param47}";
    public static final String DISCOUNT_CHARGE_AMOUNT_PARAMETER = "{param48}";
    public static final String LOGIN_PASSWORD = "{param20}";
    public static final String TXN_PASSWORD = "{param49}";
    public static final String CHANNEL_NAME = "{param50}";
    public static final String GPRS_LOGIN_PIN_PARAMETER = "{param51}";
    public static final String GPRS_CHANNEL_PARAMETER = "{param53}";
}
