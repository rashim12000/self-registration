package com.f1soft.banksmart.auto.registration.utilities;

import com.f1soft.banksmart.auto.registration.dto.PasswordFormat;
import java.util.Random;

public class RandomGenerator {

    final static int[] sizeTable = {0, 9, 99, 999, 9999, 99999, 999999, 9999999,
        99999999, 999999999, Integer.MAX_VALUE};
    private static final String ALPHA = "abcdefghijklmnopqrst";
    private static final String NUM = "0123456789";
    private static Random random = new Random();

    public static int generate(int digit) {
        int highest = sizeTable[digit] + 1;
        int lowest = sizeTable[digit - 1] + 1;

        int generated = random.nextInt(highest);

        if (generated < lowest) {
            generated = generate(digit);
        }

        return generated;
    }

    public static String generateRandomNumber(int alphaLenth, int numericLength) {

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < alphaLenth; i++) {
            int ndx = (int) (Math.random() * ALPHA.length());
            sb.append(ALPHA.charAt(ndx));
        }
        for (int i = 0; i < numericLength; i++) {
            int ndx = (int) (Math.random() * NUM.length());
            sb.append(NUM.charAt(ndx));
        }
        return sb.toString();
    }

    public static String generateLoginRandomPassword(PasswordFormat passwordFormat) {

        StringBuilder sb = new StringBuilder();

        if (passwordFormat.getLoginAlphaLength() > 0) {
            for (int i = 0; i < passwordFormat.getLoginAlphaLength(); i++) {
                int ndx = (int) (Math.random() * ALPHA.length());
                sb.append(ALPHA.charAt(ndx));
            }
        }
        if (passwordFormat.getLoginNumericLength() > 0) {
            for (int i = 0; i < passwordFormat.getLoginNumericLength(); i++) {
                int ndx = (int) (Math.random() * NUM.length());
                sb.append(NUM.charAt(ndx));
            }
        }
        if (passwordFormat.getLoginSpecialCharLength() > 0) {
            for (int i = 0; i < passwordFormat.getLoginSpecialCharLength(); i++) {
                int ndx = (int) (Math.random() * passwordFormat.getSpecialChar().length());
                sb.append(passwordFormat.getSpecialChar().charAt(ndx));
            }
        }

        return sb.toString();
    }

    public static String generateTxnRandomPassword(PasswordFormat passwordFormat) {

        StringBuilder sb = new StringBuilder();

        if (passwordFormat.getTxnAlphaLength() > 0) {
            for (int i = 0; i < passwordFormat.getTxnAlphaLength(); i++) {
                int ndx = (int) (Math.random() * ALPHA.length());
                sb.append(ALPHA.charAt(ndx));
            }
        }
        if (passwordFormat.getTxnNumericLength() > 0) {
            for (int i = 0; i < passwordFormat.getTxnNumericLength(); i++) {
                int ndx = (int) (Math.random() * NUM.length());
                sb.append(NUM.charAt(ndx));
            }
        }
        if (passwordFormat.getTxnSpecialCharLength() > 0) {
            for (int i = 0; i < passwordFormat.getTxnSpecialCharLength(); i++) {
                int ndx = (int) (Math.random() * passwordFormat.getSpecialChar().length());
                sb.append(passwordFormat.getSpecialChar().charAt(ndx));
            }
        }
        return sb.toString();
    }
}
