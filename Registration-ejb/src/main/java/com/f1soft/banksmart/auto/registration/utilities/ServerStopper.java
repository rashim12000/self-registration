package com.f1soft.banksmart.auto.registration.utilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Narayan Joshi
 * <narayan.joshi@f1soft.com>
 */
public class ServerStopper {

    private static final Logger logger = LoggerFactory.getLogger(ServerStopper.class);

    public ServerStopper() {
    }

    public static void pauseThread(long milliSecond) {
        try {
            Thread.sleep(milliSecond);
        } catch (Exception e) {
        }
    }
}
