package com.f1soft.banksmart.auto.registration.utilities;

import com.f1soft.banksmart.auto.registration.entities.ApplicationPattern;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Narayan Joshi
 * <narayan.joshi@f1soft.com>
 */
public class Validator {

    public static ApplicationPattern validateLoginPattern(String username, List<ApplicationPattern> applicationPatternList) {
        for (ApplicationPattern applicationPattern : applicationPatternList) {

            if (applicationPattern.getPattern() != null && applicationPattern.getPattern().trim().length() > 0) {

                String patternFormat = applicationPattern.getPattern();

                Pattern pattern = Pattern.compile(patternFormat);
                Matcher matcher;

                matcher = pattern.matcher(username.trim());

                if (matcher.matches()) {
                    return applicationPattern;
                }
            }
        }
        return null;
    }
}
