package com.f1soft.banksmart.auto.registration.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

/**
 *
 * @author santosh-pc
 */
public class YamlReader {

    static final Logger logger = LoggerFactory.getLogger(YamlReader.class);

    public <T> List<T> getAll(String fileName, Class<T> clazz) {
        Yaml yaml = new Yaml(new Constructor(clazz));
        List<T> list = new ArrayList<>();
        try {
            InputStream in = new FileInputStream(new File(fileName));

            Iterable<Object> loadAll = yaml.loadAll(in);

            for (Object load : loadAll) {
                T object = clazz.cast(load);
                list.add(object);
            }
        } catch (Exception e) {
            logger.error("Exception : {}", e);
        }
        return list;
    }

    public <T> T load(String fileName, Class<T> clazz) {
        Yaml yaml = new Yaml(new Constructor(clazz));
        try {
            return yaml.loadAs(new FileInputStream(fileName), clazz);
        } catch (Exception e) {
            logger.error("Exception : {}", e);
        }
        return null;
    }

}
