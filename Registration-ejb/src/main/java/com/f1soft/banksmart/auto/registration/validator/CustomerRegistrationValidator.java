package com.f1soft.banksmart.auto.registration.validator;

import com.f1soft.banksmart.auto.registration.dto.RegistrationRecord;
import com.f1soft.banksmart.auto.registration.dto.ServerResponse;
import com.f1soft.banksmart.auto.registration.entities.ApplicationPattern;
import com.f1soft.banksmart.auto.registration.entities.CustomerLogin;
import com.f1soft.banksmart.auto.registration.startup.ResourceLoader;
import com.f1soft.banksmart.auto.registration.utilities.Validator;
import java.util.List;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author sandhya.pokhrel
 */
@Slf4j
public class CustomerRegistrationValidator {

    private final RegistrationRecord registrationRecord;

    public CustomerRegistrationValidator(RegistrationRecord registrationRecord) {
        this.registrationRecord = registrationRecord;
    }

    public ServerResponse validateMobileNumber() {
        ServerResponse response = new ServerResponse();
        log.debug("Validating mobile Number");

        List<ApplicationPattern> patternList = ResourceLoader.patternList;
        ApplicationPattern applicationPattern = Validator.validateLoginPattern(registrationRecord.getMobileNumber(), patternList);
        response.setSuccess(true);

        if (applicationPattern == null) {
            log.info("Service Provider not found.");
            response.setSuccess(false);
            response.setMessage("Mobile Number Is Not Valid.");
            return response;
        }
        return response;
    }

//    public ServerResponse validateUsername(CustomerLogin customerLogin) {
//        log.info("******************** Validating customer username ************************");
//        ServerResponse response = new ServerResponse();
//        response.setSuccess(true);
//        if (customerLogin != null) {
//            response.setSuccess(false);
//            response.setMessage("Customer already registered");
//            return response;
//        }
//        return response;
//    }
}
