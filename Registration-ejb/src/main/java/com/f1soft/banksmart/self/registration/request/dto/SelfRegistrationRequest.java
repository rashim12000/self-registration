package com.f1soft.banksmart.self.registration.request.dto;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author rashim.dhaubanjar
 */
@Getter
@Setter
public class SelfRegistrationRequest {

    private String mobileNumber;
    private String accountNumber;
    private String referenceNum;

}
