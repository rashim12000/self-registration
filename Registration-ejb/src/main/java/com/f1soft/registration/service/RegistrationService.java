package com.f1soft.registration.service;

import com.f1soft.banksmart.auto.registration.dto.RegistrationRecord;
import com.f1soft.banksmart.auto.registration.dto.ServerResponse;

/**
 *
 * @author rashim.dhaubanjar
 */
public interface RegistrationService {

    ServerResponse selfRegister(RegistrationRecord registrationRecord);

}
