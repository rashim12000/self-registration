package com.f1soft.registration.service.impl;

import com.f1soft.banksmart.auto.registration.annotation.SelfRegistration;
import com.f1soft.banksmart.auto.registration.dto.RegistrationRecord;
import com.f1soft.banksmart.auto.registration.dto.ServerResponse;
import com.f1soft.banksmart.auto.registration.factory.RegistrationFactory;
import com.f1soft.banksmart.auto.registration.request.AbstractRegistration;
import com.f1soft.registration.service.RegistrationService;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;

/*
 * @author rashim.dhaubanjar
 */
@Slf4j
@ApplicationScoped
public class RegistrationServiceImpl implements RegistrationService {

    @Inject
    private RegistrationFactory registrationFactory;

    @Override
    public ServerResponse selfRegister(RegistrationRecord registrationRecord) {
        log.info("Entering self registrarion request service");
        AbstractRegistration abstractRegistration = registrationFactory.getRegistration();
        return abstractRegistration.register(registrationRecord);
    }
}
