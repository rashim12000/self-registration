package com.f1soft.banksmart.self.registration.utilities;

import com.f1soft.banksmart.auto.registration.dto.RegistrationRecord;
import com.f1soft.banksmart.auto.registration.entities.ApplicationUser;
import com.f1soft.banksmart.auto.registration.entities.Profile;
import com.f1soft.banksmart.auto.registration.startup.ResourceLoader;
import com.f1soft.banksmart.auto.registration.utilities.Constant;
import com.f1soft.banksmart.self.registration.request.dto.SelfRegistrationRequest;

/**
 *
 * @author rashim.dhaubanjar
 */
public class ConvertUtil {

    public static RegistrationRecord convertTORegistrationRecord(SelfRegistrationRequest registrationRequest, ApplicationUser applicationUser, Profile profile) {
        RegistrationRecord registrationRecord = new RegistrationRecord();
        registrationRecord.setAccountNumber(registrationRequest.getAccountNumber());
        registrationRecord.setMobileNumber(registrationRequest.getMobileNumber());
        registrationRecord.setReferenceNumber(registrationRequest.getReferenceNum());
        registrationRecord.setApplicationUserId(Long.parseLong(ResourceLoader.getApplicationSetupMap().get(Constant.DEFAULT_APPLICATION_USER).getApplicationValue()));
        registrationRecord.setProfileId(Long.parseLong(ResourceLoader.getApplicationSetupMap().get(Constant.DEFAULT_PROFILE).getApplicationValue()));
        registrationRecord.setProfile(profile);
        registrationRecord.setApplicationUser(applicationUser);
        return registrationRecord;

    }

}
