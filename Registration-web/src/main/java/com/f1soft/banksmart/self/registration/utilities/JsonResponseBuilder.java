package com.f1soft.banksmart.self.registration.utilities;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.Response;

/**
 *
 * @author rashim.dhaubanjar
 */
public class JsonResponseBuilder {

    public static JsonObject successResponse(String message) {
        return Json.createObjectBuilder()
                .add("success", true)
                .add("status", Response.Status.OK.getStatusCode())
                .add("code", 2000)
                .add("message", message)
                .build();
    }

    public static JsonObject unSucessfulResponse(String message) {
        return Json.createObjectBuilder()
                .add("success", false)
                .add("status", Response.Status.NOT_ACCEPTABLE.getStatusCode())
                .add("code", Response.Status.NOT_ACCEPTABLE.getStatusCode())
                .add("message", message)
                .build();
    }

    public static JsonObject internalServerErrorResponse() {
        return Json.createObjectBuilder()
                .add("success", false)
                .add("status", Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
                .add("code", Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
                .add("message", "Error while processing your request.")
                .build();
    }

}
