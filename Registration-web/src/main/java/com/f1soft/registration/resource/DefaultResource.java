package com.f1soft.registration.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 *
 * @author rashim.dhaubanjar
 */
@Path("/")
public class DefaultResource {

    @GET
    public String defaultApi() {
        return "Self Registration Server is Running";
    }
}
