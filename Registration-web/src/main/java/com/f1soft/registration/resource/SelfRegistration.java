package com.f1soft.registration.resource;

import com.f1soft.banksmart.auto.registration.dao.ApplicationDAO;
import com.f1soft.banksmart.auto.registration.dto.RegistrationRecord;
import com.f1soft.banksmart.auto.registration.dto.ServerResponse;
import com.f1soft.banksmart.auto.registration.entities.ApplicationUser;
import com.f1soft.banksmart.auto.registration.entities.Profile;
import com.f1soft.banksmart.auto.registration.startup.ResourceLoader;
import com.f1soft.banksmart.auto.registration.utilities.Constant;
import com.f1soft.banksmart.self.registration.request.dto.SelfRegistrationRequest;
import com.f1soft.banksmart.self.registration.utilities.ConvertUtil;
import com.f1soft.banksmart.self.registration.utilities.JsonResponseBuilder;
import com.f1soft.registration.service.RegistrationService;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author rashim.dhaubanjar
 */
@Path("selfRegistration")
public class SelfRegistration {

    @Inject
    private RegistrationService registrationService;

    @Inject
    private ApplicationDAO applicationDAO;

    @POST
    @Path("register")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response selfRegistration(SelfRegistrationRequest registrationRequest) {

        ApplicationUser applicationUser = applicationDAO.getApplicationUser(Long.parseLong(ResourceLoader.getApplicationSetupMap().get(Constant.DEFAULT_APPLICATION_USER).getApplicationValue()));
        Profile profile = applicationDAO.getProfileById(Long.parseLong(ResourceLoader.getApplicationSetupMap().get(Constant.DEFAULT_PROFILE).getApplicationValue()));

        RegistrationRecord registrationRecord = ConvertUtil.convertTORegistrationRecord(registrationRequest, applicationUser, profile);

        ServerResponse response = registrationService.selfRegister(registrationRecord);

        if (response.isSuccess()) {
            return Response.status(Response.Status.OK)
                    .entity(JsonResponseBuilder.successResponse(response.getMessage()))
                    .build();
        } else {
            return Response.status(Response.Status.OK)
                    .entity(JsonResponseBuilder.unSucessfulResponse(response.getMessage()))
                    .build();
        }
    }
}
